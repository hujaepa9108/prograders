<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment_model extends CI_Model {

	public function checkExistTitle($userId,$title)
	{
		$this->db->select("title");
		$this->db->from("assignment_tab");
		$this->db->where("title",strtoupper($title));
		$this->db->where("created_by",$userId);
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function add($dataAss,$dataSect)
	{
		/*
		1) add the assignment data first
		2) add section data later(use forloop)
		*/
		$this->db->insert("assignment_tab", $dataAss);
		
		for ($i=0; $i <count($dataSect) ; $i++) { 
			$new_dataSect["section_id"]=$dataSect["section"][$i];
			$new_dataSect["assignment_id"]=$this->db->insert_id();
			$this->db->insert("section_assignment_tab",$new_dataSect);
		}
		return true;
	}

	public function getCourseCode($sectionId) {
		$this->db->select("ct.ct_code as course_code");
		$this->db->from("course_tab as ct");
		$this->db->join("section_tab as st", "st.course_id=ct.ct_id");
		$this->db->where("section_id",$sectionId);
		$query = $this->db->get();  
        return $query->row();
	}

	public function getSectionLabel($sectionId) {
		$this->db->select("section_label");
		$this->db->from("section_tab");
		$this->db->where("section_id",$sectionId);
		$query = $this->db->get();  
        return $query->row();
	}

	//DATA TABLE STUFF <DRAFTED>
	function processListQueryDraft($sectionid,$userid)  
	  {  
	  	   $order_column = array(null,null,null,null);
	       $this->db->select("at.at_id as at_id, at.title as title, at.created_at as created_at");  
	       $this->db->from("assignment_tab as at");
	       $this->db->join("section_assignment_tab as sat", "at.at_id=sat.assignment_id");
	       $this->db->join("section_tab as st", "st.section_id=sat.section_id");
	       $this->db->join("section_user_tab as sut", "sut.section_id=st.section_id");
	       $this->db->join("user_tab as ut", "ut.ut_id=sut.user_id");
	       
	       $this->db->where("st.section_id",$sectionid);
	       $this->db->where("ut.ut_id",$userid);
	       $this->db->where("at.status","draft");
	       //for filtering purposes
	       if(!empty($_POST["search"]["value"])) {
	       		// $where  = "at.title LIKE '%".$this->db->escape($_POST["search"]["value"])."%'"; 
	         //    $this->db->where($where);
	         $this->db->like("at.title",$_POST["search"]["value"]);
	            // $this->db->or_like("ut.ut_id_no",$_POST["search"]["value"]);
	       }


	       // if(isset($_POST["order"])) {  
	       //      $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
	       // } 

	       // else {  
	       $this->db->order_by('at.created_at', 'DESC');  
	       // }  

	  }

	public function processListDraftDB($sectionid,$userid)
	{
		$this->processListQueryDraft($sectionid,$userid);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();
           return $query->result(); 
	}

	function processListCountDraft($sectionid,$userid)  
      {  
           $this->processListQueryDraft($sectionid,$userid);
           return $this->db->count_all_results();  
      }

      function processListFilteredDraft($sectionid,$userid){  
       $this->processListQueryDraft($sectionid,$userid);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
	//END OF DATA TABLE STUFF <DRAFTED>
}

/* End of file assignment_model.php */
/* Location: ./application/models/assignment_model.php */