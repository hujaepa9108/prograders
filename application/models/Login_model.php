<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function getLogin($email)
	{
		$this->db->select("user.email as email,user.id as id, user.name as name, user.password as password, user.role_id as role_id, role.name as role_name");
		$this->db->from("user_tab as user");
		$this->db->join("role_tab as role","user.role_id=role.id");
		$this->db->where("user.email",$email);
		$query = $this->db->get()->row();
		// file_put_contents("query_error.txt", $query);==>debug
		return $query;
	}
	public function insertLogged($data){
		$this->db->insert("logged_user_tab",$data);
	}
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */