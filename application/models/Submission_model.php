<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submission_model extends CI_Model {
    public function assess1($data)
    {
        if($this->db->insert('submission_tab', $data)) {
            return true;
        }
    }
    public function detail($submitId,$assignmentId)
    {
        $this->db->select("submit.c1 as c1,submit.c2 as c2,submit.c3 as c3,submit.c4 as c4,submit.c5 as c5, submit.total_mark as total_mark, submit.finalize_mark as finalize_mark, agst.item_1 as total_c1, agst.item_2 as total_c2, agst.item_3 as total_c3, agst.total_mark as overall, submit.source_code as source_code, submit.output_produce as output, assignment.output_test_case as stdout, assignment.id as assignment_id, assignment.title as title, submit.line_error as line_error, submit.feedback as feedback, submit.status as status");
        $this->db->from("submission_tab as submit");
        $this->db->join("assignment_tab as assignment","submit.assignment_id=assignment.id");
        $this->db->join("assignment_grade_setup_tab as agst","submit.assignment_id=agst.assignment_id");
        $this->db->where("submit.id",$submitId);
        $this->db->where("submit.assignment_id",$assignmentId);
        $query=$this->db->get()->row();
        return $query;
    }

    public function getDetail($id)
	{
		$this->db->select("assignment.title as title, assignment.description as description, assignment.start_date as start_date,assignment.end_date as end_date, grade.total_mark as total_mark, assignment.input_test_case as input_test_case, assignment.output_test_case as output_test_case,assignment.source_code as answer_code");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("assignment_grade_setup_tab as grade","assignment.id=grade.assignment_id");
		$this->db->where("assignment.id",$id);
		return $this->db->get()->row();
	}
    public function getSectionDetail($sectionId)
	{
		$this->db->select("name");
		$this->db->from("section_tab");
		$this->db->where("id",$sectionId);
		return $this->db->get()->row();
	}

    /*
	  DATA TABLE STUFF <finalize submission list>
	  Description: To get all for finalize submission
	  Roles:Stud
	*/
	public function finalizeListQuery($userId)  
	{   
		$this->db->select("submit.submitted_at as submitted_at, submit.finalize_mark as finalize_mark, submit.status as status,assignment.title as title, submit.assignment_id as assignment_id, submit.source_code as source_code, submit.output_produce as output,submit.status as status");  
        $this->db->from("submission_tab as submit");
        $this->db->join("assignment_tab as assignment","submit.assignment_id=assignment.id");
        $this->db->where("submit.status","finalize");
        $this->db->where("submit.submitted_by",$userId);
        $this->db->order_by("submit.submitted_at","DESC");
        $this->db->order_by("assignment.title");
        if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
            $this->db->like("assignment.title", $_POST["search"]["value"]);
        }  

	}
	public function finalizeListDB($userId)
	{
		$this->finalizeListQuery($userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function finalizeListCount($userId)  
	{  
		$this->finalizeListQuery($userId);
		return $this->db->count_all_results();  
	}

    function finalizeListFiltered($userId) {  
       $this->finalizeListQuery($userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    /*END OF DATA TABLE STUFF <finalize submission list>*/
    
    /*
	  DATA TABLE STUFF <In-progress submission list>
	  Description: To get all for in-progress submission
	  Roles:Stud
	*/
	public function inProgressListQuery($userId)  
	{   
		$this->db->select("submit.submitted_at as submitted_at, submit.total_mark as total_mark, submit.status as status,assignment.title as title, submit.assignment_id as assignment_id, submit.source_code as source_code, submit.output_produce as output");  
        $this->db->from("submission_tab as submit");
        $this->db->join("assignment_tab as assignment","submit.assignment_id=assignment.id");
        $this->db->where("submit.status","in-progress");
        $this->db->where("submit.submitted_by",$userId);
        $this->db->order_by("assignment.title","ASC");
        $this->db->order_by("submit.submitted_at","DESC");
        if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
            $this->db->like("assignment.title", $_POST["search"]["value"]);
        }
	}
	public function inProgressListDB($userId)
	{
		$this->inProgressListQuery($userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function inProgressListCount($userId)  
	{  
		$this->inProgressListQuery($userId);
		return $this->db->count_all_results();  
	}

    function inProgressListFiltered($userId) {  
       $this->inProgressListQuery($userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    /*END OF DATA TABLE STUFF <in-progress submission list>*/
    
    /*
	  DATA TABLE STUFF <In-progress submission list>
	  Description: To get all for in-progress submission
	  Roles:Stud
	*/
	public function allListQuery($userId)  
	{   
		$this->db->select("submit.submitted_at as submitted_at, submit.total_mark as total_mark, submit.status as status,assignment.title as title, submit.assignment_id as assignment_id,submit.source_code as source_code, submit.output_produce as output");  
        $this->db->from("submission_tab as submit");
        $this->db->join("assignment_tab as assignment","submit.assignment_id=assignment.id");
        $this->db->where("submit.submitted_by",$userId);
        $this->db->order_by("assignment.title","ASC");
        $this->db->order_by("submit.submitted_at","DESC");
        if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
            $this->db->like("assignment.title", $_POST["search"]["value"]);
        }
	}
	public function allListDB($userId)
	{
		$this->allListQuery($userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function allListCount($userId)  
	{  
		$this->allListQuery($userId);
		return $this->db->count_all_results();  
	}

    function allListFiltered($userId) {  
       $this->allListQuery($userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    /*END OF DATA TABLE STUFF <in-progress submission list>*/
    
    public function getSetup($assignmentId)
	{
		$this->db->select("total_mark");
		$this->db->from("assignment_grade_setup_tab");
		$this->db->where("assignment_id",$assignmentId);
		$query = $this->db->get()->row();
		return $query;
    }
    
    public function getSubmission($submissionId)
    {
        $this->db->select("source_code, output_produce as output, c1, c2, c3, c4, c5, total_mark, feedback");
		$this->db->from("submission_tab");
		$this->db->where("id",$submissionId);
		$query = $this->db->get()->row();
		return $query;
    }
    
    public function getGradeSetup($assignmentId)
    {
        $this->db->select("item_1 as c1_total_mark, item_2 as c2_total_mark, item_3 as c3_total_mark, total_mark as total_mark
        ");
		$this->db->from("assignment_grade_setup_tab");
		$this->db->where("assignment_id",$assignmentId);
		$query = $this->db->get()->row();
		return $query;
    }

    public function procFinalize($data)
    {
       $update = array(
        "c1" => $data["c1"],
        "c2" => $data["c2"],
        "c3" => $data["c3"],
        "c4" => $data["c4"],
        "c5" => $data["c5"],
        "status" => $data["status"],
        "total_mark" =>$data["total_mark"],
        "finalize_mark" =>$data["finalize_mark"],
        "feedback" => $data["feedback"]
       );
       
       $this->db->where('id', $data["submit_id"]);
       $this->db->update('submission_tab', $update);
       return true;
    }
}
