<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment_model extends CI_Model {

	public function getDetail($id)
	{
		$this->db->select("assignment.title as title, assignment.description as description, assignment.start_date as start_date,assignment.end_date as end_date, grade.total_mark as total_mark, assignment.input_test_case as input_test_case, assignment.output_test_case as output_test_case");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("assignment_grade_setup_tab as grade","assignment.id=grade.assignment_id");
		$this->db->where("assignment.id",$id);
		return $this->db->get()->row();
	}

	public function getDetailDraft($sectionId,$id)
	{
		$this->db->select("assignment.title as title, assignment.description as description, assignment.start_date as start_date,assignment.end_date as end_date, assignment.input_test_case as input_test_case, assignment.output_test_case as output_test_case,assignment.source_code as source_code, section.id as section_id, assignment.modified_at as modified_at");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
		$this->db->join("section_tab as section","sat.section_id=section.id");
		$this->db->where("assignment.id",$id);
		$this->db->where("assignment.status","draft");
		return $this->db->get()->row();
	}

	public function getSectionDetail($sectionId)
	{
		$this->db->select("name");
		$this->db->from("section_tab");
		$this->db->where("id",$sectionId);
		return $this->db->get()->row();
	}

	public function getTotalStudent($sectionId)
	{
		$this->db->select("count('user.id') as total");
		$this->db->from("user_tab as user");
		$this->db->join("section_user_tab as sut","user.id=sut.user_id");
		$this->db->join("section_tab as section","sut.section_id=section.id");
		$this->db->where("section.id",$sectionId);
		$this->db->where("user.role_id",3);
		return $this->db->get()->row();
	}

	public function countSubmission($assignmentId,$userId)
	{
		$this->db->select("count('submission.id') as total");
		$this->db->from("submission_tab as submission");
		$this->db->where("submission.assignment_id",$assignmentId);
		$this->db->where("submission.submitted_by",$userId);
		return $this->db->get()->row();
	}

	public function getTotalFinalize($assignmentId,$sectionId)
	{
		$this->db->select("count('submission.id') as total");
		$this->db->from("submission_tab as submission");
		$this->db->join("section_assignment_tab as sat","submission.assignment_id=sat.assignment_id");
		$this->db->where("sat.section_id",$sectionId);
		$this->db->where("sat.assignment_id",$assignmentId);
		$this->db->where("submission.status","finalize");
		return $this->db->get()->row();
	}

	//DATA TABLE STUFF<section list>
	function sectionListQuery($userId)  
	{   
	    $this->db->select("section.id as id, section.name as name");
		$this->db->from("user_tab as user");
		$this->db->join("section_user_tab as sut","user.id=sut.user_id");
		$this->db->join("section_tab as section","sut.section_id=section.id");
		$this->db->where("user.id",$userId);
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("section.name", $_POST["search"]["value"]);
	       }  

	}

	public function processAdd($data,$data2,$sectionId)
	{
		if($this->db->insert('assignment_tab', $data)) {
			$assignId=$this->db->insert_id();
			if(!empty($sectionId)){
				for ($i=0; $i < count($sectionId) ; $i++) { 
					$this->assignSections($sectionId[$i],$assignId);
				}
			}
			$data2["assignment_id"]=$assignId;
			$this->db->insert("assignment_grade_setup_tab",$data2);
			return true;
		}
	}

	public function processEdit($data,$data2,$sectionId,$sectionIdOri,$assignmentIdOri)
	{	/*
			1) delete the sections first
			2) add back section in sat
			3) update the assignment_tab
			4) update the assignment_grade_setup
		*/
		//step 1
		$this->db->where('assignment_id', $assignmentIdOri);
		$this->db->where('section_id', $sectionIdOri);
		$this->db->delete('section_assignment_tab');

		//step2
		if(!empty($sectionId)){
			for ($i=0; $i < count($sectionId) ; $i++) { 
				$this->assignSections($sectionId[$i],$assignmentIdOri);
			}
		}

		//step3
		$this->db->where('id', $assignmentIdOri);
		$this->db->update('assignment_tab', $data);

		//step4
		$this->db->where('assignment_id', $assignmentIdOri);
		$this->db->update("assignment_grade_setup_tab",$data2);
		return true;
	}

	public function checkExistTitle($instId,$title)
	{
		$this->db->select("id");
		$this->db->from("assignment_tab");
		$this->db->where("title",$title);
		$this->db->where("created_by",$instId);
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function countAll($sectionId)
	{
		$this->db->select("count(assignment.id) as total");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
	    $this->db->where("sat.section_id",$sectionId);
		return $this->db->get()->row();
	}

	public function countDraft($sectionId)
	{
		$this->db->select("count(assignment.id) as total");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
	    $this->db->where("sat.section_id",$sectionId);
		$this->db->where("assignment.status","draft");
		return $this->db->get()->row();
	}

	public function countActive($sectionId)
	{
		$this->db->select("count(assignment.id) as total");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
	    $this->db->where("sat.section_id",$sectionId);
		$this->db->where("assignment.status","active");
		return $this->db->get()->row();
	}

	public function assignSections($sectionId,$assignId)
	{
		$this->db->insert("section_assignment_tab",array('assignment_id' => $assignId, "section_id" => $sectionId ));
	}

	public function sectionListDB($userId)
	{
		$this->sectionListQuery($userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get(); 
           return $query->result(); 
	}
	public function sectionListCount($userId)  
    {  
       $this->sectionListQuery($userId);
       return $this->db->count_all_results();  
    }
    public function sectionListFiltered($userId){  
       $this->sectionListQuery($userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
	//DATA TABLE STUFF <section list>
	
	public function getTotalAssignment($sectionId)
	{
		$this->db->select("count('ass.assignment_id') as total");
		$this->db->from("assignment_tab as ass");
		$this->db->join("section_assignment_tab as sat","ass.id=sat.assignment_id");
		$this->db->join("section_tab as section","sat.section_id=section.id");
		$this->db->where("section.id",$sectionId);
		return $this->db->get()->row();
	}

	public function getSections($instId)
	{
		$this->db->select("section.name as section_name, section.id as section_id");
		$this->db->from("user_tab as user");
		$this->db->join("section_user_tab as sut","user.id=sut.user_id");
		$this->db->join("section_tab as section","sut.section_id=section.id");
		$this->db->where("sut.user_id",$instId);
		$this->db->where("user.role_id",2);
		return $this->db->get()->result();
	}

	public function getCourseInfo($instId)
	{
		$this->db->select("course.code as code, course.name as name, course.pl_name as pl_name,course.pl_id as pl_id");
		$this->db->from("course_tab as course");
		$this->db->join('course_user_tab as cut', 'course.id= cut.course_id');
		$this->db->join('user_tab as user', 'user.id= cut.user_id');
		$this->db->where("user.id",$instId);
		$query = $this->db->get()->row();//return single row
		return $query;
	}

	//DATA TABLE STUFF <all list>
	function allListQuery($sectionId,$userId)  
	  {   
	       $this->db->select("assignment.id as id,assignment.title as title, assignment.status as status, assignment.created_at, assignment.start_date as start_date,assignment.end_date as end_date");  
	       $this->db->from("assignment_tab as assignment");
	       $this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
		   $this->db->where("sat.section_id",$sectionId);
		   $this->db->where("assignment.created_by",$userId);
	       $this->db->order_by("created_at","DESC");
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("assignment.title", $_POST["search"]["value"]);
	       }  

	  }

	public function allListDB($sectionId,$userId)
	{
		$this->allListQuery($sectionId,$userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function allListCount($sectionId,$userId)  
      {  
           $this->allListQuery($sectionId,$userId);
           return $this->db->count_all_results();  
      }

      function allListFiltered($sectionId,$userId){  
       $this->allListQuery($sectionId,$userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
	//END OF DATA TABLE STUFF <all list>

	
	/*
	  DATA TABLE STUFF <all stud list>
	  Description: To get all active assignments list for student UI
	  Roles:Stud
	*/
	public function allStudListQuery($sectionId)  
	{   
		$this->db->select("assignment.id as id,assignment.title as title, assignment.status as status, assignment.start_date as start_date, assignment.end_date as end_date");  
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
		$this->db->where("sat.section_id",$sectionId);
		$this->db->where("assignment.status","ACTIVE");
		$this->db->order_by("created_at","DESC");
		if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
			$this->db->like("assignment.title", $_POST["search"]["value"]);
		}  

	}
	public function allStudListDB($sectionId)
	{
		$this->allStudListQuery($sectionId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function allStudListCount($sectionId)  
	{  
		$this->allStudListQuery($sectionId);
		return $this->db->count_all_results();  
	}

    function allStudListFiltered($sectionId) {  
       $this->allStudListQuery($sectionId);  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
	/*END OF DATA TABLE STUFF <all stud list>*/
	
	/*
		Description: to get section info
		Roles: stud
	*/
	public function getSectionInfo($userId)
	{
		$this->db->select("section.name as name, section.id as id");
		$this->db->from("section_tab as section");
		$this->db->join('section_user_tab as sut', 'section.id= sut.section_id');
		$this->db->join('user_tab as user', 'user.id= sut.user_id');
		$this->db->where("user.id",$userId);
		$query = $this->db->get()->row();//return single row
		return $query;
	}

	public function getLangId($sectionId)
	{
		$this->db->select("course.pl_id");
		$this->db->from("section_tab as section");
		$this->db->join('course_tab as course', 'course.id= section.course_id');
		$this->db->where("section.id",$sectionId);
		$query = $this->db->get()->row();//return single row
		return $query;
	}
	public function getSubmitData($userId,$assignmentId)
	{
		// $this->db->select("submitted_at,total_mark,finalize_mark,status");
		// $this->db->from("submission_tab");
		// $this->db->where("submitted_by",$userId);
		// $this->db->where("assignment_id",$assignmentId);
		// $this->db->order_by("submitted_at","DESC");

		// $query = $this->db->get()->row();
		$query = $this->db->query("select id,assignment_id, submitted_at as submitted_at,total_mark,finalize_mark,status
			from submission_tab 
			where submitted_at=(select max(submitted_at) from
			submission_tab 
			where assignment_id=".$this->db->escape($assignmentId)." 
			and submitted_by=".$this->db->escape($userId).")"
		);
		// $query = $this->db->get()->row();
		return $query->result();
	}

	public function getSetup($assignmentId)
	{
		$this->db->select("total_mark");
		$this->db->from("assignment_grade_setup_tab");
		$this->db->where("assignment_id",$assignmentId);
		$query = $this->db->get()->row();
		return $query;
	}

	/*
	  Description: To get all student that enrolled within the section. This is for instructor to perform assessment
	  Roles:inst
	*/
	public function getStudent($sectionId,$id)  
	{   
		$this->db->select("user.id as id, user.name as name, student.student_metric_no as metric_no, user.id as id");  
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
		$this->db->join("section_user_tab as sut","sat.section_id=sut.section_id");
		$this->db->join("user_tab as user","user.id=sut.user_id");
		$this->db->join("student_tab as student","user.id=student.student_id");
		$this->db->where("sat.section_id",$sectionId);
		$this->db->where("assignment.id",$id);
		$this->db->order_by("student.student_metric_no","ASC"); 
		$query = $this->db->get()->result();
		return $query;
	}
	public function getStudentResult($assignmentId,$studentId)
	{
		$query = $this->db->query("select id as submission_id,assignment_id, submitted_at as submitted_at,total_mark,finalize_mark,status
			from submission_tab 
			where submitted_at=(select max(submitted_at) from
			submission_tab where 
			assignment_id=".$this->db->escape($assignmentId)."
			and submitted_by=".$this->db->escape($studentId).")"
		);
		return $query->result();
	}

	//DATA TABLE STUFF <draft list>
	function draftListQuery($sectionId,$userId)  
	  {   
	       $this->db->select("assignment.id as id,assignment.title as title, assignment.status as status, assignment.created_at");  
	       $this->db->from("assignment_tab as assignment");
	       $this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
		   $this->db->where("sat.section_id",$sectionId);
		   $this->db->where("assignment.created_by",$userId);
		   $this->db->where("assignment.status","draft");
	       $this->db->order_by("created_at","DESC");
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("assignment.title", $_POST["search"]["value"]);
	       }  

	  }

	public function draftListDB($sectionId,$userId)
	{
		$this->draftListQuery($sectionId,$userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function draftListCount($sectionId,$userId)  
      {  
           $this->draftListQuery($sectionId,$userId);
           return $this->db->count_all_results();  
      }

      function draftListFiltered($sectionId,$userId){  
       $this->draftListQuery($sectionId,$userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
	//END OF DATA TABLE STUFF <draft list>
	
	//DATA TABLE STUFF <draft list>
	function activeListQuery($sectionId,$userId)  
	  {   
	       $this->db->select("assignment.id as id,assignment.title as title, assignment.status as status, assignment.created_at, assignment.start_date as start_date,assignment.end_date as end_date");  
	       $this->db->from("assignment_tab as assignment");
	       $this->db->join("section_assignment_tab as sat","assignment.id=sat.assignment_id");
		   $this->db->where("sat.section_id",$sectionId);
		   $this->db->where("assignment.created_by",$userId);
		   $this->db->where("assignment.status","active");
	       $this->db->order_by("created_at","DESC");
	       if( isset($_POST["search"]["value"]) && !empty(($_POST["search"]["value"])) ) {  
	            $this->db->like("assignment.title", $_POST["search"]["value"]);
	       }  

	  }

	public function activeListDB($sectionId,$userId)
	{
		$this->activeListQuery($sectionId,$userId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function activeListCount($sectionId,$userId)  
      {  
           $this->activeListQuery($sectionId,$userId);
           return $this->db->count_all_results();  
      }

      function activeListFiltered($sectionId,$userId){  
       $this->activeListQuery($sectionId,$userId);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
	//END OF DATA TABLE STUFF <active list>

	public function procDelete($assignmentId) {
		return $this->db->delete('assignment_tab', array('id' => $assignmentId)); 
	}
	public function getGradeSetup($assignmentId)
	{
		$this->db->select("item_2,item_3,total_mark");
		$this->db->from("assignment_grade_setup_tab as grade");
		$this->db->where("grade.assignment_id",$assignmentId);
		return $this->db->get()->row();
	}
	public function getAssignedSection($assignmentId)
	{
		$this->db->select("section.name as section_name,section.id as section_id");
		$this->db->from("section_tab as section");
		$this->db->join("section_assignment_tab as sat","section.id=sat.section_id");
		$this->db->where("sat.assignment_id",$assignmentId);
		return $this->db->get()->result();
	}

	public function getSubmitStat($assignmentId,$userId)
	{
		$this->db->select("count(submit.id) as total_submit");
		$this->db->from("submission_tab as submit");
		$this->db->where("submit.assignment_id",$assignmentId);
		$this->db->where("submit.submitted_by",$userId);
		// $this->db->where("submit.status","in-progress");
		return $this->db->get()->row();
	}

	public function getFinalizeStat($assignmentId,$userId)
	{
		$this->db->select("count(submit.id) as total_submit, submit.finalize_mark as finalize_mark");
		$this->db->from("submission_tab as submit");
		$this->db->group_by("submit.finalize_mark");
		$this->db->where("submit.assignment_id",$assignmentId);
		$this->db->where("submit.submitted_by",$userId);
		$this->db->where("submit.status","finalize");
		return $this->db->get()->row();
		
	}

	public function getTitle($assignmentId)
	{
		$this->db->select("title");
		$this->db->from("assignment_tab");
		$this->db->where("id",$assignmentId);
		return $this->db->get()->row();
	}
	/*
	  DATA TABLE STUFF <>
	  Description: 
	  Roles:Stud
	*/
	public function pastListQuery($userId,$assignmentId)  
	{   
		$this->db->select("submit.submitted_at as submitted_at, submit.total_mark as total_mark, submit.finalize_mark as finalize_mark, submit.assignment_id as assignment_id, submit.source_code as source_code, submit.output_produce as output,submit.status as status");  
        $this->db->from("submission_tab as submit");
        $this->db->join("assignment_tab as assignment","submit.assignment_id=assignment.id");
        $this->db->where("submit.assignment_id",$assignmentId);
        $this->db->where("submit.submitted_by",$userId);
        $this->db->order_by("submit.submitted_at","DESC");
	}

	public function pastDB($userId,$assignmentId)
	{
		$this->pastListQuery($userId,$assignmentId);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();  
           return $query->result(); 
	}

	function pastListCount($userId,$assignmentId)  
	{  
		$this->pastListQuery($userId,$assignmentId);
		return $this->db->count_all_results();  
	}

    function pastListFiltered($userId,$assignmentId) {  
       $this->pastListQuery($userId,$assignmentId);  
       $query = $this->db->get();  
       return $query->num_rows();  
    }
    /*END OF DATA TABLE STUFF <past list>*/
}

/* End of file assignment_model.php */
/* Location: ./application/models/assignment_model.php */