<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {
	public function add($dataStud,$dataSect)	{
		$this->db->insert('user_tab', $dataStud);
		$studId=$this->db->insert_id();

		//add section_tab
		$dataSect['user_id']=$studId;
		$this->db->insert('section_user_tab', $dataSect);
		return true;
	}

	public function checkExistEmail($email)
	{
		$this->db->select("ut_email");
		$this->db->from("user_tab");
		$this->db->where("ut_email",$email);
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function checkExistNoId($noId)
	{
		$this->db->select("ut_id_no");
		$this->db->from("user_tab");
		$this->db->where("ut_id_no",$noId);
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function getCourseCode($sectionId) {
		$this->db->select("ct.ct_code as course_code");
		$this->db->from("course_tab as ct");
		$this->db->join("section_tab as st", "st.course_id=ct.ct_id");
		$this->db->where("section_id",$sectionId);
		$query = $this->db->get();  
        return $query->row();
	}


	public function getCourseId($sectionId) {
		$this->db->select("ct.ct_id as ct_id");
		$this->db->from("course_tab as ct");
		$this->db->join("section_tab as st", "st.course_id=ct.ct_id");
		$this->db->where("section_id",$sectionId);
		$query = $this->db->get();  
        return $query->row();
	}

	public function getSectionLabel($sectionId) {
		$this->db->select("section_label");
		$this->db->from("section_tab");
		$this->db->where("section_id",$sectionId);
		$query = $this->db->get();  
        return $query->row();
	}
	
	//DATA TABLE STUFF
	function processListQuery($sectionid)  
	  {  
	  	   // $order_column = array("ut_name","ut_id_no","ut_email","ut_created_by", "ut_created_at",null); 
	  	   $order_column = array(null,null,null,null,null,null);
	       $this->db->select("ut.ut_id, ut.ut_name, ut.ut_id_no, ut.ut_email, ut.ut_created_at, ut.ut_created_by, st.section_label");  
	       $this->db->from("user_tab as ut");
	       $this->db->join("section_user_tab as sut", "sut.user_id=ut.ut_id");
	       $this->db->join("section_tab as st", "st.section_id=sut.section_id");
	       
	       $this->db->where("st.section_id",$sectionid);
	       $this->db->where("ut.ut_role",2);
	       //for filtering purposes
	       if(!empty($_POST["search"]["value"])) {
	       		$where  = "(`ut`.`ut_name` LIKE '%".$_POST["search"]["value"]."%' ESCAPE '!'";
				$where .= " OR `ut`.`ut_id_no` LIKE '%".$_POST["search"]["value"]."%' ESCAPE '!'";
				$where .= "OR `ut`.`ut_email` LIKE '%".$_POST["search"]["value"]."%' ESCAPE '!')"; 
	            $this->db->where($where);
	            // $this->db->or_like("ut.ut_id_no",$_POST["search"]["value"]);
	       }


	       if(isset($_POST["order"])) {  
	            $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
	       } 

	       else {  
	            $this->db->order_by('ut_created_at', 'DESC');  
	       }  

	  }

	public function processListDB($sectionid)
	{
		$this->processListQuery($sectionid);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get(); 
           file_put_contents("debug.txt", $this->db->Last_query()); 
           return $query->result(); 
	}

	function processListCount($sectionid)  
      {  
           $this->processListQuery($sectionid);
           return $this->db->count_all_results();  
      }

      function processListFiltered($sectionid){  
       $this->processListQuery($sectionid);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
      //END OF DATA TABLE STUFF
      

      //GET SECTION ID
      public function getSectId($userid)
      {
      	$this->db->select("st.section_id as section_id");
		$this->db->from("section_tab as st");
		$this->db->join("section_user_tab as sut", "sut.section_id=st.section_id");
		$this->db->where("sut.user_id",$userid);
		return $this->db->get()->row_array(); 
      }

    public function deleteStud($data) {
		return $this->db->delete('user_tab', $data); 
	}
}

/* End of file Student_model.php */
/* Location: ./application/models/Student_model.php */