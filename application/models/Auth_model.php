<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {
    public function changePasswd($data)
    {
        $this->db->set('password', $data["password"]);
        $this->db->where('id', $data["userId"]);
        $this->db->update('user_tab');
        return true;
    }
}