<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	/*
		-Description: To get total all created assignments by the current logged instructor.
		-Roles: inst
	*/
	public function getAssgmt($userId)
	{
		$this->db->select("count(assignment.id) as total");
		$this->db->from("assignment_tab as assignment");
		$this->db->join('user_tab as user', 'assignment.created_by = user.id');
		$this->db->where("user.id",$userId);
		$query = $this->db->get()->row();
		return $query;
	}

	/*
		-Description: To get total all created assignments by the current logged instructor.
		-Roles: inst
	*/
	public function getAssgmtInfo($userId)
	{
		$this->db->select("assignment.id as assignment_id");
		$this->db->from("assignment_tab as assignment");
		$this->db->join('user_tab as user', 'assignment.created_by = user.id');
		$this->db->where("user.id",$userId);
		$query = $this->db->get()->row();
		return $query;
	}

	/*
		-Description: to get total student in the class section.
		-Roles: inst
	*/
	public function getStudent($sectionId)
	{
		$this->db->select("count(user.id) as total");
		$this->db->from("user_tab as user");
		$this->db->join('section_user_tab as sut', 'user.id = sat.user_id');
		$this->db->join('section_tab as section', 'section.id = sut.section_id');
		$this->db->where("section_tab.section_id",$sectionId);
		$query = $this->db->get()->row();
		return $query;
	}

	/*
		Description: to get course code
		Roles: stud, inst
	*/
	public function getCourseInfo($userId)
	{
		$this->db->select("course.id as id,course.code as code, course.name as name");
		$this->db->from("course_tab as course");
		$this->db->join('course_user_tab as cut', 'course.id= cut.course_id');
		$this->db->join('user_tab as user', 'user.id= cut.user_id');
		$this->db->where("user.id",$userId);
		$query = $this->db->get()->row();//return single row
		return $query;
	}

	/*
		Description: to get section info
		Roles: stud
	*/
	public function getSectionInfo($userId)
	{
		$this->db->select("section.name as name,section.id as id");
		$this->db->from("section_tab as section");
		$this->db->join('section_user_tab as sut', 'section.id= sut.section_id');
		$this->db->join('user_tab as user', 'user.id= sut.user_id');
		$this->db->where("user.id",$userId);
		$query = $this->db->get()->row();//return single row
		return $query;
	}

	/*
		Description: to get section list for instructor
		Roles: inst
	*/
	public function getSectionList($userId)
	{
		$this->db->select("section.name as name, section.id as section_id");
		$this->db->from("section_tab as section");
		$this->db->join('section_user_tab as sut', 'section.id= sut.section_id');
		$this->db->join('user_tab as user', 'user.id= sut.user_id');
		$this->db->where("user.id",$userId);
		$query = $this->db->get()->result();
		return $query;
	}
	/*
		Description: To get list of student including average mark from highest to lowest
		Roles: inst
	*/
	public function getStudents($sectionId)
	{
		$this->db->select("user.name as name,user.id as stud_id, student.student_metric_no as metric_no, '' as finalize_mark, '' as grade_mark, '' as percentage, '' as title");
		$this->db->from("user_tab as user");
		$this->db->join('student_tab as student', 'user.id= student.student_id');
		$this->db->join('section_user_tab as sut', 'student.student_id= sut.user_id');
		$this->db->where("sut.section_id",$sectionId);
		$query = $this->db->get()->result();
		return $query;
	}

	/*
		Description: to fetch student assignment
		Roles: stud
	*/
	public function getAssignment($userId,$sectionId)
	{
		$this->db->select("assignment.id as id,assignment.title as title, assignment.id as id, '' as finalize_mark, '' as grade_mark, '' as percentage");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat", "assignment.id=sat.assignment_id");
		$this->db->join("section_tab as section", "sat.section_id=section.id");
		$this->db->where("section.id",$sectionId);
		$this->db->where("assignment.status","active");
		$this->db->order_by("assignment.created_at","DESC");
		$query = $this->db->get()->result();
		return $query;
	}

	/*
		Description: count total assignment for student
		Roles: stud
	*/
	public function getTotalAssgmt($userId,$sectionId)
	{
		$this->db->select("count(assignment.id) as value");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat", "assignment.id=sat.assignment_id");
		$this->db->join("section_tab as section", "sat.section_id=section.id");
		$this->db->where("section.id",$sectionId);
		$this->db->where("assignment.status","active");
		$this->db->order_by("assignment.created_at","DESC");
		$query = $this->db->get()->row();
		return $query;
	}

	public function getGradeSetup($assignmentId){
		$this->db->select("total_mark as grade_mark");
		$this->db->from("assignment_grade_setup_tab");
		$this->db->where("assignment_id",$assignmentId);
		$query = $this->db->get()->row();
		return $query;
	}

	public function getAssessment($assignmentId, $userId)
	{
		$this->db->select("submission.finalize_mark as finalize_mark");
		$this->db->from("submission_tab as submission");
		$this->db->where("assignment_id",$assignmentId);
		$this->db->where("submitted_by",$userId);
		$this->db->where("status","finalize");
		$query = $this->db->get()->row();
		return $query;
	}

	public function getHighestMark($studId)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
		$this->db->select("max(finalize_mark) as highest_mark,submission.assignment_id as assignment_id,setup.total_mark,assignment.title as title");
		$this->db->from("submission_tab as submission");
		$this->db->join("assignment_grade_setup_tab as setup", "submission.assignment_id=setup.assignment_id");
		$this->db->join("assignment_tab as assignment", "setup.assignment_id=assignment.id");
		$this->db->group_by("submission.assignment_id");
		$this->db->where("submission.submitted_by",$studId);
		$this->db->where("submission.status","finalize");
		$this->db->where("submission.assignment_id!=","null");

		$query = $this->db->get()->row();
		return $query;
	}

	public function getTotalMark($instId)
	{
		$this->db->select("assignment.id as assignment_id");
		$this->db->from("assignment_tab as assignment");
		$this->db->join("section_assignment_tab as sat", "assignment.id=sat.assignment_id");
		$this->db->join("section_user_tab as sut", "sat.section_id=sut.section_id");
		$this->db->join("user_tab as user", "sut.user_id=user.id");
		$this->db->where("user.id",$instId);
		$query = $this->db->get()->row();
		return $query;
	}
}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */