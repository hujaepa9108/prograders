<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section_model extends CI_Model {

	/*
	Processing adding new sections
	 */
	public function addDB($data)	{
		return $this->db->insert('section_tab', $data);
	}

	public function deleteDB($sectionId) {
		return $this->db->delete('section_tab', array('section_id' => $sectionId)); 
	}

	public function getSection($userId) {
		$this->db->select("st.section_label as section_label,st.section_id as section_id");
		$this->db->from("section_tab as st");
		$this->db->join("section_user_tab as sut","sut.section_id=st.section_id");
		$this->db->where("sut.user_id",$userId);
		$this->db->order_by("st.section_label");
		return $this->db->get()->result();
	}

	public function getInstName($created_by=1)
	{
		$this->db->select("ut_name");
		$this->db->from("user_tab");
		$this->db->where("ut_role",1);
		$this->db->where("ut_id",$created_by);
		return $this->db->get()->row();
	}


	public function editLabel($data,$sectionId)
	{
		$this->db->where("section_id",base64_decode($sectionId));
		$this->db->update("section_tab",$data);
		
	}


	//DATA TABLE STUFF
	//2
	function processListQuery($userid)  
	  {  
	  	   $order_column = array("section_label","ct_name",null); 
	       $this->db->select("st.section_id as section_id, ct.ct_id as ct_id,ct.ct_code as ct_code, st.section_label as section_label, ct.ct_name as ct_name, ut.ut_name as ut_name, st.created_by as created_by, st.created_at as created_at, st.role_by as role_by");  
	       $this->db->from("section_tab as st");
	       $this->db->join("section_user_tab as sut", "sut.section_id=st.section_id");
	       $this->db->join("course_tab as ct", "ct.ct_id=st.course_id"); 
	       $this->db->join("user_tab as ut", "ut.ut_id=sut.user_id");
	       $this->db->where("ut.ut_id",$userid);
	       $this->db->where("ut.ut_role",1);
	       if(!empty($_POST["search"]["value"])) { 
	            $this->db->like("section_label", $_POST["search"]["value"]);
	       }

	       if(isset($_POST["order"])) {  
	            $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
	       } 

	       else {  
	            $this->db->order_by('created_at', 'DESC');  
	       }  

	  }

	  //1
	public function processListDB($userid)
	{
		$this->processListQuery($userid);  
           if($_POST["length"] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);

           $query = $this->db->get();
           file_put_contents("debug.txt", $this->db->Last_query());
           return $query->result(); 
	}


	function processListCount($userid)  
      {  
           $this->processListQuery($userid);
           return $this->db->count_all_results();  
      }

      function processListFiltered($userid){  
       $this->processListQuery($userid);  
       $query = $this->db->get();  
       return $query->num_rows();  
      }
}

/* End of file section_model.php */
/* Location: ./application/models/section_model.php */