@extends('layouts.student.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>{{$course->code}} ({{$section->name}})</h3>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{$total_assgmt->value}}</h3>
              <p>Assignments</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-bag"></i> -->
              <i class="fas fa-file-alt"></i>
            </div>
            <a href="{{base_url()}}assignment/all" class="small-box-footer">Go to assignment list <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->     

        <!-- ./col -->
      </div>

    </div>

    <div class="content">
      <div class="container-fluid">
        <div class="row mt-2">
          <div class="col-sm-12">
            <h4><i class="nav-icon fas fa-chart-line"></i> Overall Performance</h4>
            <hr>
            {{-- WHITE BACKGROUND --}}
            <div class="invoice p-3 mb-3">
                <div class="table-responsive">
                  <table id="student_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                    <thead class="thead-light">
                      <tr>
                          <th>No</th>
                          <th>Assignment Title</th>
                          <th>Finalize Mark</th>
                          <th>Finalize Mark(%)</th>
                          <th>Go to Details</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(!empty($assignment))
                      @for ($i = 0; $i < count($assignment); $i++)
                        <tr>
                          <td>@php $row=$i+1; echo $row; @endphp</td>
                          <td>{{$assignment[$i]->title}}</td>
                          <td>{{$assignment[$i]->finalize_mark}}/{{$assignment[$i]->grade_mark}}</td>
                          <td>{{$assignment[$i]->percentage}}%</td>
                          <td align="center">
                          <a href="{{base_url()}}assignment/detail/{{base64_encode($section->id)}}/{{base64_encode($assignment[$i]->id)}}" data-toggle="tooltip" title="More Details" name="view" class="btn btn-info btn-sm"><i class="right fas fa-chevron-circle-right"></i></a>
                          </td>
                        </tr>
                      @endfor
                      @else
                        <tr>
                          <td colspan="5" class="text-secondary font-weight-bolder text-center">No assignment has been created</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
            </div>
            </div>



          </div>
        </div>
      </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection