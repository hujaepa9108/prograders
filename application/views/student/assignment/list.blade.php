@extends('layouts.student.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <h4 class="text-secondary"><i class="nav-icon fas fa-list-alt"></i> {{$title}}</h4>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
            <div class="table-responsive">
            <input type="hidden" id="sectionId" value="{{base64_encode($section->id)}}">
            <table id="assignment_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Assignment Title{{$section->id}}</th>
                        <th>Date/Time Start At</th>
                        <th>Due Date/Time</th>
                        <th>Time Left</th>
                        <th>Finalize Mark</th>
                        {{-- <th>Status</th> --}}
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                  <tr></tr>
                </tbody>  
            </table>
            </div>{{-- end of table responsive --}}    
            
          </div>
      </div>
    </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<script src="{{ base_url() }}assets/myscript/student/assignment/list.js"></script>
<!-- /.content-wrapper -->
@endsection