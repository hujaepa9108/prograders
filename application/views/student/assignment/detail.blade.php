@extends('layouts.student.master')

@section('content')
<style>
  img {
    max-width: 100%;
    height: auto;
}
</style>
{{-- DEPENDENCIES STUFF --}}
{{-- <link rel="stylesheet" href="{{base_url()}}assets/node_modules/xterm/css/xterm.css" />
<script src="{{base_url()}}assets/node_modules/xterm/lib/xterm.js"></script> --}}

<script type="text/javascript" src="{{base_url()}}assets/node_modules/moment/min/moment.min.js"></script>
<script type="text/javascript" src="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}assignment/all">Assignment List</a></li>
              <li class="breadcrumb-item active" aria-current="page">{{$assignment->title}} </li>
            </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
            
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
            <a href="{{base_url()}}assignment/all" class="btn btn-default"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
            <hr>
            <div class="row">
              <div class="col-sm-6">
                <h4>{{$assignment->title}}</h4>
                <br>
                <span class="text-success font-weight-bold font-italic" id="countdown">...</span>
                <br>
                <br>
                <h5 class="text-info">Description</h5>
                <div class="border border-muted p-3">
                  {!! $assignment->description !!}
                </div>
                <h5 class="text-secondary font-weight-bold pt-2"> Total Mark : {{$assignment->total_mark}}</h5>
                
              </div>
              <div class="col-sm-6">
                <p class="text-secondary font-italic" align="justify"><span class="text-primary font-weight-bolder">*Note:</span> Only the latest submission will be consider as the answer. Thus, you can submit as many as you want as long it is before the due date.</p>
                <label for="">Your submission</label>
                <div class="table-responsive">
                <table id="submission_list" class="table table-striped table-bordered thead-dark " style="width:100%">
                    <thead class="thead-light">
                        <tr>
                            <th>Submitted on</th>
                            <th>Total Mark</th>
                            <th>Finalize Mark</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($submitData[0]->id))
                          <tr>
                            {{-- <td><i class='fas fa-clock'></i>&nbsp;{{date("h:i A",strtotime($submitData[0]->submitted_at))}} <i class='fas fa-calendar-alt'></i>&nbsp;{{date("d-M-Y",strtotime($submitData[0]->submitted_at))}}</td> --}}
                            <td>{{$submitOn}}</td>
                            <td>{{$submitData[0]->total_mark}}/{{$assignment->total_mark}}</td>
                            @if($submitData[0]->finalize_mark!=0)
                              <td>{{$submitData[0]->finalize_mark}}/{{$assignment->total_mark}}</td>
                            @else
                              <td>Your mark have not been finalized</td>
                            @endif
                            <td>
                              <a href="{{base_url()}}submission/detail/{{base64_encode($submitData[0]->id)}}/{{base64_encode($submitData[0]->assignment_id)}}/{{base64_encode($sectionId)}}" class="btn btn-info">
                              <i class="right fas fa-chevron-circle-right"></i> 
                              </a>
                            </td>
                          </tr>
                          <tr style="border: 0px">
                            <td colspan="4" align="center">
                            <a href="{{base_url()}}assignment/past/{{base64_encode($sectionId)}}/{{$assignmentId}}" class="btn btn-primary">View Past Submissions ({{$submission->total}})</a>
                            </td>
                          </tr>
                        @else
                          <tr>
                            <td colspan="4" class="font-italic text-info font-weight-bold" align="center"> No submission has been made</td>
                          </tr>
                        @endif
                    </tbody>  
                </table>
                </div>
                {{-- code editor --}}
                <div class="codePanel pt-3">
                    <label for="">Put your source code here [langId: {{$langId}} ]</label>
                    <label class="empty-code-error"></label>
                    <div id="codeEditor" style="min-width: 100px; min-height: 200px;"></div>
                </div>

                {{-- compiler --}}
                {{-- <div class="compiler">
                    <label for="">Compiler</label>
                    <div id="compiler" style="min-height: 200px; min-width: 100px"></div>
                </div> --}}

                {{-- output --}}
                {{-- <div class="outputTerminal">
                    <label for="">Output</label>
                    <label class="empty-output-error"></label>
                    <div id="outputTestCase" style="min-height:200px; min-width: 100px"></div>
                </div> --}}

                <div class="pt-3">
                  <button type="button" class="btn btn-success" id="submit" disabled>Submit</button>
                </div>
                
                {{-- form stuff --}}
                <input type="hidden" id="lang_id" value="{{$langId}}">
                <input type="hidden" id="input" value="{{$assignment->input_test_case}}">
                <input type="hidden" id="output" value="{{$assignment->output_test_case}}">
                <input type="hidden" id="start_date" value="{{$assignment->start_date}}">
                <input type="hidden" id="end_date" value="{{$assignment->end_date}}">
                
                
              </div>
            </div>
          </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
{{-- <script src="{{base_url()}}assets/node_modules/tinymce/js/tinymce/tinymce.min.js"></script> --}}
<script src="{{base_url()}}assets/node_modules/ace/ace.js"></script>
{{-- <script src="{{base_url()}}assets/node_modules/jquery-countdown/jquery.countdown.js"></script> --}}
<script src="{{base_url()}}assets/myscript/student/assignment/detail.js"></script>{{-- role/controller/file --}}

@endsection