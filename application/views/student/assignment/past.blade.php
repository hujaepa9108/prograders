@extends('layouts.student.master')

@section('content')

{{-- DEPENDENCIES STUFF --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{base_url()}}assignment/all">Assignment List</a></li>
              <li class="breadcrumb-item" aria-current="page"><a href="{{base_url()}}assignment/detail/{{$sectionId}}/{{$assignmentId}}">{{$assignment->title}}</a></li>
              <li class="breadcrumb-item active">Past submissions</li>
            </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
          <a href="{{base_url()}}assignment/detail/{{$sectionId}}/{{$assignmentId}}" class="btn btn-default"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table id="past_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Submitted on</th>
                                <th>Total Mark</th>
                                <th>Finalize Mark</th>
                                <th>Source Code</th>
                                <th>Output Produced</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr></tr>
                        </tbody>  
                    </table>
                    </div>{{-- end of table responsive --}}
                </div>
            </div>
          </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
{{-- <script src="{{base_url()}}assets/node_modules/tinymce/js/tinymce/tinymce.min.js"></script> --}}
<script src="{{base_url()}}assets/node_modules/ace/ace.js"></script>
{{-- <script src="{{base_url()}}assets/node_modules/jquery-countdown/jquery.countdown.js"></script> --}}
<script src="{{ base_url() }}assets/myscript/student/submission/past.js"></script>
@endsection