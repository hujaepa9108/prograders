<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Welcome to Prograders</title>
    <meta name="description" content="This is an automated assessment tool designed sepcifically for assesessing programming assignments">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/vendor/font-awesome/css/font-awesome.css">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/css/landy-iconfont.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800">
    <!-- owl carousel-->
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/vendor/owl.carousel/assets/owl.carousel.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/vendor/owl.carousel/assets/owl.theme.default.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ base_url() }}assets/landy/css/custom.css">
    <!-- Favicon-->
    <!-- <link rel="shortcut icon" href="favicon.png"> -->
  </head>
<body>
@include("layouts.landing.header")
@yield('content')
@include("layouts.landing.footer")
</body>
</html>