
    <!-- navbar-->
<header class="header">
  <nav class="navbar navbar-expand-lg fixed-top">
    <a class="navbar-brand" href="#">
        <img src="{{ base_url() }}assets/landy/img/prograders.png" width="auto" height="50" alt="">
    </a>
    <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><span></span><span></span><span></span></button>
    <div id="navbarSupportedContent" class="collapse navbar-collapse">
      <ul class="navbar-nav ml-auto align-items-start align-items-lg-center">
        <li class="nav-item"><a href="#about-us" class="nav-link link-scroll"><i class="fa fa-info-circle" aria-hidden="true"></i> About This Site</a></li>
        <li class="nav-item"><a href="#contact" class="nav-link link-scroll"><i class="fa fa-envelope" aria-hidden="true"></i> Contact Information</a></li>
        {{-- <li class="nav-item"><a href="#extra-features" class="nav-link link-scroll">Features</a></li> --}}
        {{-- <li class="nav-item"><a href="#testimonials" class="nav-link link-scroll">Testimonials</a></li> --}}
        {{-- <li class="nav-item"><a href="text.html" class="nav-link">Text Page</a></li> --}}
      </ul>
      <div class="navbar-text">   
        <!-- Button trigger modal--><a href="{{ base_url() }}login" class="btn btn-primary btn-shadow btn-gradient">Login</a>
      </div>
    </div>
  </nav>
</header>