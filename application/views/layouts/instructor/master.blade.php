<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ base_url() }}assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ base_url() }}assets/node_modules/admin-lte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
  {{-- <link rel="stylesheet" href="{{ base_url() }}assets/mystyle.css"> --}}
  <link rel="stylesheet" href="{{ base_url() }}assets/node_modules/bootstrap/dist/css/bootstrap.min.css">

  <link rel="stylesheet" href="{{ base_url() }}assets/mystyle.css">

  <!-- jQuery -->
  <script src="{{ base_url() }}assets/node_modules/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ base_url() }}assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="{{ base_url() }}assets/node_modules/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="{{ base_url() }}assets/node_modules/admin-lte/dist/js/adminlte.min.js"></script>

  <script src="{{ base_url() }}assets/node_modules/vue/dist/vue.js"></script>
  <script src="{{ base_url() }}assets/node_modules/axios/dist/axios.js"></script>
  <script src="{{ base_url() }}assets/node_modules/jquery-validation/dist/jquery.validate.js"></script>
  <script src="{{ base_url() }}assets/node_modules/jquery-validation/dist/additional-methods.js"></script>
  <script src="{{ base_url() }}assets/node_modules/bootbox/bootbox.js"></script>
</head>
<body class="hold-transition sidebar-mini">
 
  <input type="hidden" id="url" value="{{ base_url() }}">
  @include('layouts.instructor.navbar')
  @include('layouts.instructor.sidebar')
  @yield('content')

 
</body>

<footer class="main-footer">
  <strong>Copyright &copy; 2020 Prograders.org</strong> All rights
  reserved.
</footer>
</html>