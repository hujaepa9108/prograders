
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4">
    <!-- Brand Logo -->
    <a href="{{ base_url() }}" class="brand-link navbar-primary" style="text-align: center">
      <span class="brand-text font-weight-light" style="color:white;"><i class="fas fa-user-tie"></i> Instructor Zone</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
        </div>
        <div class="info">
          <span><b>Welcome,</b> <b class="text-primary"><i>{{ ucwords(strtolower($_SESSION['display'])) }}</i></b></span>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          {{-- DASHBOARD --}}
          <li class="nav-item has-treeview menu-close">
            <a href="{{base_url()}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
          </li>         
          <div class="dropdown-divider"></div>

          {{-- ASSIGNMENTS --}}
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-edit"></i> 
              <p>
                Assignment
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ base_url() }}assignment/add" class="nav-link">
                  <i class="nav-icon fas fa-plus-square"></i>
                  <p>Create Assignment</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{base_url()}}assignment/section" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>Assignment List</p>
                </a>
              </li>

              <li class="nav-item">
                {{-- <a href="{{base_url()}}assignment/rubric" class="nav-link"> --}}
                <a href="#" onclick="window.open('{{base_url()}}assignment/rubric','_blank','toolbar=yes,scrollbars=yes,resizable=yes,width=1000,height=600')" class="nav-link">
                  <i class="nav-icon fas fa-calendar-alt"></i>
                  <p>View Rubric</p>
                </a>
              </li>

            </ul>
          </li>
          <div class="dropdown-divider"></div>

          {{-- STUDENT PERFORMANCE--}}
          {{-- <li class="nav-item has-treeview menu-close">
            <a href="{{base_url()}}" class="nav-link active">
              <i class="nav-icon fas fa-chart-line"></i> 
              <p>
                Class Performance
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
          </li>
          <div class="dropdown-divider"></div> --}}
          {{-- ASSIGNMENTS --}}
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fas fa-tools"></i> 
              <p>
                User Manual
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ base_url() }}manual/manual1" class="nav-link" target="_blank">
                  <i class="fas fa-book-open"></i>
                  <p>Creating Assignments</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{ base_url() }}manual/manual2" class="nav-link" target="_blank">
                  <i class="fas fa-book-open"></i>
                  <p>Finalizing Assessment</p>
                </a>
              </li>

            </ul>
          </li>
          <div class="dropdown-divider"></div>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
