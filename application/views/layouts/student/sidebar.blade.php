
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4">
    <!-- Brand Logo -->
    <a href="{{ base_url() }}" class="brand-link navbar-info" style="text-align: center">
      <span class="brand-text font-weight-light" style="color:white;"><i class="fas fa-user-graduate"></i> Student Zone</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
        </div>
        <div class="info">
          <span><b>Welcome,</b> <b class="text-primary"><i>{{ ucwords(strtolower($_SESSION['display'])) }}</i></b></span>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          {{-- DASHBOARD --}}
          <li class="nav-item has-treeview menu-close">
            <a href="{{base_url()}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
          </li>         
          <div class="dropdown-divider"></div>

          {{-- ASSIGNMENTS --}}
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-edit"></i> 
              <p>
                Assignments
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{base_url()}}assignment/all" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>Assignment List</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" onclick="window.open('{{base_url()}}assignment/rubric','_blank','toolbar=yes,scrollbars=yes,resizable=yes,width=1000,height=600')" class="nav-link">
                  <i class="nav-icon fas fa-calendar-alt"></i>
                  <p>View Rubric</p>
                </a>
              </li>

            </ul>
          </li>
          <div class="dropdown-divider"></div>

          {{--PAST SUBMISSIONS --}}
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-history"></i>
              <p>
                Submissions History
                <i class="right fas fa-chevron-circle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a href="{{base_url()}}submission/finalize" class="nav-link">
                  <i class="nav-icon fas fa-check-circle"></i> 
                  <p>Finalize</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{base_url()}}submission/inprogress" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>In-Progress</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{base_url()}}submission/all" class="nav-link">
                  <i class="nav-icon fas fa-list-alt"></i> 
                  <p>All Submissions</p>
                </a>
              </li>

            </ul>
          </li>
          <div class="dropdown-divider"></div>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
