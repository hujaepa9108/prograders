@extends('layouts.landing.master')

@section('content')
<section id="hero" class="hero hero-home bg-gray">
  <div class="container">
    <div class="row d-flex">
      <div class="col-lg-6 text order-2 order-lg-1">
        <h1 class=''>Welcome to Prograders</h1>
        <p class="hero-text ">Grading assignments like a pro!</p>
        <div class="CTA"><a href="#about-us" class="btn btn-primary btn-shadow btn-gradient link-scroll">Click to know more</a></div>
      </div>
      <div class="col-lg-6 order-1 order-lg-2"><img src="{{ base_url() }}assets/landy/img/Macbook2.png" alt="..." class="img-fluid"></div>
    </div>
  </div>
</section>
{{-- <section id="browser" class="browser">
  <div class="container">
    <div class="row d-flex justify-content-center"> 
      <div class="col-lg-8 text-center">
        <h2 class="h3 mb-5">How it works</h2>
        <div class="browser-mockup">
          <div id="nav-tabContent" class="tab-content">
            <div id="nav-first" role="tabpanel" aria-labelledby="nav-first-tab" class="tab-pane fade show active"><img src="{{ base_url() }}assets/landy/img/preview-3.png" alt="..." class="img-fluid"></div>
            <div id="nav-second" role="tabpanel" aria-labelledby="nav-second-tab" class="tab-pane fade"><img src="{{ base_url() }}assets/landy/img/preview-2.png" alt="..." class="img-fluid"></div>
            <div id="nav-third" role="tabpanel" aria-labelledby="nav-third-tab" class="tab-pane fade"><img src="{{ base_url() }}assets/landy/img/preview-1.png" alt="..." class="img-fluid"></div>
          </div>
        </div>
      </div>
    </div>
    <div id="myTab" role="tablist" class="nav nav-tabs">
      <div class="row">
        <div class="col-md-4"><a id="nav-first-tab" data-toggle="tab" href="#nav-first" role="tab" aria-controls="nav-first" aria-expanded="true" class="nav-item nav-link active"> <span class="number">1</span>Choose any website to turn into an interactive pinboard for feedback</a></div>
        <div class="col-md-4"><a id="nav-second-tab" data-toggle="tab" href="#nav-second" role="tab" aria-controls="nav-second" class="nav-item nav-link"> <span class="number">2</span>Choose any website to turn into an interactive pinboard for feedback</a></div>
        <div class="col-md-4"><a id="nav-third-tab" data-toggle="tab" href="#nav-third" role="tab" aria-controls="nav-third" class="nav-item nav-link"> <span class="number">3</span>Choose any website to turn into an interactive pinboard for feedback</a></div>
      </div>
    </div>
  </div>
</section> --}}
<section id="about-us" class="about-us">
  <div class="container">
    <h2 class="text-primary"><i class="fa fa-info-circle" aria-hidden="true"></i> About Prograders</h2>
    <div class="row">
      <p class="lead col-sm-12" align='justify'>Prograders is an online assessment tool that is designed to assists instructors in assessing programming assignment. Its main feature the semi-automated assessment allows the assignments to be partially assessed automatically. In addition, the built-in rubric also makes the assessment process more convenience for the instructor on giving grades. It also supports multiple programming languages such as C, C++, Python and Java.</p>
    </div>
    <h2 class="text-secondary" align="center">How it works?</h2>
    <div class="row">
      <div class="col-sm-12 pt-2" align='center'>
        <br/>
        <div class="row">
          <div class="col-sm-3">
            <img src="{{ base_url() }}assets/landy/img/paper.png" alt="" width="200px" height="100px" class="img-fluid custom-border" >
            <p class="lead" align='center'>
              <span class='text-primary font-weight-bold'>Step 1:</span> Instructor Create The Assignment
            </p>
          </div>
          <div class="col-sm-3" align='center'>
            <img src="{{ base_url() }}assets/landy/img/upload.png" alt="" width="200px" height="100px" class="img-fluid custom-border" >
            <p class="lead">
              <span class='text-primary font-weight-bold'>Step 2:</span> Student Submits Their Assignment Program
            </p>
          </div>
          <div class="col-sm-3" align='center'>
            <img src="{{ base_url() }}assets/landy/img/idea.png" alt="" width="200px" height="100px" class="img-fluid custom-border" >
            <p class="lead">
              <span class='text-primary font-weight-bold'>Step 3:</span> The Assignment Is Then Partially Assessed By Prograders
            </p>
          </div>
          <div class="col-sm-3" align='center'>
            <img src="{{ base_url() }}assets/landy/img/clipboards.png" alt="" width="200px" height="100px" class="img-fluid custom-border" >
            <p class="lead">
              <span class='text-primary font-weight-bold'>Step 4:</span> Instructor Finalized The Assessment
            </p>
          </div>
          <div class="col-sm-3" align='center'>
            <img src="{{ base_url() }}assets/landy/img/test.png" alt="" width="200px" height="100px" class="img-fluid custom-border" >
            <p class="lead">
              <span class='text-primary font-weight-bold'>Step 5:</span> Assessment Complete
            </p>
          </div>
        </div>
      </div>
    </div>
    {{-- <a href="#" class="btn btn-primary btn-shadow btn-gradient">Discover More</a> --}}
  </div>
</section>

{{-- <section id="features" class="features">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="text col-lg-6 order-2 order-lg-1">
        <div class="icon"><img src="{{ base_url() }}assets/landy/img/medal.svg" alt="..." class="img-fluid"></div>
        <h4>Your peace of mind is our business</h4>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </p><a href="#" class="btn btn-primary btn-shadow btn-gradient">View More</a>
      </div>
      <div class="image col-lg-6 order-1 order-lg-2"><img src="{{ base_url() }}assets/landy/img/feature-1.png" alt="..." class="img-fluid"></div>
    </div>
    <div class="row d-flex align-items-center">
      <div class="image col-lg-6"><img src="{{ base_url() }}assets/landy/img/feature-2.png" alt="..." class="img-fluid"></div>
      <div class="text col-lg-6">
        <div class="icon"><img src="{{ base_url() }}assets/landy/img/hourglass.svg" alt="..." class="img-fluid"></div>
        <h4>Your peace of mind is our business</h4>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </p><a href="#" class="btn btn-primary btn-shadow btn-gradient">View More</a>
      </div>
    </div>
    <div class="row d-flex align-items-center">
      <div class="text col-lg-6 order-2 order-lg-1">
        <div class="icon"><img src="{{ base_url() }}assets/landy/img/cup.svg" alt="..." class="img-fluid"></div>
        <h4>Your peace of mind is our business</h4>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. </p><a href="#" class="btn btn-primary btn-shadow btn-gradient">View More</a>
      </div>
      <div class="image col-lg-6 order-1 order-lg-2"><img src="{{ base_url() }}assets/landy/img/feature-3.png" alt="..." class="img-fluid"></div>
    </div>
  </div>
</section> --}}
{{--
<section id="extra-features" class="extra-features bg-primary">
  <div class="container text-center">
    <header>
      <h2>Available Features</h2>
      <div class="row">
        <p class="lead col-lg-8 mx-auto">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
      </div>
    </header>
    <div class="grid row">
      <div class="item col-lg-4 col-md-6">
        <div class="icon"> <i class="icon-diploma"></i></div>
        <h3 class="h5">Semi Auto-Assessment</h3>
        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
      </div>
      <div class="item col-lg-4 col-md-6">
        <div class="icon"> <i class="icon-folder-1"></i></div>
        <div class="icon"> &lt;/&gt;</div>
        <h3 class="h5">Multiple Language Support</h3>
        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
      </div>
      <div class="item col-lg-4 col-md-6">
        <div class="icon"> <i class="icon-gears"></i></div>
        <h3 class="h5">Lorem Ipsum Dolor</h3>
        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
      </div>
      <div class="item col-lg-4 col-md-6">
        <div class="icon"> <i class="icon-management"></i></div>
        <h3 class="h5">Lorem Ipsum Dolor</h3>
        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
      </div>
      <div class="item col-lg-4 col-md-6">
        <div class="icon"> <i class="icon-pie-chart"></i></div>
        <h3 class="h5">Lorem Ipsum Dolor</h3>
        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
      </div>
      <div class="item col-lg-4 col-md-6">
        <div class="icon"> <i class="icon-quality"></i></div>
        <h3 class="h5">Lorem Ipsum Dolor</h3>
        <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
      </div>
    </div>
  </div>
</section>
--}}
{{-- <section id="testimonials" class="testimonials">
  <div class="container">
    <header class="text-center no-margin-bottom">   
      <h2>Happy Clients</h2>
      <p class="lead">There are many variations of passages of Lorem Ipsum available, but the majority have</p>
    </header>
    <div class="owl-carousel owl-theme testimonials-slider"> 
      <div class="item-holder">
        <div class="item">
          <div class="avatar"><img src="{{ base_url() }}assets/landy/img/avatar-3.jpg" alt="..." class="img-fluid"></div>
          <div class="text">
            <div class="quote"><img src="{{ base_url() }}assets/landy/img/quote.svg" alt="..." class="img-fluid"></div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong class="name">Jessica Watson</strong>
          </div>
        </div>
      </div>
      <div class="item-holder">
        <div class="item"> --}}
          {{-- <div class="avatar"><img src="base_url() }}assets/landy/img/avatar-5.jpg" alt="..." class="img-fluid"></div> --}}
          {{-- <div class="text">
            <div class="quote"><img src="base_url() }}assets/landy/img/quote.svg" alt="..." class="img-fluid"></div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong class="name">Sarrah Wood</strong>
          </div>
        </div>
      </div>
      <div class="item-holder">
        <div class="item"> --}}
          {{-- <div class="avatar"><img src="{{ base_url() }}assets/landy/img/avatar-3.jpg" alt="..." class="img-fluid"></div> --}}
          {{-- <div class="text">
            <div class="quote"><img src="{{ base_url() }}assets/landy/img/quote.svg" alt="..." class="img-fluid"></div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong class="name">Jessica Watson</strong>
          </div>
        </div>
      </div>
      <div class="item-holder">
        <div class="item">
          <div class="avatar"><img src="{{ base_url() }}assets/landy/img/avatar-5.jpg" alt="..." class="img-fluid"></div>
          <div class="text">
            <div class="quote"><img src="{{ base_url() }}assets/landy/img/quote.svg" alt="..." class="img-fluid"></div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong class="name">Sarrah Wood</strong>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="newsletter" class="newsletter bg-gray">
  <div class="container text-center">
    <h2>Subscribe to Newsletter</h2>
    <p class="lead">There are many variation passages of lorem ipsum, but the majority have</p>
    <div class="form-holder">
      <form id="newsletterForm" action="#">
        <div class="form-group">
          <input type="email" name="email" id="email" placeholder="Enter Your Email Address">
          <button type="submit" class="btn btn-primary btn-gradient submit">Subscribe</button>
        </div>
      </form>
    </div>
  </div>
</section> --}}
<div id="scrollTop">
  <div class="d-flex align-items-center justify-content-end"><i class="fa fa-long-arrow-up"></i>To Top</div>
</div>
@endsection