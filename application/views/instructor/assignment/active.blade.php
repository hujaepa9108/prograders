@extends('layouts.instructor.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-1">
        <div class="col-sm-12">
          {{-- <h1>List of Assignments for <span class="text-primary"><b>Section {{ $section->section_label }}</b></span> ({{ $course->course_code }})</h1> --}}
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{base_url()}}assignment/section">Section List</a></li>
                <li class="breadcrumb-item active" aria-current="page" class="text-white">Assignment List ({{$section->name}}) </li>
              </ol>
          </nav>

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">

          <a href="{{base_url()}}assignment/section" class="btn btn-default"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
          
          <div class="float-right">
          	<a href="{{base_url()}}assignment/add" class="btn btn-success" data-toggle="tooltip" title="Create New Assignment"><i class="fas fa-plus-circle"></i> Create</a>
          </div>
          <hr>

         <ul class="nav nav-tabs">
		  <li class="nav-item">
		    <a class="nav-link" href="{{base_url()}}assignment/list/{{$sectionId}}">All <span class="badge badge-pill badge-primary">{{$all}}</span></a>
		  </li>
		  <li class="nav-item">
            <a class="nav-link " href="{{base_url()}}assignment/draft/{{$sectionId}}">Draft <span class="badge badge-pill badge-warning">{{$draft}}</span></a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link active" href="{{base_url()}}assignment/active/{{$sectionId}}">Active <span class="badge badge-pill badge-success">{{$active}}</span></a>
		  </li>
		</ul>

		{{-- <div class="tab-content" id="myTabContent">
			
		</div> --}}
		<br>
		<div class="table-responsive">
            <table id="assignment_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Assignment Title</th>
                        <th>Status</th>
                        <th>Finalize Submissions</th>
                        <th>Created At</th>
                        <th>Time/Date Start</th>
                        <th>Time/Date Due</th>
                        <th>Time Left</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <tr></tr>
                </tbody>  
            </table>
            </div>{{-- end of table responsive --}}
        </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="{{base_url()}}assets/myscript/instructor/assignment/active.js"></script>
@endsection