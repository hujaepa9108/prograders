@extends('layouts.instructor.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-1">
        <div class="col-sm-6">
          <h4 class='text-secondary'>{{$title}}</h4>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
            
            <div class="table-responsive">
                <table id="section_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                    <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Section Name</th>
                            <th>Total Enrolled Student</th>
                            <th>Total Created Assignment</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr></tr>
                    </tbody>  
                </table>
            </div>{{-- end of table responsive --}}
            
          </div>
      </div>
    </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<script src="{{ base_url() }}assets/myscript/instructor/assignment/section.js"></script>
<!-- /.content-wrapper -->
@endsection