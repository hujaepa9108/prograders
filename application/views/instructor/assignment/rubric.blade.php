@extends('layouts.instructor.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          {{-- <h3>{{$title}}</h3> --}}

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
             <div class="table-responsive">
                <table class="table table-bordered thead-dark" style="width:100%">
                    <thead class="thead-light">
                        <tr>
                        	<th>Item no.</th>
                            <th>Criteria</th>
                            <th>Sub-criteria</th>
                            <th>Marks</th>
                            <th>Assessment Type(Manual/Automatic)</th>
                            <th>Bloom's Taxonomy Level (Cognitive/Psychomotor)</th>
                        </tr>
                    </thead>
                    <tbody>
                    	{{-- Criteria 1 --}}
                        <tr>
                        	<td rowspan="5">1</td>
                        	<td rowspan="5" ><b>Ability to analyze problem and identify requirements</td>
                        	<td>Unable to identify any input and output</b></td>
                        	<td>1</td>
                        	<td rowspan="5">Automatic<br/><i class="text-primary">*Still can be altered manually during performing assessment</i></td>
                        	<td rowspan="5">Cognitive</td>
                        </tr>
                        <tr>
                        	<td>Able to identify only one input and output</td>
                        	<td>2</td>
                        </tr>
                        <tr>
                        	<td>
                            Able to identify correctly some input and output <b>(1% &#8805; 50% match)</b>.<br/><i class="text-primary">*This applies only for output that has more than one line</i>
                          </td>
                        	<td>3</td>
                        </tr>
                        <tr>
                          <td>
                            Able to identify correctly some input and output <b>(&gt;50% to &lt;100% match)</b><br/><i class="text-primary">*This applies only for output that has more than one line</i>
                          </td>
                        	<td>4</td>
                        </tr>
                        <tr>
                          <td>
                            Able to identify correctly some input and output <b>(100% match)</b>.<br/><i class="text-primary">*This applies only for output that has more than one line</i>
                          </td>
                        	<td>5</td>
                        </tr>

                        {{-- Criteria 2 --}}
                        <tr>
                        	<td rowspan="5">2</td>
                        	<td  rowspan="5"><b>Ability to apply the required data type (accurate variable declaration such as int,float,double, etc.), data structure (e.g usage of arrays) and Object-oriented Programming (e.g usage of objects, classes etc.)</b><br><i class="text-primary">* Optional criteria</i></td>
                          <td>Unable to identify any required data type/data structure/OOP
                          </td>
                        	<td>1</td>
                        	<td rowspan="5">Manual</td>
                        	<td rowspan="5">Psychomotor</td>
                        </tr>
                        <tr>
                        	<td>Able to identify some of the required data type/data structure/OOP</td>
                        	<td>2</td>                        	
                        </tr>
                        <tr>
                        	<td>Able to identify all of the required data type/data structure/OOP but does not produced correct output <b>(compilation or logical error)</b>
                          </td>
                        	<td>3</td>                        	
                        </tr>
                         <tr>
                        	<td>Able to identify all of the required data type/data structure/OOP but only partial correct output were produced <b>(eg. minor wrong output format)</b></td>
                        	<td>4</td>                        	
                        </tr>
                        <tr>
                        	<td>Able to identify all of the required data type/data structure/OOP and does produce correct output  <b>(100% match)</b></td>
                        	<td>5</td>                        	
                        </tr>

                        {{-- Criteria 3 --}}
                        <tr>
                          <td rowspan="5">3</td>
                        	<td rowspan="5" ><b>Ability to apply required control structure(e.g if-statement, looping and so forth)</b><br><i class="text-primary">* Optional criteria</i></td>
                        	<td>Unable to identify any of the required control structure</td>
                        	<td>1</td>
                        	<td rowspan="5">Manual</td>
                        	<td rowspan="5">Psychomotor</td>
                        </tr>
						            <tr>
                        	<td>Able to identify some of the required control structure</td>
                        	<td>2</td>
                        </tr>
                        <tr>
                        	<td>Able to identify all of the required control structure but does not produced correct output <b>(compilation or logical error)</b></td>
                        	<td>3</td>
                        </tr>
                        <tr>
                        	<td>Able to identify all of the required control structure but only partial correct output were produced <b>(eg. minor wrong output format)</b></td>
                        	<td>4</td>
                        </tr>
                        <tr>
                        	<td>Able to identify all of the required control structure and does produce correct output <b>(100% match)</b></td>
                        	<td>5</td>
                        </tr>

                        {{-- Criteria 4 --}}
                        <tr>
                          <td rowspan="4">4</td>
                        	<td rowspan="4" ><b>Ability to run the program without syntax and runtime error</td>
                        	<td>Unable to compile/run the program</b></td>
                        	<td>1</td>
                        	<td rowspan="4">Automatic <br/><i class="text-primary">*Still can be altered manually during performing assessment</i></td>
                        	<td rowspan="4">Psychomotor</td>
                        </tr>
                        <tr>
                        	<td>Able to compile/run the program but have runtime error</td>
                        	<td>2</td>
                        </tr>
                        <tr>
                        	<td>Able to compile/run the program without runtime error but wrong output produced</td>
                        	<td>3</td>
                        </tr>
                        {{-- <tr>
                        	<td>Able to compile/run the program without runtime error but wrong output produced <b>(minor wrong output format)</b></td>
                        	<td>4</td>
                        </tr> --}}
                        <tr>
                        	<td>Able to compile/run the program without runtime error and does produced correct output</td>
                        	<td>4</td>
                        </tr>

                        {{-- Criteria 5 --}}
                        <tr>
                          <td rowspan="5">5</td>
                        	<td rowspan="5" ><b>Ability to produce a readable code</b></td>
                        	<td>Poorly organized code <span class="text-secondary">(e.g no indentation)</span>,bad at naming conventions and no comments</td>
                        	<td>1</td>
                        	<td rowspan="5">Manual</td>
                        	<td rowspan="5">Psychomotor</td>
                        </tr>
                        <tr>
                        	<td>The code is organized <span class="text-secondary">(e.g have indentation)</span> but still bad at naming conventions and no comments added</td>
                        	<td>2</td>
                        </tr>
                        <tr>
                        	<td>The code is organized, good naming conventions and but no comments added</td>
                        	<td>3</td>
                        </tr>
                        <tr>
                        	<td>The code is organized, good naming conventions and comments were added which makes the student's code easy to read</td>
                        	<td>4</td>
                        </tr>
                        <tr>
                        	<td>The code is extremely well organized, accurate naming conventions and very detail comments were added which makes the student's code really easy to read</td>
                        	<td>5</td>
                        </tr>
                    </tbody>  
                </table>
            </div>{{-- end of table responsive --}}
            
          </div>
      </div>
    </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection