@extends('layouts.instructor.master')

@section('content')

{{-- DEPENDENCIES STUFF --}}
{{-- <link rel="stylesheet" href="{{base_url()}}assets/node_modules/xterm/css/xterm.css" />
<script src="{{base_url()}}assets/node_modules/xterm/lib/xterm.js"></script> --}}

<script type="text/javascript" src="{{base_url()}}assets/node_modules/moment/min/moment.min.js"></script>
<script type="text/javascript" src="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.css" />
<style>
  a.disabled {
    pointer-events: none;
    cursor: default;
  }
  img {
    max-width: 100%;
    height: auto;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-1">
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{base_url()}}assignment/section">Section List</a></li>
                <li class="breadcrumb-item"><a href="{{base_url()}}assignment/list/{{base64_encode($sectionId)}}">Assignment List ({{$section->name}})</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$assignment->title}} </li>
              </ol>
          </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
            <a href="{{base_url()}}assignment/list/{{base64_encode($sectionId)}}" class="btn btn-default"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
            <hr>
            <div class="row">
              <div class="col-sm-6">
                  <h4>{{$assignment->title}}</h4>
                  <br>
                  <span class="text-success font-weight-bold font-italic" id="countdown">...</span>
                  <br>
                  <br>
                  <h5 class="text-info">Description</h5>
                  <div class="border border-muted p-3">
                    {!! $assignment->description !!}
                  </div>
                  <h5 class="text-secondary font-weight-bold pt-2"> Total Mark : {{$assignment->total_mark}}</h5>
              </div>
              <div class="col-sm-6">
                <form action="{{base_url()}}assignment/downloadCSV" method="post">
                  <h4>Student Submissions 
                    <button class="btn btn-info btn-sm" type="submit" name="genCSV">Download as CSV</button>
                  </h4>
                  <input type="hidden" name="sectionId" value="{{$sectionId}}">
                  <input type="hidden" name="assignmentId" value="{{$assignmentId}}">
                </form>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered thead-dark">
                      <thead class="thead-light">
                          <tr>
                              <th>No</th>
                              <th>Student Metric No</th>
                              <th>Student Name</th>
                              <th>Total Mark</th>
                              <th>Finalize Mark</th>
                              <th>Assessment</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php $j=0; @endphp
                        @for ($i = 0; $i <count ($student); $i++)
                            <tr>
                              <td>@php $j=$i+1; echo $j; @endphp</td>
                              <td>{{$student[$i]->metric_no}}</td>
                              <td>{{$student[$i]->name}}</td>
                              @if(!empty($result[$i]))
                                @for($r=0;$r<count($result[$i]);$r++)

                                  @if(isset($result[$i][$r]->total_mark) && $result[$i][$r]->total_mark!=0)
                                    <td>{{$result[$i][$r]->total_mark}}/{{$setup->total_mark}}</td>
                                  @endif

                                  @if(isset($result[$i][$r]->finalize_mark) && $result[$i][$r]->finalize_mark!=0)
                                    <td><i class="fas fa-check-circle text-success"></i> {{$result[$i][$r]->finalize_mark}}/{{$setup->total_mark}}</td>
                                  @else
                                    <td><span class="badge badge-danger">!</span> Marks has not been finalize yet</td>
                                  @endif

                                  @if(isset($result[$i][$r]->status) && $result[$i][$r]->status=="finalize")
                                    <td>
                                      <a href="{{base_url()}}submission/detail/{{base64_encode($result[$i][$r]->submission_id)}}/{{$assignmentId}}/{{base64_encode($sectionId)}}/{{base64_encode($student[$i]->metric_no)}}" class="btn btn-success btn-sm" data-toggle='tooltip' title='View Assessment Details'><i class="fas fa-search"></i></a>
                                    </td>
                                  @else
                                  <td>
                                    <a href="{{base_url()}}submission/assessment/{{base64_encode($sectionId)}}/{{$assignmentId}}/{{base64_encode($result[$i][$r]->submission_id)}}" class="btn btn-warning btn-sm assessment" data-toggle='tooltip' title='Perform Assessment'><i class="fas fa-edit"></i></a>
                                    <!-- Button to Open the Modal -->
                                    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-manually" data-id="{{base64_encode($student[$i]->id)}},{{$assignmentId}}" data-backdrop="static">
                                      Add Manually
                                    </button>
                                  </td>
                                  @endif
                                @endfor
                              @else
                                <td>0/{{$setup->total_mark}}</td>
                                <td class="text-danger font-weight-bolder" colspan="2"><i class="fas fa-times-circle"></i> No submission has been made
                                  <br>
                                    <!-- Button to Open the Modal -->
                                    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-manually" data-id="{{base64_encode($student[$i]->id)}},{{$assignmentId}}" data-backdrop="static">
                                      Add Manually
                                    </button>
                                </td>
                              @endif
                            </tr>
                        @endfor
                      </tbody>
                  </table>
                  
                  <input type="hidden" id="lang_id" value="{{$langId}}">
                  <input type="hidden" id="input" value="{{$assignment->input_test_case}}">
                  <input type="hidden" id="output" value="{{$assignment->output_test_case}}">
                  <input type="hidden" id="start_date" value="{{$assignment->start_date}}">
                  <input type="hidden" id="end_date" value="{{$assignment->end_date}}">

                </div>
              </div>
            </div>
          </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Add Source Code -->
<div class="modal fade" id="add-manually">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <p class="modal-title font-weight-bolder">Add Source Manually</p>
        <button type="button" class="close" data-dismiss="modal" >&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="codePanel">
          <label class="empty-code-error">Add The Code Below</label>
          <div id="codeEditor" style="min-width: 100px; min-height: 200px;"></div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="submit">Submit</button>
      </div>

    </div>
  </div>
</div>
{{--END OF MODAL--}}

<script src="{{base_url()}}assets/node_modules/ace/ace.js"></script>
<script src="{{base_url()}}assets/myscript/instructor/assignment/detail.js"></script>{{-- role/controller/file --}}

@endsection