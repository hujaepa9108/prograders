@extends('layouts.instructor.master')

@section('content')

{{-- DEPENDENCIES STUFF --}}
{{-- <link rel="stylesheet" href="{{base_url()}}assets/node_modules/xterm/css/xterm.css" />
<script src="{{base_url()}}assets/node_modules/xterm/lib/xterm.js"></script> --}}

<script type="text/javascript" src="{{base_url()}}assets/node_modules/moment/min/moment.min.js"></script>
<script type="text/javascript" src="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <h4 class="text-secondary"><i class="nav-icon fas fa-plus-square"></i> Create Assignment</h4>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
              
            <form id="add_assignment">
              {{-- PART 1 --}}
              <div class="form-group" id="section_list">
                 <label><h4>1) Assigning Sections</h4></label>
                  <table class="table table-striped table-bordered thead-dark" style="width:50%">
                    <tr>
                      <th>Section<span class='text-danger'>*</span></td><th>Click the checkbox</th>
                    </tr>
                    <tr v-for="item in section">
                      <td>@{{item.section_name}}</td>
                      <td>
                        <input type="checkbox" v-bind:value="item.section_id" name="sectionId[]"/>
                      </td>
                    </tr>
                  </table>
              </div>{{-- end of form group --}}
              <hr>

              {{-- PART 2 --}}
              <div class="form-group">
                <label><h4>2) Assignment Info</h4></label>
                <div class="form-group">
                  <label for="">Assignment Title<span class='text-danger'>*</span></label> &nbsp;&nbsp;
                  <input type="text" name="title" class="form-control" width="200px">
                </div>

                <div class="form-group">
                  <label for="" >Assignment Description<span class='text-danger'>*</span></label> &nbsp;&nbsp;
                  <label for="" class="empty-content-error badge badge-danger"></label>
                  {{-- <span class="empty-content-error"> --}}<textarea name="content" id="content"></textarea>{{-- </span> --}}   
                </div>
              
                <div class="form-group">
                    <label for="">Assignment Submission Due Date<span class='text-danger'>*</span></label>
                    <div class="input-group">
                      <input type="text" name="duedate" class="form-control" placeholder="Click to enter the date range" />
                      <div class="input-group-append">
                        <button class="btn btn-success" type="button" id="cleardates">Reset Dates</button>
                      </div>
                    </div>
                    {{-- <button class="btn btn-success btn-sm" id="cleardates">Reset Dates</button> --}}
                </div>
              </div>
              <hr>

              {{-- PART 3 --}}
              <div class="form-group">
                <label><h4>3) Source Code & Test Case Info</h4></label>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card card-primary">
                     <div class="card-header">
                        <h4 class="card-title">IDE</h4>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-sm-12">
                            <ul class="border border-info p-2" style="list-style-type:none;">
                              <li class="text-primary font-weight-bold"><i class="fas fa-info-circle"></i> How to?</li>
                              <li class="font-weight-bold"><span class="text-success">Step 1:</span> Add the code into the code editor.</li>
                              <li class="font-weight-bold"><span class="text-success">Step 2:</span> Add the input test case if it is part of the assignment. <span class="text-secondary">(Optional)</span> </li>
                              <li class="font-weight-bold"><span class="text-success">Step 3:</span> Click "Compile & Execute" button to generate the output test case.</li>
                            </ul>
                          </div>
                        </div>
                        {{-- IDE  STUFF--}}
                        <div class="row">
                          <div class="col-sm-6 codePanel">
                            <label for="">Code Editor<span class='text-danger'>*</span></label>
                            <label class="empty-code-error"></label>
                            <div id="codeEditor" style="min-width: 200px; min-height: 450px;"></div>
                          </div>
                          <div class="col-sm-6">
                            <div class="row">
                              <div class="col-sm-12">
                                <label for="">Input Test Case <span class="text-secondary">Optional</span></label>
                                <div id="inputTestCase" style="min-height: 100px; min-width: 200px"></div>
                              </div>
                              <div class="col-sm-12">
                                <label for="">Compiler</label>
                                <div id="compiler" style="min-height: 100px; min-width: 200px"></div>
                              </div>
                              <div class="col-sm-12 outputTerminal">
                                <label for="">Output Test case<span class='text-danger'>*</span></label>
                                <label class="empty-output-error"></label>
                                <div id="outputTestCase" style="min-height: 100px; min-width: 200px"></div>
                              </div>
                              <div class="col-sm-12 pt-2">
                                <button type="button" class="btn btn-success" id="compile">Compile & Execute</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        {{-- END OF IDE STUFF --}}
                      </div>{{-- end of card body --}}
                    </div>{{-- end of card opening --}}
                  </div>{{-- end of col part3--}}
                </div>{{-- end of row part3 --}}
              </div>{{-- End of form-group part3 --}}

              <hr>
              
              <div class="form-group">
                <label><h4>4) Assessment Setup </h4></label>
                <br>
                <div class="border border-primary p-2">
                  <label><span class="text-primary"><i class="fas fa-info-circle"></i> Info</span></label>
                    <p align="justify">Based on the assignment rubric that this platform use, criteria item number 2 (C2) and criteria item number 3 (C3) are optional since not all programming assignment involves with these items. It is up to the instructor whether to include it or not by choosing relevant or not relevant. The total mark of the assignment will differ if not include these items.</p>
                </div>
                <br/>
                <div class="form-group">                  
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered thead-dark" style="width:100%">
                        <thead class="thead-light">
                            <tr>
                                <th>Criteria<span class='text-danger'>*</span></th>
                                <th>Relevant</th>
                                <th>Not Relevant</th>
                            </tr>
                        </thead>
                        <tbody>
                          
                          {{-- Q1 --}}
                          <tr>
                            <td>(C2) Ability to apply the required data type (accurate variable declaration such as int, float, double, etc.), data structure (e.g usage of arrays) and Object-oriented Programming (e.g usage of objects, classes etc.)
                            <td><input type="radio" name="item_2" value="1"></td>
                            <td><input type="radio" name="item_2" value="0"></td>
                          </tr>
                          
                          {{-- Q2 --}}
                          <tr>
                            <td>(C3) Ability to apply required control structure(e.g if-statement, looping and so forth)</td>
                            <td><input type="radio" name="item_3" value="1"></td>
                            <td><input type="radio" name="item_3" value="0"></td>
                          </tr>

                        </tbody>  
                    </table>
                  </div>
                  {{-- end of table responsive --}}
                </div>
                {{-- end of form group --}}
              </div>
              <hr>
              <div class="form-group">
                <button type="button" class="btn btn-secondary" id="cancel">Cancel</button>
                <div class="float-right">
                  <button type="button" class="btn btn-primary" id="draft"><i class="fas fa-edit"></i> Save & set as draft</button>
                  <button type="button" class="btn btn-success" id="active"><i class="fas fa-save"></i> Save & set as active</button>
                </div>
              </div>
              <input type="hidden" name="type" value=""/>
            </form>

          </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
{{-- <script src="{{base_url()}}assets/node_modules/ckeditor/ckeditor.js"></script> --}}
<script src="{{base_url()}}assets/node_modules/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="{{base_url()}}assets/node_modules/ace/ace.js"></script>
<script src="{{base_url()}}assets/myscript/instructor/assignment/add.js"></script>{{-- role/controller/file --}}

@endsection