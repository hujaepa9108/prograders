@extends('layouts.instructor.master')

@section('content')

{{-- DEPENDENCIES STUFF --}}
{{-- <link rel="stylesheet" href="{{base_url()}}assets/node_modules/xterm/css/xterm.css" />
<script src="{{base_url()}}assets/node_modules/xterm/lib/xterm.js"></script> --}}

<script type="text/javascript" src="{{base_url()}}assets/node_modules/moment/min/moment.min.js"></script>
<script type="text/javascript" src="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.css" />
<style>
.myMarker {
  position:absolute;
  background:rgba(100,200,100,0.5);
  z-index:20
}
.myMarker2 {
  position:absolute;
  background:red;
  z-index:20;
  opacity: 0.3;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{base_url()}}assignment/section">Section List</a></li>
                <li class="breadcrumb-item"><a href="{{base_url()}}assignment/list/{{$sectionId}}">Assignment List</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{base_url()}}assignment/detail/{{$sectionId}}/{{$assignmentId}}">{{$assignment->title}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$metricNo}} Assessment Summary</li>
            </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
          <input type="hidden" id="totalLine" value="{{count($line)}}">
              @php
                for($i=0;$i<count($line);$i++) {
                    echo "<input type='hidden' id='line_".$i."' value='".$line[$i]."'>";
                }
              @endphp
            <div class="row">
              <div class="col-sm-12">
                <a href="{{base_url()}}assignment/detail/{{$sectionId}}/{{$assignmentId}}" class="btn btn-default"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
                <hr>
                <h4 align="center"><u>&lt;{{$metricNo}}&gt; Assessment Detail</u></h4>
                <h5>Scoring Results</h5>
                <div class="table-responsive">
                    <table id="submission_list" class="table table-striped table-bordered thead-dark " style="width:100%">
                        <thead class="thead-light">
                            <tr>
                                <th>C1</th>
                                <th>C2</th>
                                <th>C3</th>
                                <th>C4</th>
                                <th>C5</th>
                                <th>Total Mark</th>
                                <th>Finalize Mark</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr align="center">
                            <td>{{$result->c1}}/{{$result->total_c1}}</td>
                                <td>
                                    @php
                                        if($result->total_c2==5){
                                            if($result->c2==0)
                                                echo "In-Progress";
                                            else
                                                echo $result->c2."/".$result->total_c2;
                                        }
                                        else
                                            echo "<b>-</b>";
                                    @endphp
                                </td>
                                <td>
                                    @php
                                        if($result->total_c3==5){
                                            if($result->c3==0)
                                                echo "In-Progress";
                                            else
                                                echo $result->c3."/".$result->total_c3;
                                        }
                                        else
                                            echo "<b>-</b>";
                                    @endphp
                                </td>
                                <td>
                                    {{$result->c4}}/4
                                </td>
                                <td>
                                    @if($result->c5==0)
                                        In-Progress
                                    @else
                                        {{$result->c5}}/5
                                    @endif
                                </td>
                                <td>{{$result->total_mark}}/{{$result->overall}}</td>
                                <td>
                                    @if($result->finalize_mark==0)
                                        In-Progress
                                    @else
                                        {{$result->total_mark}}/{{$result->overall}}
                                    @endif
                                </td>
                            </tr>
                        </tbody>  
                    </table>
                    </div>
                    <hr>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h5>Code Peformance</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Student's Source Code</label> 
                    {{-- <button class="btn btn-primary btn-sm" id="copy" data-toggle="tooltip" title="click to copy the code"><i class="fas fa-copy"></i> Copy</button> --}}
                    <div class="alert alert-success" id="showMsg">
                        <strong>Copied! </strong>
                    </div>
                    <div id="codeEditor" style="min-width: 200px; min-height: 450px;">{{$result->source_code}}</div>
                    {{-- <textarea id="clipboard"></textarea> --}}
                </div>
                <div class="col-sm-4">
                    <label for="">Student's Output</label>
                    <div id="output" style="min-width: 200px; min-height: 250px;">{{$result->output}}</div>
                    <div class="pt-2">
                    <span class="text-secondary font-weight-bolder">Output Match: {{$match}}/{{$countStdout}}</span>
                    @if($percent==100) 
                        (<span class="text-success font-weight-bold">{{$percent}}% Match</span>) 
                    @elseif($percent>0 && $percent<100)
                        (<span class="text-primary font-weight-bold">{{$percent}}% match</span>) 
                    @elseif($percent==0)
                        (<span class="text-danger font-weight-bold">{{$percent}}% Match</span>) 
                    @endif
                    
                    </div>
                </div>
                <div class="col-sm-4">
                    <label for="">Expected Output</label>
                    <div id="stdout" style="min-width: 200px; min-height: 250px;">{{$result->stdout}}</div>
                </div>  
              </div>
              
              <input type="hidden" id="line_error" value="{{$result->line_error}}">
          </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
{{-- <script src="{{base_url()}}assets/node_modules/tinymce/js/tinymce/tinymce.min.js"></script> --}}
<script src="{{base_url()}}assets/node_modules/ace/ace.js"></script>
{{-- <script src="{{base_url()}}assets/node_modules/jquery-countdown/jquery.countdown.js"></script> --}}
<script src="{{base_url()}}assets/myscript/student/submission/detail.js"></script>{{-- role/controller/file --}}

@endsection