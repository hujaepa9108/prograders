@extends('layouts.instructor.master')

@section('content')

{{-- DEPENDENCIES STUFF --}}
{{-- <link rel="stylesheet" href="{{base_url()}}assets/node_modules/xterm/css/xterm.css" />
<script src="{{base_url()}}assets/node_modules/xterm/lib/xterm.js"></script> --}}

<script type="text/javascript" src="{{base_url()}}assets/node_modules/moment/min/moment.min.js"></script>
<script type="text/javascript" src="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{base_url()}}assets/node_modules/daterangepicker/daterangepicker.css" />
<style>
.myMarker {
  position:absolute;
  background:rgba(100,200,100,0.5);
  z-index:20
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-1">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{base_url()}}assignment/section">Section List</a></li>
                  <li class="breadcrumb-item"><a href="{{base_url()}}assignment/list/{{$sectionId}}">Assignment List ({{$section->name}})</a></li>
                  <li class="breadcrumb-item" aria-current="page"><a href="{{base_url()}}assignment/detail/{{$sectionId}}/{{$assignmentId}}">{{$assignment->title}}</a></li>
                  <li class="breadcrumb-item active">Finalize Assessment</li>
                </ol>
            </nav>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
          <div class="invoice p-3 mb-3">
            <input type="hidden" id="totalLine" value="{{count($line)}}">
              @php
                for($i=0;$i<count($line);$i++) {
                    echo "<input type='hidden' id='line_".$i."' value='".$line[$i]."'>";
                }
              @endphp
              <a href="{{base_url()}}assignment/detail/{{$sectionId}}/{{$assignmentId}}" class="btn btn-default"><i class="nav-icon fas fa-arrow-circle-left"></i> Back</a>
              <hr>
            <form action="#" method="#" id="assessment">
            <div class="row">
                <div class="col-sm-6" style="height:800px; overflow-y: scroll; ">
                    
                    {{-- TAB1 SETTINGS --}}
                    <ul class="nav nav-tabs" id="tab1" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="question-tab" data-toggle="tab" href="#question" role="tab" aria-controls="question" aria-selected="true">Assignment Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="answer-tab" data-toggle="tab" href="#answer" role="tab" aria-controls="answer" aria-selected="false">Student's Answer</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="tab1-content">
                        <div class="tab-pane fade p-2" id="question" role="tabpanel" aria-labelledby="question-tab">
                            <h4><i>{{$assignment->title}}</i></h4>
                            <h5 class="text-info">Description</h5>
                            <div class="border border-muted p-3">
                                {!! $assignment->description !!}
                            </div>
                            <br>

                            <label for="">Answer's code</label>
                            <div id="answerCode" class="pr-3"style="min-width: 400px; height: 250px;">{{$assignment->answer_code}}</div>
                        </div>

                        <div class="tab-pane fade show active p-2" id="answer" role="tabpanel" aria-labelledby="answer-tab">

                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="">Student's Source Code</label> 
                                    <div id="codeEditor" class="pr-3"style="min-width: 400px; height: 250px;">{{$submission->source_code}}</div>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-sm-12">
                                    <label for="">Answer's Source Code</label> 
                                    <div id="answerCode" class="pr-3"style="min-width: 400px; height: 250px;">{{$assignment->answer_code}}</div>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="">Student's Output</label>
                                <div id="output" style="min-width: 150px; height: 250px;">{{$submission->output}}</div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="">Expected Output</label>
                                    <div id="stdout" style="min-width: 150px; height: 250px;">{{$assignment->output_test_case}}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pt-2">
                                        <span class="text-secondary font-weight-bolder">Output Match: {{$match}}/{{$countStdout}}</span>
                                        @if($percent==100) 
                                            (<span class="text-success font-weight-bold">{{$percent}}% Match</span>) 
                                        @elseif($percent>0 && $percent<100)
                                            (<span class="text-primary font-weight-bold">{{$percent}}% match</span>) 
                                        @elseif($percent==0)
                                            (<span class="text-danger font-weight-bold">{{$percent}}% Match</span>) 
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6" style="height:800px; overflow-y: scroll;"> 
                    {{-- TAB2 SETTINGS --}}
                    <ul class="nav nav-tabs" id="tab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="c1-tab" data-toggle="tab" href="#c1" role="tab" aria-controls="c1" aria-selected="true">C1 mark</a>
                        </li>

                        @if($grade_setup->c2_total_mark==0)
                            <li class="nav-item">
                                <a class="nav-link disabled" id="c2-tab" data-toggle="tab" href="#c2" role="tab" aria-controls="c2" aria-selected="false"><i class="fas fa-times-circle"></i> C2 mark</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" id="c2-tab" data-toggle="tab" href="#c2" role="tab" aria-controls="c2" aria-selected="false"> C2 mark</a>
                            </li>
                        @endif

                        @if($grade_setup->c3_total_mark==0)
                            <li class="nav-item">
                                <a class="nav-link disabled" id="c3-tab" data-toggle="tab" href="#c3" role="tab" aria-controls="c3" aria-selected="false"><i class="fas fa-times-circle"></i> C3 mark</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" id="c3-tab" data-toggle="tab" href="#c3" role="tab" aria-controls="c3" aria-selected="false"> C3 mark</a>
                            </li>
                        @endif

                        <li class="nav-item">
                            <a class="nav-link" id="c4-tab" data-toggle="tab" href="#c4" role="tab" aria-controls="c4" aria-selected="false">C4 mark</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="c5-tab" data-toggle="tab" href="#c5" role="tab" aria-controls="c5" aria-selected="false">C5 mark</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="finalize-tab" data-toggle="tab" href="#finalize" role="tab" aria-controls="finalize" aria-selected="false"><i class="fas fa-stamp"></i> Finalize mark</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="tab2-content">

                        {{-- CRITERIA 1 --}}
                        <div class="tab-pane fade p-2" id="c1" role="tabpanel" aria-labelledby="c1-tab">
                            <label for=""><b>Criteria 1: Ability to analyze problem and identify requirements</b></label>
                            <div class="table-responsive">
                                <table class="table table-bordered thead-dark" style="width:100%">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Sub-criteria</th>
                                        <th>Marks Scale</th>
                                        <th>Marks</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Unable to identify any input and output</b></td>
                                        <td>1</td>
                                        <td><input type="radio" name="c1" value="1" @if($submission->c1==1) checked @endif/></td>
                                    </tr>
                                    <tr>
                                        <td>Able to identify only one input and output</td>
                                        <td>2</td>
                                        <td><input type="radio" name="c1" value="2" @if($submission->c1==2) checked @endif/></td>
                                    </tr>
                                    @if($grade_setup->c1_total_mark>2)
                                        <tr>
                                            <td>
                                                Able to identify correctly some input and output <b>(50% match)</b>.<br/><i class="text-primary">*This applies only for output that has more than one line</i>
                                            </td>
                                            <td>3</td>
                                            <td><input type="radio" name="c1" value="3" @if($submission->c1==3) checked @endif/></td>
                                        </tr>
                                    @elseif($grade_setup->c1_total_mark==2)
                                        <tr class="bg-light">
                                            <td>
                                                Able to identify correctly some input and output <b>(1% &#8805; 50% match)</b>.<br/><i class="text-primary">*This applies only for output that has more than one line</i>
                                            </td>
                                            <td>3</td>
                                            <td class='text-danger font-weight-bolder'>X</td>
                                        </tr>
                                    @endif

                                    @if($grade_setup->c1_total_mark>2)
                                        <tr>
                                            <td>
                                                Able to identify correctly some input and output <b>(&gt;50% to &lt;100% match)</b><br/><i class="text-primary">*This applies only for output that has more than one line</i>
                                            </td>
                                            <td>4</td>
                                            <td><input type="radio" name="c1" value="4" @if($submission->c1==4) checked @endif/></td>
                                        </tr>
                                    @elseif($grade_setup->c1_total_mark==2)
                                        <tr class="bg-light">
                                            <td>
                                                Able to identify correctly some input and output <b>(&gt;50% to &lt;100% match)</b><br/><i class="text-primary">*This applies only for output that has more than one line</i>
                                            </td>
                                            <td>4</td>
                                            <td class='text-danger font-weight-bolder'>X</td>
                                        </tr>
                                    @endif

                                    @if($grade_setup->c1_total_mark>2)    
                                    <tr >
                                        <td>
                                            Able to identify correctly some input and output <b>(100% match)</b>.<br/><i class="text-primary">*This applies only for output that has more than one line</i>
                                        </td>
                                        <td>5</td>
                                        <td><input type="radio" name="c1" value="5" @if($submission->c1==5) checked @endif/></td>
                                    </tr>
                                    @elseif($grade_setup->c1_total_mark==2)
                                    <tr class="bg-light">
                                        <td>
                                            Able to identify correctly some input and output <b>(100% match)</b>.<br/><i class="text-primary">*This applies only for output that has more than one line</i>
                                        </td>
                                        <td>5</td>
                                        <td class='text-danger font-weight-bolder'>X</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            </div>
                        </div>

                        {{-- CRITERIA 2 --}}
                        @if($grade_setup->c2_total_mark==5)
                            <div class="tab-pane fade p-2" id="c2" role="tabpanel" aria-labelledby="c2-tab">
                                <span for="" class="c2-error font-italic text-danger"></span>
                                <label for=""><b>Criteria 2: Ability to apply the required data type (accurate variable declaration such as int,float,double, etc.), data structure (e.g usage of arrays) and Object-oriented Programming (e.g usage of objects, classes etc.)</b></label>
                                <div class="table-responsive">
                                    <table class="table table-bordered thead-dark" style="width:100%">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>Sub-criteria</th>
                                            <th>Marks Scale</th>
                                            <th>Marks</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Unable to identify any required data type/data structure/OOP</td>
                                            <td>1</td>
                                            <td><input type="radio" name="c2" value="1"/></td>
                                        </tr>
                                        <tr>
                                            <td>Able to identify some of the required data type/data structure/OOP</td>
                                            <td>2</td>
                                            <td><input type="radio" name="c2" value="2"/></td>                        	
                                        </tr>
                                        <tr>
                                            <td>Able to identify all of the required data type/data structure/OOP but does not produced correct output <b>(eg. compilation or logical error)</b></td>
                                            <td>3</td>
                                            <td><input type="radio" name="c2" value="3"/></td>                      	
                                        </tr>
                                        <tr>
                                            <td>Able to identify all of the required data type/data structure/OOP but only partial correct output were produced <b>(eg. minor wrong output format)</b></td>
                                            <td>4</td>
                                            <td><input type="radio" name="c2" value="4"/></td>                        	
                                        </tr>
                                        <tr>
                                            <td>Able to identify all of the required data type/data structure/OOP and does produce correct output  <b>(100% match)</b></td>
                                            <td>5</td>
                                            <td><input type="radio" name="c2" value="5"/></td>                       	
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        {{-- CRITERIA 3 --}}
                        @if($grade_setup->c3_total_mark==5)
                            <div class="tab-pane fade p-2" id="c3" role="tabpanel" aria-labelledby="c3-tab">
                                <span for="" class="c3-error font-italic text-danger"></span>
                                <label for=""><b>Criteria 3: Ability to apply required control structure(e.g if-statement, looping and so forth)</b></label>
                                <div class="table-responsive">
                                    <table class="table table-bordered thead-dark" style="width:100%">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Sub-criteria</th>
                                                <th>Marks Scale</th>
                                                <th>Marks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Unable to identify the required control structure</td>
                                                <td>1</td>
                                                <td><input type="radio" name="c3" value="1"/></td>
                                            </tr>
                                            <tr>
                                                <td>Able to identify some of the required control structure</td>
                                                <td>2</td>
                                                <td><input type="radio" name="c3" value="2"/></td>
                                            </tr>
                                            <tr>
                                                <td>Able to identify all of the required control structure but does not produced correct output <b>(eg. compilation or logical error)</b></td>
                                                <td>3</td>
                                                <td><input type="radio" name="c3" value="3"/></td>
                                            </tr>
                                            <tr>
                                                <td>Able to identify all of the required control structure but only partial correct output were produced <b>(eg. minor wrong output format)</b></td>
                                                <td>4</td>
                                                <td><input type="radio" name="c3" value="4"/></td>
                                            </tr>
                                            <tr>
                                                <td>Able to identify the required control structure and does produce correct output <b>(100% match)</b></td>
                                                <td>5</td>
                                                <td><input type="radio" name="c3" value="5"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        <div class="tab-pane fade p-2" id="c4" role="tabpanel" aria-labelledby="c4-tab">

                            <label for=""><b>Criteria 4: Ability to run the program without syntax and runtime error</b></label>
                            <div class="table-responsive">
                                <table class="table table-bordered thead-dark" style="width:100%">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Sub-criteria</th>
                                            <th>Marks Scale</th>
                                            <th>Marks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Unable to compile/run the program</td>
                                            <td>1</td>
                                            <td><input type="radio" name="c4" value="1" @if($submission->c4==1) checked @endif/></td>
                                        </tr>
                                        <tr>
                                            <td>Able to compile/run the program but have runtime error</td>
                                            <td>2</td>
                                            <td><input type="radio" name="c4" value="2" @if($submission->c4==2) checked @endif/></td>
                                        </tr>
                                        <tr>
                                            <td>Able to compile/run the program without runtime error but wrong output produced</td>
                                            <td>3</td>
                                            <td><input type="radio" name="c4" value="3" @if($submission->c4==3) checked @endif/></td>
                                        </tr>
                                        <tr>
                                            <td>Able to compile/run the program without runtime error and with correct output produced</td>
                                            <td>4</td>
                                            <td><input type="radio" name="c4" value="4" @if($submission->c4==4) checked @endif/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        {{-- CRITERIA 5 --}}
                        <div class="tab-pane fade p-2" id="c5" role="tabpanel" aria-labelledby="c5-tab">
                            <span for="" class="c5-error font-italic text-danger"></span>
                            <label for=""><b>Criteria 5: Ability to produce a readable code</b></label>
                            <div class="table-responsive">
                                <table class="table table-bordered thead-dark" style="width:100%">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Sub-criteria</th>
                                            <th>Marks Scale</th>
                                            <th>Marks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Poorly organized code <span class="text-secondary">(e.g no indentation)</span>,bad at naming conventions and no comments</td>
                                            <td>1</td>
                                            <td><input type="radio" name="c5" value="1"/></td>
                                        </tr>
                                        <tr>
                                            <td>The code is organized <span class="text-secondary">(e.g have indentation)</span> but still bad at naming conventions and no comments added</td>
                                            <td>2</td>
                                            <td><input type="radio" name="c5" value="2"/></td>
                                        </tr>
                                        <tr>
                                            <td>The code is organized, good naming conventions and but no comments added</td>
                                            <td>3</td>
                                            <td><input type="radio" name="c5" value="3"/></td>
                                        </tr>
                                        <tr>
                                            <td>The code is organized, good naming conventions and comments were added which makes the student's code easy to read</td>
                                            <td>4</td>
                                            <td><input type="radio" name="c5" value="4"/></td>
                                        </tr>
                                        <tr>
                                            <td>The code is extremely well organized, accurate naming conventions and very detail comments were added which makes the student's code really easy to read</td>
                                            <td>5</td>
                                            <td><input type="radio" name="c5" value="5"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="tab-pane fade show active p-2" id="finalize" role="tabpanel" aria-labelledby="Finalize-tab">
                            <label for="">Summary</label>
                            <div class="table-responsive">
                                <table id="submission_list" class="table table-striped table-bordered thead-dark " style="width:100%">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>C1</th>
                                            <th>C2</th>
                                            <th>C3</th>
                                            <th>C4</th>
                                            <th>C5</th>
                                            <th>Total Mark</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr align="center">
                                            <td class="font-weight-bolder"><span id="c1_mark">{{$submission->c1}}</span>/{{$grade_setup->c1_total_mark}}</td>
                                            @if($grade_setup->c2_total_mark!=0)
                                                <td class="font-weight-bolder"><span id="c2_mark" >{{$submission->c2}}</span>/{{$grade_setup->c2_total_mark}}</td>
                                            @else
                                                <td class="text-danger font-weight-bolder">X</td>
                                            @endif
                                            @if($grade_setup->c3_total_mark!=0)
                                                <td class="font-weight-bolder"><span id="c3_mark">{{$submission->c3}}</span>/{{$grade_setup->c3_total_mark}}</td>
                                            @else
                                                <td class="text-danger font-weight-bolder">X</td>
                                            @endif
                                            <td class="font-weight-bolder"><span id="c4_mark">{{$submission->c4}}</span>/4</td>
                                            <td class="font-weight-bolder"><span  id="c5_mark">{{$submission->c5}}</span>/5</td>
                                            <td class="font-weight-bolder"><span id="total_mark">{{$submission->total_mark}}</span>/{{$grade_setup->total_mark}}</td>
                                        </tr>
                                    </tbody>  
                                </table>
                            </div>
                            <label for="">Feedback(Optional)</label>
                            <textarea name="feedback" id="feedback"></textarea>
                            <div class="pt-2 pb-2">
                                <button class="btn btn-success" id="finalize"><i class="fas fa-stamp"></i> Finalize Mark</button>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
          </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="{{base_url()}}assets/myscript/instructor/submission/assessment.js"></script>
<script src="{{base_url()}}assets/node_modules/ace/ace.js"></script>
<script src="{{base_url()}}assets/node_modules/tinymce/js/tinymce/tinymce.min.js"></script>

@endsection