@extends('layouts.instructor.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Please select one of the section first</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8">
        <div class="invoice p-3 mb-3">
        <h4>{{ $title }}</h4>
        <hr>
        <div class="table-responsive">
            <table id="section_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>Section Name</th>
                        <th>Course</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
        </div>{{-- end of table responsive --}}
        <script src="{{ base_url() }}assets/myscript/section/list.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->

{{-- section-edit-modal --}}

<div class="modal fade" tabindex="-1" role="dialog" id="section-edit-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title"><i class='fas fa-pencil-alt'></i> Edit section label</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{base_url()}}section/editLabel/student">
      <div class="modal-body">
         <div class="form-group">
          <label for="">Enter the new section label</label>
          <input type="text" name="sectionLabel" id="sectionLabel" class="form-control" required/>
          <input type="hidden" name="sectionId" id="sectionId" value=""/>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>


{{-- <div class="modal fade" id="section-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form>
         <div class="form-group">
          <label for="">Enter new section label</label>
          <input type="text" name="sectionLabel" id="sectionLabel" class="form-control" required/>
          <input type="hidden" name="sectionId"/>
         </div>
      </form>
    </div>
  </div>
</div> --}}
@endsection

