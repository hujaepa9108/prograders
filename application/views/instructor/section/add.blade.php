@extends('layouts.instructor.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{ $title }}</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
        <div class="invoice p-3 mb-3">

          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>

          <div id="inst">
            <form role="form" id="add_section">
              <div class="card-body">

               <div class="form-group row">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">Section Name </label> 
                  <input type="text" id="section" name="section" placeholder="Enter section name" required="required" class="form-control">
                </div>
               </div>

               <div class="form-group">
                <label>Course</label>
                <select name="course" id="course" class="form-control" v-model="selected" @change="onchange">
                  <option value="">Please select course</option>
                  <option v-for="item in course" v-bind:value="item.ct_id">@{{item.ct_name}}</option>
                  <option value="addcourse" >Add New Course</option>
                </select>
                <span class="text-danger" v-if="message.length > 0"><b><i>@{{message}}</i></b></span>
              </div>


               <button type="submit" class="btn btn-success">Submit</button> 
               <button type="reset" class="btn btn-danger">Clear</button>

              </div>
           </form>
         </div>
          <script src="{{ base_url() }}assets/myscript/section/add.js"></script>
          
        </div>
      </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection