@extends('layouts.instructor.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-1">
        <div class="col-sm-12">
            <h5><i class="fas fa-edit"></i>Change password</h5>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
        <div class="invoice p-3 mb-3">
            <span id="error_msg" class="text-danger font-weight-bold font-italic"></span>
            <form action="{{base_url()}}auth/process" method="post" id="changePasswd">
                <div class="form-group">
                  <label for="password1">Enter Your New Password</label>
                  <input type="password" name="password1" class="form-control" placeholder="Enter your new password" id="password1">
                </div>
                <div class="form-group">
                  <label for="pwd">Enter Again Your New Password</label>
                  <input type="password" name="password2" class="form-control" placeholder="Enter again your new password" id="password2">
                </div>
                <button type="button" class="btn btn-success" id="change">Change</button>
            </form>
        </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="{{base_url()}}assets/myscript/instructor/auth.js"></script>
@endsection