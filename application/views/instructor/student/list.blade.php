@extends('layouts.instructor.master')

@section('content')
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>List of Students for {{ $section->section_label }} ({{ $course->course_code }})</h1>

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
        <div class="invoice p-3 mb-3">
              <a href="{{base_url()}}section/list/student" class="btn btn-default"><i class="far fa-arrow-alt-circle-left"></i> Back</a>
              <div class="float-right"><a href="{{base_url()}}student/add/{{$uri}}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Add</a></div>
          <hr>

          <div class="table-responsive">
            <table id="stud_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                <thead class="thead-light">
                    <tr>
                        <th>Name</th>
                        <th>Metric Number</th>
                        <th>Email</th>
                        <th>Added At</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>  
            </table>
          </div>{{-- end of table responsive --}}

        </div>{{-- end of invoice --}}
        </div>{{-- end of col --}}
      </div>{{-- end of row --}}
    </div>{{-- end of container-fluid --}}
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="{{base_url()}}assets/myscript/instructor/stud_list.js"></script>
@endsection