@extends('layouts.instructor.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Add New Student for {{ $section->section_label }} ({{ $course->course_code }})</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-8">
        <div class="invoice p-3 mb-3">
          <div id="overlay">
            <div id="overlay-text">Processing....</div>
          </div>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="manual-tab" data-toggle="tab" href="#manual" role="tab" aria-controls="manual" aria-selected="true">One by one</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="csv-tab" data-toggle="tab" href="#csv" role="tab" aria-controls="csv" aria-selected="true">Or CSV</a>
            </li>
          </ul>

        <div class="tab-content" id="myTabContent">

          
          <div class="tab-pane fade show active" id="manual" role="tabpanel" aria-labelledby="manual-tab">
           <div id="inst">
            <form role="form" id="add_stud_manual" method="post" novalidate="novalidate" action="{{ base_url() }}student/processadd">
              <div class="card-body">
                
                <div class="form-group">
                  <label>Student Email <sup>*</sup></label>
                  <input type="email" id="email" name="email" placeholder="Enter the student email" required="required" class="form-control">
                </div>

                <div class="form-group">
                  <label>Student Name <sup>*</sup></label> 
                  <input type="text" id="name" name="name" placeholder="Enter the student full name" required="required" class="form-control">
                </div>

                <div class="form-group">
                  <label>Student Metric Number<sup>*</sup></label> 
                  <input type="text" id="noId" name="noId" placeholder="Enter the student metric number" required="required" class="form-control">
                </div> 

                <div class="form-group">
                  <label>Student Gender<sup>*</sup></label> 
                  <select name="gender" id="gender" class="form-control" required>
                    <option value="">Please select</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>
                <input type="hidden" value="{{$uri}}" name="sectionid">

                <div class="form-group">
                  <button type="submit" class="btn btn-success">Submit</button> 
                  <button type="reset" class="btn btn-danger">Clear</button>
                  <div class="float-right">
                    <a href="{{base_url()}}student/list/{{$uri}}" class="btn btn-default"><i class="far fa-arrow-alt-circle-left"></i> Back</a>
                  </div>
                </div>
                
              </div>
            </form> 
          </div>
          </div>
          
          <div class="tab-pane fade" id="csv" role="tabpanel" aria-labelledby="csv-tab">
            ...
          </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="{{base_url()}}assets/myscript/instructor/stud_add.js"></script>
@endsection