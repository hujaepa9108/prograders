@extends('layouts.instructor.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h3>{{$course->code}} ({{$course->name}})</h3>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{$assignment->total}}</h3>
              <p>Total Assignments</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-bag"></i> -->
              <i class="fas fa-file-alt"></i>
            </div>
            <a href="{{base_url()}}assignment/section" class="small-box-footer">Go to assignment list <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        {{-- <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
                <h3>0</h3>
              <p>Total Student</p>
            </div>
            <div class="icon">
              <i class="fas fa-user-graduate"></i>
            </div>
            <a href="#" class="small-box-footer">Go to student list <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>      --}}

        <!-- ./col -->
      </div>
      
    </div>
      
      <div class="content">
        <div class="container-fluid">
          <div class="row mt-2">
            <div class="col-sm-12">
              <h4><i class="nav-icon fas fa-chart-line"></i> Class Performance</h4>
              <hr>
              {{-- WHITE BACKGROUND --}}
              <div class="invoice p-3 mb-3">
                
         
                <ul class="nav nav-tabs" id="section-tab" role="tablist">
                  @for($i=0; $i<count($fetch_sections);$i++)
                      <li class="nav-item">
                        <a class="nav-link @if($i==0)active @endif" id="section_{{$fetch_sections[$i]->section_id}}-tab" data-toggle="tab" href="#section_{{$fetch_sections[$i]->section_id}}">{{$fetch_sections[$i]->name}}</a>
                      </li>
                  @endfor
                </ul>

                <div class="tab-content" id="tab-content">
                  @for($i=0;$i<count($fetch_sections);$i++)
                    <div class="tab-pane fade @if($i==0)show active @endif pt-2" id="section_{{$fetch_sections[$i]->section_id}}">
                      <div class="table-responsive">
                        <table id="student_list" class="table table-striped table-bordered thead-dark" style="width:100%">
                          <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Student Metric No</th>
                                <th>Student Name</th>
                                <th>Highest Mark(%)</th>
                                <th>Assignment Title</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if(!empty($studentList[$i]))
                              @for($j=0;$j<count($studentList[$i]);$j++)
                              <tr>
                                  <td>@php $row=$j+1; echo $row;@endphp</td>
                                  <td>{{$studentList[$i][$j]->metric_no}}</td>
                                  <td>{{$studentList[$i][$j]->name}}</td>
                                  <td>{{$studentList[$i][$j]->finalize_mark}} ({{$studentList[$i][$j]->percentage}}%)</td>
                                  <td>{{$studentList[$i][$j]->title}}</td>
                              </tr>
                              @endfor
                            @else
                              <tr>
                                <td colspan='5' class='font-weight-bolder' align="center">No students assign in this section</td>
                              </tr>
                            @endif
                          </tbody>
                        </table>
                      </div>
                    </div>
                  @endfor
                  
                </div>

              </div>
              {{-- END OF WHITE BACKGROUND --}}
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
{{-- datatable stuff --}}
<link rel="stylesheet" href="{{ base_url() }}assets/node_modules/datatablesbs4/css/dataTables.bootstrap4.css">
<script src="{{ base_url() }}assets/node_modules/datatables/js/jquery.dataTables.js"></script>
<script src="{{ base_url() }}assets/node_modules/datatablesbs4/js/dataTables.bootstrap4.js"></script>
<script src="{{base_url()}}assets/myscript/instructor/dashboard/dashboard.js"></script>
@endsection