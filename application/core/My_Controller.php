<?php 

use Coolpraz\PhpBlade\PhpBlade;//namespace for coolpraz blade for CI

/*-This class is too handle generic functionality that can be applied to other sub-controller as well

List of functions:
<function_name> : <purpose>

getRole() : to get session role data.
generateToken() : to generate token which is used for login security.
validAuth() : redirect authenticate user to its respective dashboard.
-*/

class MY_Controller extends CI_Controller{

	protected $views = APPPATH."views";
	protected $cache = APPPATH."cache";
	protected $blade;

	public function __construct(){
		parent::__construct();
		$this->blade = new PhpBlade($this->views,$this->cache); //initiate blade object.
	}

	public function initiateStat() {
		return array('val'=>'',
			'msg'=>'',
			'status'=>''
		);
	}

	/*-This function is to generate token which is used for login security. Only those with the right token can have a valid authentication-*/
	public function generateToken($email) {
		return date("Ymd").$email;
	}

	/*-This return validates valid auth for authorization to access the dashboard.
	-*/
	public function validAuth()
	{
		// $email=$this->decrypt($_SESSION["email"]);//the email needs to be in original string first
		if(isset($_SESSION['email'])&& date("Ymd").$this->getEmail()==$this->decrypt($_SESSION['token']))
			return true;
		else
			return false;
	}

	public function encrypt($string)
	{
		$cipher_method="aes-128-ctr";
		$encrypt_key="rVR9ZpWy73#";
		$option=0;
		$encrypt_iv="1234567891011121";

		$crypted_str= openssl_encrypt($string, $cipher_method, $encrypt_key, $option, $encrypt_iv);

		return $crypted_str;
	}

	public function decrypt($crypted_str)
	{
		$cipher_method="aes-128-ctr";
		$decrypt_key="rVR9ZpWy73#";
		$option=0;
		$decrypt_iv="1234567891011121";

		$decrypted_str=openssl_decrypt ($crypted_str, $cipher_method,  
        $decrypt_key, $option, $decrypt_iv); 

        return $decrypted_str;
	}

	/*-This function is to get session email data decrypted-*/
	public function getEmail()
	{
		return $this->decrypt($_SESSION['email']);
	}

	/*-This function is to get session role data decrypted-*/
	public function getRole()
	{
		return $this->decrypt($_SESSION['role']);
	}
	//TIME AGO FUNCTION
	public function get_time_ago($time)
	{
		$time_difference = time() - $time;

		if( $time_difference < 1 ) { return 'less than 1 second ago'; }
			$condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
						30 * 24 * 60 * 60       =>  'month',
						24 * 60 * 60            =>  'day',
						60 * 60                 =>  'hour',
						60                      =>  'minute',
						1                       =>  'second'
			);

		foreach( $condition as $secs => $str )
		{
			$d = $time_difference / $secs;

			if( $d >= 1 )
			{
				$t = round( $d );
				return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
			}
		}
	}
}
?>