<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends My_Controller {
    public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model','auth_m');
		if(!$this->validAuth()) {
			redirect("landing");
		}
    }
    
    public function index()
    {
        //instructor
        if($this->getRole()==2){
            echo $this->blade->view()->make("instructor.auth");
        }
        //instructor
        else if($this->getRole()==3){
            echo $this->blade->view()->make("student.auth");
        }
    }

    public function process()
    {
        if(isset($_POST)) {
            extract($_POST);
            $status = $this->initiateStat();//intiate status
            $data =array(
                "password" => password_hash($password2,PASSWORD_DEFAULT),
                "userId" => $this->decrypt($_SESSION["userId"])

            );
            if($this->auth_m->changePasswd($data)) {
                echo "<script>alert('successfully changed password');</script>";
                redirect("dashboard");
            }
        }
    }
}