<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends My_Controller {

	public function index()
	{
		$this->session->sess_destroy();
		redirect('landing');
	}

}

/* End of file Logout.php */
/* Location: ./application/controllers/Logout.php */