<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Section_model','section_m');
		$this->load->library('form_validation');
		
		if(!$this->validAuth()){
			redirect("landing");
		}
	}

	public function editLabel($type)
	{
		if($this->getRole()==1) {
			$data = array(
				"section_label" => strtoupper($this->input->post('sectionLabel'))
			);
			$this->section_m->editLabel($data,$this->input->post('sectionId'));
			redirect("section/list/".$type);
		}
	}

	public function getSection()
	{
		$data["section"]=$this->section_m->getSection($this->decrypt($_SESSION['userid']));
		echo json_encode($data);
	}

	//-Data list stuff for section-//
	public function list($type)
	{
		if($this->getRole()==1) {
			$data['title']="List of Section";
			$data['type']=$type;
			echo $this->blade->view()->make("instructor.section.list",$data);
		}
	}

	public function processList($type)
	{
	   $userid=$this->decrypt($_SESSION['userid']);//just added
	   $fetch_data = $this->section_m->processListDB($userid);  
       $data = array();

       foreach($fetch_data as $row)  
       { 
	           	//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
			$butt_edit = "<button type='button' data-toggle='tooltip' title='Change Section Label' name='update' onclick='edit(".$row->section_id.",\"".$row->section_label."\")' class='btn btn-warning btn-sm'><i class='fas fa-pencil-alt'></i></button>";

			if($type=="student") {
				$butt_go = "<a data-toggle='tooltip' title='Go to student list' href=".base_url()."student/list/".base64_encode($row->section_id)." class='btn btn-primary btn-sm'><i class='far fa-arrow-alt-circle-right'></i></button>";
			}

			elseif ($type=="assignment") {
				$butt_go = "<a data-toggle='tooltip' title='Go to assignment list' href='".base_url()."assignment/list/".base64_encode($row->section_id)."' class='btn btn-primary btn-sm'><i class='far fa-arrow-alt-circle-right'></i></button>";
			}

			if($row->role_by==3)
				$created_by="ADMIN";
			else if($row->role_by==1) {
				//fetch the instructor name for created by column
				$obj = $this->section_m->getInstName($row->created_by);
				$created_by=$obj->ut_name;
			}

                $sub_array = array();   
                $sub_array[] = $row->section_label;  
                $sub_array[] = $row->ct_name;
                $sub_array[] = $butt_edit."&nbsp;&nbsp;"."&nbsp;&nbsp;".$butt_go;
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->section_m->processListCount($this->decrypt($_SESSION['userid'])),  
                "recordsFiltered" => $this->section_m->processListFiltered($this->decrypt($_SESSION['userid'])),  
                "data" => $data  
           );  
           echo json_encode($output);
	}
}

/* End of file Section.php */
/* Location: ./application/controllers/Section.php */