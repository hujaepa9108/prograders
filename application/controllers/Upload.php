<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends My_Controller {
    public function __construct() {
		parent::__construct();
		if(!$this->validAuth()){
			redirect("landing");
		}
    }
    
    public function tinymce_upload() {
        $this->load->library("upload");
        // print_r($_FILES);
		if(is_uploaded_file($_FILES["file"]['tmp_name'])) {
            $config['upload_path'] = "./assets/img/tinymce";
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2000;
            $config["file_name"] = time();
            $config["file_ext_tolower"] = true;
            $config["overwrite"] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file')) {
                $data['error'] = $this->upload->display_errors();
                // $this->load->view('update_gambar_profile',$data);
                echo json_encode($data);
                
            }
            //successfully uploaded
            else {
                 $data = $this->upload->data();
                 echo json_encode(array('location' => base_url()."assets/img/tinymce/".$data['file_name']));
            }
        }
	}
}
