<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends My_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->validAuth()){
			redirect("landing");
		}
		$this->load->library('form_validation');
		$this->load->model('Student_model','student_m');
	}

	//load add form
	public function add($sectionid)
	{
		if($this->getRole()==1) { 
			$data['course']=$this->student_m->getCourseCode(base64_decode($this->uri->segment(3)));
			$data['section']=$this->student_m->getSectionLabel(base64_decode($this->uri->segment(3)));
			$data['uri']=$this->uri->segment(3);
			echo $this->blade->view()->make("instructor.student.add",$data);
		}
		else{
			redirect("dashboard");
		}
	}

	public function checkExistEmail()
	{
		if($this->student_m->checkExistEmail($this->input->get('email'))>0)
			echo "false";
		else
			echo "true";
	}

	public function checkExistNoId()
	{
		if($this->student_m->checkExistNoId($this->input->get('noId'))>0)
			echo "false";
		else
			echo "true";
	}

	public function processadd() {

		if($this->getRole()==1) {
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('noId', 'noId', 'required');
			$this->form_validation->set_rules('gender', 'gender', 'required');
			
			// form validation
			if(!$this->form_validation->run())
				echo $this->blade->view()->make("instructor.student.add");

			else {
				extract($this->input->post());//extract input
				$status = $this->initiateStat();//intiate status
				$course= $this->student_m->getCourseId(base64_decode($sectionid));//fetch course id based on section id
				$dataStud=array(
					"ut_name"=> strtoupper($name),
					"ut_password"=> password_hash($noId,PASSWORD_DEFAULT),
					"ut_email"=> $email,
					"ut_id_no"=> $noId,
					"ut_role"=> 2,
					"role_name" => "Instructor",
					"ut_gender" => $gender,
					"ut_ct_id" =>$course->ct_id,
					"display_name"=>ucwords(strtolower($name)),
					"ut_created_by" => $this->decrypt($_SESSION['userid']),
					"ut_created_at"=> date('Y-m-d H:i')
				);

				$dataSect=array(
					"section_id" => base64_decode($sectionid)
				);

				// $dataSect=$this->student_m->getSectId($this->decrypt($_SESSION['userid']));

				if($this->student_m->add($dataStud,$dataSect)) {

					$status['val']=true;
					$status['msg']="Successfully added new student";
					$status['uri']=$sectionid;
					echo json_encode($status);
					// redirect('student/list/'.$sectionid);
				}

				else {
					$status['val']=false;
					$status['msg']="Fail to add student! Technical error with the server";
					echo json_encode($status);
				}
				
			}//end of else form validation
		}
	}//end of process add

	//-Data list stuff for student-//
	public function list($sectionId)
	{
		if($this->getRole()==1){
			$data['course']=$this->student_m->getCourseCode(base64_decode($this->uri->segment(3)));
			$data['section']=$this->student_m->getSectionLabel(base64_decode($this->uri->segment(3)));
			$data['uri']=$this->uri->segment(3);
			echo $this->blade->view()->make("instructor.student.list",$data);
		}
	}

	public function processList($sectionid)
	{
		if($this->getRole()==1) {
		$fetch_data = $this->student_m->processListDB($sectionid);  
           $data = array();
           foreach($fetch_data as $row)  
           { 
           		//SETTING THE BUTTONS FOR EDIT AND DELETE. FOR READABILITY
		$butt_edit = "<button type='button' data-toggle='tooltip' title='Edit Instructor Info' name='update' onclick='edit(".base64_encode($row->ut_id).",\"".$row->ut_name."\")' class='btn btn-warning btn-sm'><i class='fas fa-pencil-alt'></i></button>";

		$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete Instructor Info' name='update' onclick='remove(\"".base64_encode($row->ut_id)."\",\"".base64_encode($row->ut_name)."\",\"".$row->ut_id_no."\")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";

                $sub_array = array();   
                $sub_array[] = $row->ut_name;  
                $sub_array[] = $row->ut_id_no;
                $sub_array[] = $row->ut_email;
                $sub_array[] = "<i class='fas fa-clock'></i>&nbsp;".date("H:i:s",strtotime($row->ut_created_at))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->ut_created_at));
                $sub_array[] = $butt_edit."&nbsp;&nbsp;".$butt_delete;
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->student_m->processListCount($sectionid),  
                "recordsFiltered" => $this->student_m->processListFiltered($sectionid),  
                "data" => $data  
           );  
           echo json_encode($output);
		}
	}
	//delete student
	public function processDelete()
	{
		$status = $this->initiateStat();//intiate status
		$data =array(
			"ut_id" => base64_decode($this->input->post("studentId"))
		);
		if($this->student_m->deleteStud($data)) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}
}

/* End of file Student.php */
/* Location: ./application/controllers/Student.php */