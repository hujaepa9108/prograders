<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment extends My_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->validAuth()){
			redirect("landing");
		}
		$this->load->library('form_validation');
		$this->load->model("Assignment_model", "assignment_m");
	}

	/*
		Description: to load student assignment list view
		Roles: stud
	*/
	public function all()
	{
		if($this->getRole()==3){
			$data["title"]="Assignment List";
			$data["section"]=$this->assignment_m->getSectionInfo($this->decrypt($_SESSION["userId"]));
			// echo $data["section"]->id;			
			// die();
			echo $this->blade->view()->make("student.assignment.list",$data);
		}
	}

	/*
		Description: to load section list view
		Roles: inst
	*/
	public function section()
	{
		if($this->getRole()==2) {
			$data['title']=ucwords("section list");
			echo $this->blade->view()->make("instructor.assignment.section",$data);
		}
	}

	/*
		Description: to load view assignment details for instructor/student
		Roles: inst, stud
	*/
	public function detail($sectionId,$id)
	{
		//for instructors
		if($this->getRole()==2) {
			$data["section"]=$this->assignment_m->getSectionDetail(base64_decode($sectionId));
			$data["assignment"]=$this->assignment_m->getDetail(base64_decode($id));
			$data["sectionId"]=base64_decode($sectionId);
			$data["assignmentId"]=$id;
			$langId=$this->assignment_m->getLangId(base64_decode($sectionId));
			$data["langId"]=(!empty($langId->pl_id)) ? $langId->pl_id : null;
			//GET LIST OF STUDENTS ALONG WITH LATEST SUBMISSION FOR EVERY EACH OF THEM
			$data["student"]=$this->assignment_m->getStudent(base64_decode($sectionId),base64_decode($id));
			$data["setup"]=$this->assignment_m->getSetup(base64_decode($id));
			for($i=0; $i<count($data["student"]); $i++){
				$data["result"][$i]=$this->assignment_m->getStudentResult(base64_decode($id),$data["student"][$i]->id);
			}
			// print_r($data["result"]);
			echo $this->blade->view()->make("instructor.assignment.detail",$data);
		}
		
		//for student
		else if($this->getRole()==3){
			$data["section"]=$this->assignment_m->getSectionDetail(base64_decode($sectionId));
			$data["assignment"]=$this->assignment_m->getDetail(base64_decode($id));
			$data["assignmentId"]=$id;
			$data["submission"]=$this->assignment_m->countSubmission(base64_decode($id),$this->decrypt($_SESSION['userId']));
			$data["sectionId"]=base64_decode($sectionId);
			$langId=$this->assignment_m->getLangId(base64_decode($sectionId));
			$data["langId"]=(!empty($langId->pl_id)) ? $langId->pl_id : null;
			$data["submitData"]=$this->assignment_m->getSubmitData($this->decrypt($_SESSION['userId']),base64_decode($id));
			if(!empty($data["submitData"]))
				$data["submitOn"]=$this->get_time_ago(strtotime($data["submitData"][0]->submitted_at));
			echo $this->blade->view()->make("student.assignment.detail",$data);
		}
	}
	
	/*
		Description: to load assignment add form view
		Roles: inst
	*/
	public function add()
	{
		if($this->getRole()==2) { 
			//REQUIRES COURSE ID, PROGRAMMING LANGUAGE & ITS ID AS WELL
			
			echo $this->blade->view()->make("instructor.assignment.add");
		}
	}

	/*
		Description: to process assignment add
		Roles: inst
	*/
	public function processAdd()
	{
		extract($_POST);
		$status = $this->initiateStat();
		$assignCode=date("Ymd")."_".str_replace(" ","_",$title);
		if(!empty($duedate)){
			$arrDates=explode("-", $duedate);
			$startDateTime=date("Y-m-d H:i:s",strtotime(str_replace("/","-",$arrDates[0])));
			$endDateTime=date("Y-m-d H:i:s",strtotime(str_replace("/","-",$arrDates[1])));
		}
		else{
			$startDateTime=null;
			$endDateTime=null;
		}

		$total_mark=null;
		$c2=null;
		$c3=null;
		// TOTAL SCORE CONDITIONS
		if(!empty($output) && !empty($source_code)) {
			$total_mark=24;
			$countOutput = count(explode("\n",trim($output)));

			//Item1
			if($countOutput==1) {
				$total_mark-=3;
				$c1=2;
			}
			else if($countOutput>1) {
				$c1=5;
			}
			
			if(isset($item_2)) {
				//Item2
				if($item_2==0){
					$total_mark-=5;
					$c2=0;
				}
				else if($item_2==1){
					$c2=5;
				}
			}

			if(isset($item_3)) {
				//Item3
				if($item_3==0) {
					$total_mark-=5;
					$c3=0;
				}
				else if($item_3==1) {
					$c3=5;
				}
			}

		}

		$data = array(
			"title"=> strtoupper($title),
			"description"=> $content,
			"source_code"=>$source_code,
			"input_test_case" => $input,
			"output_test_case"=>$output,
			"status" => strtoupper($type),
			"start_date" =>$startDateTime,
			"end_date" =>$endDateTime,
			"created_at"=> date('Y-m-d H:i:s'),
			"created_by" => $this->decrypt($_SESSION['userId'])
		);
		$data2 = array(
			"item_1"=>(isset($c1) && !empty($c1)) ? $c1 : null
			,
			"item_2"=>$c2,
			"item_3"=>$c3,
			"total_mark"=>(isset($total_mark) && !empty($total_mark)) ? $total_mark : null,
		);

		if(!isset($sectionId)|| empty($sectionId))
			$sectionId=null;

		if($this->assignment_m->processAdd($data,$data2,$sectionId)){
			
			$status['val']=true;
			if($type=="draft")
				$status['msg']="Successfully created new assignment (Drafted)";
			else if($type=="active")
				$status['msg']="Successfully created new assignment (Active)";
		}	
		echo json_encode($status);
	}

	/*
		Description: to process assignment edit
		Roles: inst
	*/
	public function processEdit($sectionIdOri,$assignmentIdOri)
	{
		extract($_POST);
		// print_r($_POST);
		// die();
		$status = $this->initiateStat();
		$assignCode=date("Ymd")."_".str_replace(" ","_",$title);
		if(!empty($duedate)){
			$arrDates=explode("-", $duedate);
			$startDateTime=date("Y-m-d H:i:s",strtotime(str_replace("/","-",$arrDates[0])));
			$endDateTime=date("Y-m-d H:i:s",strtotime(str_replace("/","-",$arrDates[1])));
		}
		else{
			$startDateTime=null;
			$endDateTime=null;
		}

		$total_mark=null;
		$c2=null;
		$c3=null;
		// TOTAL SCORE CONDITIONS
		if(!empty($output) && !empty($source_code)){
			$total_mark=24;
			$countOutput = count(explode("\n",trim($output)));

			//Item1
			if($countOutput==1){
				$total_mark-=3;
				$c1=2;
			}
			else if($countOutput>1){
				$c1=5;
			}

			//item2
			if(isset($item_2)) {
				//Item2
				if($item_2==0){
					$total_mark-=5;
					$c2=0;
				}
				else if($item_2==1){
					$c2=5;
				}
			}
			
			//Item3
			if(isset($item_3)) {
				if($item_3==0) {
					$total_mark-=5;
					$c3=0;
				}
				else if($item_3==1) {
					$c3=5;
				}
			}

		}

		$data = array(
			"title"=> strtoupper($title),
			"description"=> $content,
			"source_code"=>$source_code,
			"input_test_case" => $input,
			"output_test_case"=>$output,
			"status" => strtoupper($type),
			"start_date" =>$startDateTime,
			"end_date" =>$endDateTime,
			"modified_at"=> date('Y-m-d H:i:s'),
			"created_by" => $this->decrypt($_SESSION['userId'])
		);

		$data2 = array(
			"item_1"=>(isset($c1) && !empty($c1)) ? $c1 : null
			,
			"item_2"=> $c2,
			"item_3"=> $c3,
			"total_mark"=>(isset($total_mark) && !empty($total_mark)) ? $total_mark : null,
		);

		if(!isset($sectionId)|| empty($sectionId))
			$sectionId=null;

		if($this->assignment_m->processEdit($data,$data2,$sectionId,base64_decode($sectionIdOri),base64_decode($assignmentIdOri))){
			
			$status['val']=true;
			if($type=="draft")
				$status['msg']="Successfully edited";
			else if($type=="active")
				$status['msg']="Successfully change assignment to active status";
		}	
		echo json_encode($status);
	}

	public function getCourseInfo(){
		$data["course"]=$this->assignment_m->getCourseInfo($this->decrypt($_SESSION["userId"]));
		echo json_encode($data);
	}

	/*
		Description: to load assignment list (all) for instructor view
		Roles: inst
	*/
	public function processAllList($sectionId)
	{
		if($this->getRole()==2) {
		   $sectionId=base64_decode($sectionId);
		   // $userId=$this->decrypt($_SESSION["userId"]);
		   $fetch_data=$this->assignment_m->allListDB($sectionId,$this->decrypt($_SESSION["userId"]));
		   $student=$this->assignment_m->getTotalStudent($sectionId);
		   $data = array();
           foreach($fetch_data as $row) { 

				//calculate date/time left
				$now=strtotime(date("h:i A d-M-Y"));
				if(strtotime($row->end_date) < $now){
						$timeLeft="Has Passed Due Date";
				}
				else if(strtotime($row->start_date) <= $now){
						$current = new DateTime(date("h:i A d-M-Y"));
						$timeDiff = $current->diff(new DateTime($row->end_date));
						$timeLeft=$timeDiff->days."d ".$timeDiff->h." h ".$timeDiff->i."m ".$timeDiff->s."s left";
				}
				else {
						$timeLeft="Has not started yet";
				}

				$cleantitle=str_replace("\"","",str_replace("'", "", $row->title));
				$butt_details = "<a href='".base_url()."assignment/detail/".base64_encode($sectionId)."/".base64_encode($row->id)."' data-toggle='tooltip' title='More Details' name='view' class='btn btn-info btn-sm'><i class='right fas fa-chevron-circle-right'></i></a>";
				$status=($row->status=="ACTIVE") ? "<span class='text-success'><i class='far fa-check-circle'></i> ".$row->status : "<span class='text-warning'><i class='far fa-edit'></i> ".$row->status;

				$butt_edit = "<a href='".base_url()."assignment/edit/".base64_encode($sectionId)."/".base64_encode($row->id)."' data-toggle='tooltip' title='Edit Assignment' name='edit' class='btn btn-warning btn-sm'><i class='fas fa-edit'></i></a>";

				$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete Assignment' name='delete' class='btn btn-danger btn-sm' onclick='removeAssgmt(\"".base64_encode($row->id)."\",\"".$cleantitle."\")'><i class='fas fa-trash-alt'></i></button>";
				$finalize=$this->assignment_m->getTotalFinalize($row->id,$sectionId);
                $sub_array = array();  
                $sub_array[] = ""; 
                $sub_array[] = $row->title;
                $sub_array[] = "<b>".$status."</b>";//status
                $sub_array[] = $finalize->total."/".$student->total;//total submission 
				$sub_array[] = $this->get_time_ago(strtotime($row->created_at));
                if($row->status=="ACTIVE") {
					$sub_array[] ="<i class='fas fa-clock'></i>&nbsp;".date("h:i A",strtotime($row->start_date))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->start_date));//due date time start
					$sub_array[] ="<i class='fas fa-clock'></i>&nbsp;".date("h:i A",strtotime($row->end_date))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->end_date));//due date time end
					$sub_array[] = $timeLeft;//time left
					$sub_array[] = $butt_delete."&nbsp;&nbsp;".$butt_details;//button details
				}
                else{
					$sub_array[] ="<p class='font-weight-bold' align='center'>-</p>";
					$sub_array[] ="<p class='font-weight-bold' align='center'>-</p>";
					$sub_array[] ="<p class='font-weight-bold' align='center'>-</p>";
                	$sub_array[] = $butt_delete."&nbsp;&nbsp;".$butt_edit;//button edit
				}
                $data[] = $sub_array;  
           }//end of foreach
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->assignment_m->allListCount($sectionId,$this->decrypt($_SESSION["userId"])),  
                "recordsFiltered" => $this->assignment_m->allListFiltered($sectionId,$this->decrypt($_SESSION["userId"])),  
                "data" => $data  
           );  
           echo json_encode($output);
		}
	}

	/*
		Description: to load assignment list (active) for student
		Roles: stud
	*/
	public function processStudList($sectionId)
	{
		if($this->getRole()==3) {
		   $sectionId=base64_decode($sectionId);
		   // $userId=$this->decrypt($_SESSION["userId"]);
		   $fetch_data=$this->assignment_m->allStudListDB($sectionId);
		   $data = array();
           foreach($fetch_data as $row) { 
			   //calculate date/time left
			   $now=strtotime(date("h:i A d-M-Y"));
			   if(strtotime($row->end_date) < $now){
				   	$timeLeft="Has Passed Due Date";
			   }
			   else if(strtotime($row->start_date) <= $now){
					$current = new DateTime(date("h:i A d-M-Y"));
					$timeDiff = $current->diff(new DateTime($row->end_date));
					$timeLeft=$timeDiff->days."d ".$timeDiff->h." h ".$timeDiff->i."m ".$timeDiff->s."s left";
			   }
			   else {
					$timeLeft="Has not started yet";
			   }


			   $status=$this->assignment_m->getSubmitStat($row->id,$this->decrypt($_SESSION["userId"]));
			
			   $mark=$this->assignment_m->getGradeSetup($row->id);
			   
				//check finalize stuff
				if($status->total_submit==0){
					$statmsg="No submission has been made";
				}
				else if($status->total_submit>0) {
					$statmsg="In-Progress";
					$finalizestat=$this->assignment_m->getFinalizeStat($row->id,$this->decrypt($_SESSION["userId"]));
					if(isset($finalizestat->total_submit) && $finalizestat->total_submit > 0) {
						$statmsg=$finalizestat->finalize_mark."/".$mark->total_mark;
					}
				}
				$butt_details = "<a href='".base_url()."assignment/detail/".base64_encode($sectionId)."/".base64_encode($row->id)."' data-toggle='tooltip' title='More Details' name='view' class='btn btn-info btn-sm'><i class='right fas fa-chevron-circle-right'></i></a>";
                $sub_array = array();  
                $sub_array[] = ""; 
                $sub_array[] = $row->title."(".$sectionId.")";
				$sub_array[] = "<i class='fas fa-clock'></i>&nbsp;".date("h:i A",strtotime($row->start_date))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->start_date));				
				$sub_array[] = "<i class='fas fa-clock'></i>&nbsp;".date("h:i A",strtotime($row->end_date))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->end_date)); //due date/time
				$sub_array[] = $timeLeft;//time left
				$sub_array[] = $statmsg;"No submission has been made";//finalize Mark
                // $sub_array[] = "";//status
				$sub_array[] = $butt_details;//button details
                $data[] = $sub_array;  
           }//end of foreach
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->assignment_m->allStudListCount($sectionId),  
                "recordsFiltered" => $this->assignment_m->allStudListFiltered($sectionId),  
                "data" => $data  
           );  
           echo json_encode($output);
		}
	}

	/*
		Description: to check existing assignment title based on the instructor id during add assignment form process
		Roles: inst
	*/
	public function checkExistTitle()
	{
		if($this->getRole()==2){
			if( $this->assignment_m->checkExistTitle($this->decrypt($_SESSION['userId']),$_GET["title"]) > 0)
				echo "false";
			else
				echo "true";	
		}
	}

	public function processSectionList()
	{	
		if($this->getRole()==2) {
		   $userId=$this->decrypt($_SESSION["userId"]);
		   $fetch_data=$this->assignment_m->sectionListDB($userId);
		   $data = array();
           foreach($fetch_data as $row) { 

           		$student=$this->assignment_m->getTotalStudent($row->id);
           		$assignment=$this->assignment_m->getTotalAssignment($row->id);

				$butt_details = "<a href='".base_url()."assignment/list/".base64_encode($row->id)."' data-toggle='tooltip' title='Go to assignment list' name='view' class='btn btn-info btn-sm'><i class='fas fa-chevron-circle-right'></i></a>";

                $sub_array = array();  
                $sub_array[] =""; 
                $sub_array[] = $row->name;
                $sub_array[] = $student->total; 
                $sub_array[] = $assignment->total;
                $sub_array[] = $butt_details;
                $data[] = $sub_array;  
           }//end of foreach
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->assignment_m->sectionListCount($userId),  
                "recordsFiltered" => $this->assignment_m->sectionListFiltered($userId),  
                "data" => $data  
           );  
           echo json_encode($output);
		}//end of if role inst
	}
	
	public function getSections()
	{
		$section=$this->assignment_m->getSections($this->decrypt($_SESSION['userId']));
		echo json_encode($section);
	}

	public function list($sectionId)
	{
		$data['title']="Assignment List";
		$data['sectionId']=$sectionId;
		$data["section"]=$this->assignment_m->getSectionDetail(base64_decode($sectionId));
		$all=$this->assignment_m->countAll(base64_decode($sectionId));
		$draft=$this->assignment_m->countDraft(base64_decode($sectionId));
		$active=$this->assignment_m->countActive(base64_decode($sectionId));
		$data['all']=$all->total;
		$data['draft']=$draft->total;
		$data['active']=$active->total;
		echo $this->blade->view()->make("instructor.assignment.list",$data);
	}

	public function processDraftList($sectionId)
	{	
		if($this->getRole()==2) {
		   $userId=$this->decrypt($_SESSION["userId"]);
		   $fetch_data=$this->assignment_m->draftListDB(base64_decode($sectionId),$this->decrypt($_SESSION["userId"]));
		   $data = array();
           foreach($fetch_data as $row) { 
				$butt_details = "<a href='".base_url()."assignment/detail/".base64_encode($sectionId)."/".base64_encode($row->id)."' data-toggle='tooltip' title='More Details' name='view' class='btn btn-info btn-sm'><i class='right fas fa-chevron-circle-right'></i></a>";
				$status=($row->status=="ACTIVE") ? "<span class='text-success'><i class='far fa-check-circle'></i> ".$row->status : "<span class='text-warning'><i class='far fa-edit'></i> ".$row->status;
				$butt_edit = "<a href='".base_url()."assignment/edit/".$sectionId."/".base64_encode($row->id)."' data-toggle='tooltip' title='Edit Assignment' name='edit' class='btn btn-warning btn-sm'><i class='fas fa-edit'></i></a>";
				$sub_array = array();  
				$sub_array[] = ""; 
				$sub_array[] = $row->title;
				// $sub_array[] = "<b>".$status."</b>";//status
				// $sub_array[] = "/total student";//total submission 
				$sub_array[] = $this->get_time_ago(strtotime($row->created_at));
				$sub_array[] = $butt_edit;//button details
				$data[] = $sub_array;    
           }//end of foreach
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->assignment_m->draftListCount(base64_decode($sectionId),$userId),  
                "recordsFiltered" => $this->assignment_m->draftListFiltered(base64_decode($sectionId),$userId),  
                "data" => $data  
           );  
           echo json_encode($output);
		}//end of if role inst
	}

	public function rubric()
	{
		if($this->getRole()==2) {
			// $data["title"]="Assignment's Standard Rubric";
			echo $this->blade->view()->make("instructor.assignment.rubric2");
		}
		if($this->getRole()==3) {
			// $data["title"]="Assignment's Standard Rubric";
			echo $this->blade->view()->make("student.assignment.rubric2");
		}
	}

	public function draft($sectionId)
	{
		if($this->getRole()==2) {
			$data['title']="Assignment List";
			$data['sectionId']=$sectionId;
			$data["section"]=$this->assignment_m->getSectionDetail(base64_decode($sectionId));
			$all=$this->assignment_m->countAll(base64_decode($sectionId));
			$draft=$this->assignment_m->countDraft(base64_decode($sectionId));
			$active=$this->assignment_m->countActive(base64_decode($sectionId));
			$data['all']=$all->total;
			$data['draft']=$draft->total;
			$data['active']=$active->total;
			echo $this->blade->view()->make("instructor.assignment.draft",$data);
		}
	}
	public function active($sectionId)
	{
		if($this->getRole()==2) {
			$data['title']="Assignment List";
			$data['sectionId']=$sectionId;
			$data["section"]=$this->assignment_m->getSectionDetail(base64_decode($sectionId));
			$all=$this->assignment_m->countAll(base64_decode($sectionId));
			$draft=$this->assignment_m->countDraft(base64_decode($sectionId));
			$active=$this->assignment_m->countActive(base64_decode($sectionId));
			$data['all']=$all->total;
			$data['draft']=$draft->total;
			$data['active']=$active->total;
			echo $this->blade->view()->make("instructor.assignment.active",$data);
		}
	}
	public function processActiveList($sectionId)
	{	
		if($this->getRole()==2) {
		   $userId=$this->decrypt($_SESSION["userId"]);
		   $fetch_data=$this->assignment_m->activeListDB(base64_decode($sectionId),$this->decrypt($_SESSION["userId"]));
		   $student=$this->assignment_m->getTotalStudent(base64_decode($sectionId));
		   $data = array();
           foreach($fetch_data as $row) { 
			   	//calculate date/time left
				$now=strtotime(date("h:i A d-M-Y"));
				if(strtotime($row->end_date) < $now){
						$timeLeft="Has Passed Due Date";
				}
				else if(strtotime($row->start_date) <= $now){
						$current = new DateTime(date("h:i A d-M-Y"));
						$timeDiff = $current->diff(new DateTime($row->end_date));
						$timeLeft=$timeDiff->days."d ".$timeDiff->h." h ".$timeDiff->i."m ".$timeDiff->s."s left";
				}
				else {
						$timeLeft="Has not started yet";
				}

				$butt_details = "<a href='".base_url()."assignment/detail/".$sectionId."/".base64_encode($row->id)."' data-toggle='tooltip' title='More Details' name='view' class='btn btn-info btn-sm'><i class='right fas fa-chevron-circle-right'></i></a>";
				$status=($row->status=="ACTIVE") ? "<span class='text-success'><i class='far fa-check-circle'></i> ".$row->status : "<span class='text-warning'><i class='far fa-edit'></i> ".$row->status;

				$cleantitle=str_replace("\"","",str_replace("'", "", $row->title));
				
				$butt_delete = "<button type='button' data-toggle='tooltip' title='Delete Assignment' name='delete' class='btn btn-danger btn-sm' onclick='removeAssgmt(\"".base64_encode($row->id)."\",\"".$cleantitle."\")'><i class='fas fa-trash-alt'></i></button>";
				$finalize=$this->assignment_m->getTotalFinalize($row->id,base64_decode($sectionId));
				$sub_array = array();  
				$sub_array[] = ""; 
				$sub_array[] = $row->title;
				$sub_array[] = "<b>".$status."</b>";//status
				if($row->status=="ACTIVE")
					$sub_array[] = $finalize->total."/".$student->total;//total submission 
				else
					$sub_array[] ="-";
				$sub_array[] = $this->get_time_ago(strtotime($row->created_at));

				if($row->status=="ACTIVE") {
					$sub_array[] ="<i class='fas fa-clock'></i>&nbsp;".date("h:i A",strtotime($row->start_date))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->start_date));//due date time start
					$sub_array[] ="<i class='fas fa-clock'></i>&nbsp;".date("h:i A",strtotime($row->end_date))."&nbsp;&nbsp;<i class='fas fa-calendar-alt'></i>&nbsp;".date("d-M-Y",strtotime($row->end_date));//due date time end
					$sub_array[] =$timeLeft;//time left
					$sub_array[] = $butt_delete."&nbsp;&nbsp;".$butt_details;//button details
				}

				else{
					$sub_array[] ="<p class='font-weight-bold' align='center'>-</p>";
					$sub_array[] ="<p class='font-weight-bold' align='center'>-</p>";
					$sub_array[] ="<p class='font-weight-bold' align='center'>-</p>";
					$sub_array[] = $butt_delete."&nbsp;&nbsp;".$butt_edit;//button edit
				}
				$data[] = $sub_array;     
           }//end of foreach
           $output = array(  
                "draw" => intval($_POST["draw"]),  
                "recordsTotal" => $this->assignment_m->activeListCount(base64_decode($sectionId),$userId),  
                "recordsFiltered" => $this->assignment_m->activeListFiltered(base64_decode($sectionId),$userId),  
                "data" => $data  
           );  
           echo json_encode($output);
		}//end of if role inst
	}

	/*
		Description: to delete assignment
		Roles: inst
	*/
	public function procDelete()
	{
		$status = $this->initiateStat();//intiate status
		if($this->assignment_m->procDelete(base64_decode($_POST['assignmentId']))) {
			$status['val']=true;
			$status['msg']="success";
		}

		else {
			$status['val']=false;
			$status['msg']="fail!";
		}
		echo json_encode($status);
	}

	//jgn lupa setel setup
	public function edit($sectionId,$assignmentId)
	{
		if($this->getRole()==2) { 
			//REQUIRES COURSE ID, PROGRAMMING LANGUAGE & ITS ID AS WELL
			$data["assignment"]=$this->assignment_m->getDetailDraft(base64_decode($sectionId),base64_decode($assignmentId));
			$data['setup']=$this->assignment_m->getGradeSetup(base64_decode($assignmentId));
			$data['section']=$this->assignment_m->getAssignedSection(base64_decode($assignmentId));
			$data['sectionList']=$this->assignment_m->getSections($this->decrypt($_SESSION['userId']));
			echo $this->blade->view()->make("instructor.assignment.edit",$data);
		}
	}

	public function past($sectionId,$assignmentId)
	{
		if($this->getRole()==3) { 
			$data["sectionId"]=$sectionId;
			$data["assignmentId"]=$assignmentId;
			$data["assignment"]=$this->assignment_m->getTitle(base64_decode($assignmentId));
			echo $this->blade->view()->make("student.assignment.past",$data);
		}
	}
	public function procPastSub($assignmentId)
	{
		if($this->getRole()==3) {
			$setup = $this->assignment_m->getGradeSetup(base64_decode($assignmentId));
			$fetch_data=$this->assignment_m->pastDB($this->decrypt($_SESSION["userId"]),base64_decode($assignmentId));
			$data = array();
			foreach($fetch_data as $row) { 
				$sub_array = array();  
                $sub_array[] = ""; 
                $sub_array[] = $this->get_time_ago(strtotime($row->submitted_at));		
				$sub_array[] = $row->total_mark."/".$setup->total_mark;
				$sub_array[] = $row->finalize_mark."/".$setup->total_mark;//finalize Mark
				$sub_array[] = "<textarea readonly style='background-color: black; color:#28A745;'>".$row->source_code."</textarea>";
          		$sub_array[] = "<textarea readonly class='form-control'  style='background-color: black; color:#fff;'>".$row->output."</textarea>";
                $data[] = $sub_array;  
            }//end of foreach
			$output = array(  
					"draw" => intval($_POST["draw"]),  
					"recordsTotal" => $this->assignment_m->pastListCount($this->decrypt($_SESSION["userId"]),base64_decode($assignmentId)),  
					"recordsFiltered" => $this->assignment_m->pastListFiltered($this->decrypt($_SESSION["userId"]),base64_decode($assignmentId)),  
					"data" => $data  
			);  
			echo json_encode($output);
		}
	}

	public function downloadCSV()
	{
		$sectionId=$this->input->post('sectionId');
		$assignmentId=$this->input->post('assignmentId');
	 	// get data
		$student=$this->assignment_m->getStudent($sectionId,base64_decode($assignmentId));
		$setup=$this->assignment_m->getSetup(base64_decode($assignmentId));
		for($i=0; $i<count($student); $i++){
			$result[$i]=$this->assignment_m->getStudentResult(base64_decode($assignmentId),$student[$i]->id);
		}

		//file creation
		$file_name = 'student_details_on_'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$file_name"); 
		header("Content-Type: application/csv;");
		$file = fopen('php://output', 'w');
		$header = array("No","Student Metric No","Student Name","Total Mark","Finalize Mark"); 
		fputcsv($file, $header);//setout the header

		//looping the data
		$studData = array();
		$row=0;
		for ($i = 0; $i <count ($student); $i++){
			$studData[$i]["no"]=++$row;
			$studData[$i]["metric_no"]=$student[$i]->metric_no;
			$studData[$i]["name"]=$student[$i]->name;
			if(!empty($result[$i])){
				for($r=0;$r<count($result[$i]);$r++){
					if(isset($result[$i][$r]->total_mark) && $result[$i][$r]->total_mark!=0)
						$studData[$i]["total_mark"]="'".$result[$i][$r]->total_mark."/".$setup->total_mark;
					if(isset($result[$i][$r]->finalize_mark) && $result[$i][$r])
						$studData[$i]["finalize_mark"]="'".$result[$i][$r]->finalize_mark."/".$setup->total_mark;
				}
			}
			else{
				$studData[$i]["total_mark"]="'0/".$setup->total_mark;
				$studData[$i]["finalize_mark"]="'0/".$setup->total_mark;
			}
		}
		//end looping data
		for ($i=0; $i < count($studData) ; $i++) { 
			fputcsv($file,array($studData[$i]["no"],$studData[$i]["metric_no"],$studData[$i]["name"],$studData[$i]["total_mark"],$studData[$i]["finalize_mark"]));
		}

		fclose($file); 
		exit; 
	}
}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */