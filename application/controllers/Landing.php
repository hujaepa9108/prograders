<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*-This class is too handle guest users or refer as unauthorize users.

List of functions:
<function_name> : <purpose>

index() : to load landing page view for guest.
-*/
class Landing extends My_Controller {
	public function __construct() {
		parent::__construct();
		if($this->validAuth()){
			redirect("dashboard");
		}
	}

	public function index() {
		echo $this->blade->view()->make("guest.landy");
	}
}