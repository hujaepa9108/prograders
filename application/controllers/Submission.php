<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
NOTES: Cek kat function procFinalize... setelkan form
*/
class Submission extends My_Controller {
    public function __construct() {
      parent::__construct();
      if(!$this->validAuth()){
        redirect("landing");
      }
      $this->load->library('form_validation');
      $this->load->model("Submission_model", "submission_m");
    }

    // COVER CRITERIA 1
    public function c1($statusId,$stdout,$output)
    {
      if($statusId==3) {
        $countStdout=count($stdout);
        $countOutput=count($output);

        //for more than 1 line of output
        if($countStdout>1) {
          $min=min($countStdout,$countOutput);
          $result=0;
          for($i=0; $i<$min; $i++) {
            $noSpaceStdout=str_replace(" ","",$stdout[$i]);
            $noSpaceOutput=str_replace(" ","",$output[$i]);
            if(strcmp($noSpaceStdout,$noSpaceOutput)==0) {
              $result++;
            }
          }
          //if only 1 test case pass
          if($result==1)
            $c1=2;

          //more than 1 test case pass
          else if($result>1) {
            $percent=floor(($result/$countStdout)*100);
            if($percent==50)
              $c1=3;
            else if($percent>50 && $percent<100)
              $c1=4;
            else if($percent==100)
              $c1=5;
            else
              $c1=2;
          }
          else
            $c1=1;
        }

        //for one line output
        else if($countStdout==1) {
          for($i=0; $i<$countStdout; $i++) {
            $noSpaceStdout=str_replace(" ","",$stdout[$i]);
            $noSpaceOutput=str_replace(" ","",$output[$i]);
            if(strcmp($stdout[$i],$output[$i])==0) 
              $c1=2;
            else
              $c1=1;
          }
        }

      }//end of if statusId
      else//else if status!=3
        $c1=1;
      return $c1;
    }

    public function c4($statusId,$stdout,$output)
    {
      if($statusId!=3)
        $c4=1;
      else if($statusId>=7 && $statusId<=12 && $statusId==5)
        $c4=2;
      else if($statusId==3){
        $trimStdout=trim($stdout);
        $trimOutput=trim($output);
        if(strcmp($trimStdout,$trimOutput)!=0)
          $c4=3;
        else if(strcmp($trimStdout,$trimOutput)==0)
          $c4=4;
      }
      return $c4;
    }

    public function assess1($studentID=null)
    {
      extract($_POST);
      $status = $this->initiateStat();
      $breakStdout = explode("\n",trim($stdout));
      $breakOutput = explode("\n",trim($output));
      $c1=$this->c1($statusId,$breakStdout,$breakOutput);
      $c4=$this->c4($statusId,$stdout,$output);
      $total_mark=$c1+$c4;
      $getLineNumber=0;

      if(!empty($compile_message)){
        if(preg_match("/main.cpp:\d:\d/", $compile_message,$matches))
          $getLineNumber=explode(":",$matches[0]);
      }
      
      $data =array(
        "assignment_id"=> base64_decode($assignmentId),
        "c1"=> $c1,
        "c4"=> $c4,
        "submitted_by"=> $this->decrypt($_SESSION["userId"]),
        "status"=> "in-progress",
        "submitted_at"=>date('Y-m-d H:i:s'),
        "output_produce"=>($statusId!=3) ? $statusDesc." : \nCheck at line number ".$getLineNumber[1]."\n==============\nCompiler Status:\n".$compile_message : $output,
        "total_mark"=> $total_mark,
        "source_code"=>$source_code,
        "compile_status"=>$statusDesc,
        "compile_message" =>$compile_message,
        "line_error"=>$getLineNumber[1]
      );

      if(isset($studentId) && !is_null($studentId)){
        $data["submitted_by"]=base64_decode($studentId);
      }

      if($this->submission_m->assess1($data)){
        $status['val']=true;
        $status['msg']="Processing";
      }
      echo json_encode($status);
    }
    
  public function detail($submitId,$assignmentId,$sectionId,$metricNo=NULL)
  {
    if($this->getRole()==3){
      $data["result"]=$this->submission_m->detail(base64_decode($submitId),base64_decode($assignmentId));
      $data["assignmentId"]=$assignmentId;
      $data["sectionId"]=$sectionId;

      //fetch output match stuff
      $data["line"]=array();
      $breakStdout = explode("\n",trim($data["result"]->stdout));
      $breakOutput = explode("\n",trim($data["result"]->output));
      
      $countStdout=count($breakStdout);
      $countOutput=count($breakOutput);
      $match=0;

      //if output only 1 line
      if($countStdout==1) {
        $data["line"][0]=0;
        $stdout=str_replace(" ","",$data["result"]->stdout);
        $output=str_replace(" ","",$data["result"]->output);
        if(strcmp($output,$stdout)==0){
          $data["line"][0]=1;
          $match=1;
        }
      }

      //if output > 1 line
      else if($countStdout>1){
        $min=min($countStdout,$countOutput);
        for($i=0; $i<$min; $i++) {
          $noSpaceStdout=str_replace(" ","",$breakStdout[$i]);
          $noSpaceOutput=str_replace(" ","",$breakOutput[$i]);
          if(strcmp($noSpaceStdout,$noSpaceOutput)==0) {
            $data["line"][$i]=$i;
            $match++;
          }
          else
            $data["line"][$i]=0;
        }
      }

      $data["percent"]=floor(($match/$countStdout)*100);
      $data["match"]=$match;
      $data["countStdout"]=$countStdout;
      echo $this->blade->view()->make("student.submission.detail",$data); 
    }
    
    else if($this->getRole()==2){
      
      $data['metricNo']=base64_decode($metricNo);
      $data["result"]=$this->submission_m->detail(base64_decode($submitId),base64_decode($assignmentId));
      $data["assignmentId"]=$assignmentId;
      $data["sectionId"]=$sectionId;
      $data["assignment"]=$this->submission_m->getDetail(base64_decode($assignmentId));
      //fetch output match stuff
      $data["line"]=array();
      $breakStdout = explode("\n",trim($data["result"]->stdout));
      $breakOutput = explode("\n",trim($data["result"]->output));
      
      $countStdout=count($breakStdout);
      $countOutput=count($breakOutput);
      $match=0;
      //if output only 1 line
      if($countStdout==1) {
        $data["line"][0]=0;
        $stdout=str_replace(" ","",$data["result"]->stdout);
        $output=str_replace(" ","",$data["result"]->output);
        if(strcmp($output,$stdout)==0){
          $data["line"][0]=1;
          $match=1;
        }
      }

      //if output > 1 line
      else if($countStdout>1){
        $min=min($countStdout,$countOutput);
        for($i=0; $i<$min; $i++) {
          $noSpaceStdout=str_replace(" ","",$breakStdout[$i]);
          $noSpaceOutput=str_replace(" ","",$breakOutput[$i]);
          if(strcmp($noSpaceStdout,$noSpaceOutput)==0) {
            $data["line"][$i]=$i;
            $match++;
          }
          else
            $data["line"][$i]=0;
        }
      }

      $data["percent"]=floor(($match/$countStdout)*100);
      $data["match"]=$match;
      $data["countStdout"]=$countStdout;
      echo $this->blade->view()->make("instructor.submission.detail",$data); 
    }
  }
  public function assessment($sectionId,$assignmentId,$submissionId)
  {
    if($this->getRole()==2){
      $data["sectionId"]=$sectionId;
      $data["assignmentId"]=$assignmentId;
      $data["section"]=$this->submission_m->getSectionDetail(base64_decode($sectionId));
      $data["assignment"]=$this->submission_m->getDetail(base64_decode($assignmentId));
      $data["submission"]=$this->submission_m->getSubmission(base64_decode($submissionId));
      $data["grade_setup"]=$this->submission_m->getGradeSetup(base64_decode($assignmentId));


      //fetch output match stuff
      $data["line"]=array();
      $breakStdout = explode("\n",trim($data["assignment"]->output_test_case));
      $breakOutput = explode("\n",trim($data["submission"]->output));
      
      $countStdout=count($breakStdout);
      $countOutput=count($breakOutput);
      $match=0;

      //if output only 1 line
      if($countStdout==1) {
        $data["line"][0]=0;
        $stdout=str_replace(" ","",$data["assignment"]->output_test_case);
        $output=str_replace(" ","",$data["submission"]->output);
        if(strcmp($output,$stdout)==0){
          $data["line"][0]=1;
          $match=1;
        }
      }

      //if output > 1 line
      else if($countStdout>1){
        $min=min($countStdout,$countOutput);
        for($i=0; $i<$min; $i++) {
          $noSpaceStdout=str_replace(" ","",$breakStdout[$i]);
          $noSpaceOutput=str_replace(" ","",$breakOutput[$i]);
          if(strcmp($noSpaceStdout,$noSpaceOutput)==0) {
            $data["line"][$i]=$i;
            $match++;
          }
          else
            $data["line"][$i]=0;
        }
      }

      $data["percent"]=floor(($match/$countStdout)*100);
      $data["match"]=$match;
      $data["countStdout"]=$countStdout;
      //END OF OUTPUT MATCH STUFF

      /*
      1) GET ASSIGNMENT CONTENT
      2) SUBMISSION CONTENT
      3) GRADE SETUP CONTENT
      4) SYNTAX HIGLIGHT STUFF
      */
      echo $this->blade->view()->make("instructor.submission.assessment",$data);
    }
  }

  public function finalize()
  {
    if($this->getRole()==3)
      echo $this->blade->view()->make("student.submission.finalize");
  }

  public function procFinalize()
  {
    if(isset($_POST)) {
      if($this->getRole()==2){
        extract($_POST);
        $status = $this->initiateStat();
        $data = array(
          "c1" => $c1,
          "c2" => (isset($c2) || !empty($c2)) ? $c2: null,
          "c3" => (isset($c3) || !empty($c3)) ? $c3: null,
          "c4" => $c4,
          "c5" => $c5,
          "total_mark" =>$total_mark,
          "finalize_mark" =>$total_mark,
          "submit_id"=> base64_decode($submit_id),
          "status"=>"finalize",
          // "assignment_id", base64_decode($assignment_id),
          "feedback" => (isset($feedback) || !empty($feedback)) ? $feedback : null
        );
        if($this->submission_m->procFinalize($data)){
          $status['val']=true;
          $status['msg']="Assessment has been finalize";
        }
        echo json_encode($status);
      }
    }
  }

  public function procFinalizeList()
  {
    if($this->getRole()==3){
      $userId=$this->decrypt($_SESSION["userId"]);
      $fetch_data=$this->submission_m->finalizeListDB($userId);
      $data = array();
      foreach($fetch_data as $row) { 
        
        $setup=$this->submission_m->getSetup($row->assignment_id);
          $sub_array = array();  
          $sub_array[] = ""; 
          $sub_array[] = $row->title; 
          $sub_array[] =$this->get_time_ago(strtotime($row->submitted_at));
          $sub_array[] = $row->finalize_mark."/".$setup->total_mark;//total submission
          $sub_array[] = "<textarea readonly style='background-color: black; color:#28A745;'>".$row->source_code."</textarea>";
          $sub_array[] = "<textarea readonly class='form-control'  style='background-color: black; color:#fff;'>".$row->output."</textarea>";
          $data[] = $sub_array;  
      }//end of foreach
      $output = array(  
          "draw" => intval($_POST["draw"]),  
          "recordsTotal" => $this->submission_m->finalizeListCount($userId),  
          "recordsFiltered" => $this->submission_m->finalizeListFiltered($userId),  
          "data" => $data  
      );  
      echo json_encode($output);
    }//end of get role
  }

  public function inprogress()
  {
    if($this->getRole()==3)
      echo $this->blade->view()->make("student.submission.inprogress");
  }

  public function procInProgressList()
  {
    if($this->getRole()==3){
      $userId=$this->decrypt($_SESSION["userId"]);
      $fetch_data=$this->submission_m->inProgressListDB($userId);
      $data = array();
      foreach($fetch_data as $row) { 
          $setup=$this->submission_m->getSetup($row->assignment_id);
          $sub_array = array();  
          $sub_array[] = "";
          $sub_array[] = $row->title;
          $sub_array[] =$this->get_time_ago(strtotime($row->submitted_at));
          // $sub_array[] = $row->total_mark."/".$setup->total_mark;//total submission
          $sub_array[] = "<textarea readonly class='form-control'  style='background-color: black; color:#28A745;'>".$row->source_code."</textarea>";
          $sub_array[] = "<textarea readonly class='form-control'  style='background-color: black; color:#fff;'>".$row->output."</textarea>";
          $data[] = $sub_array;  
      }//end of foreach
      $output = array(  
          "draw" => intval($_POST["draw"]),  
          "recordsTotal" => $this->submission_m->inProgressListCount($userId),  
          "recordsFiltered" => $this->submission_m->inProgressListFiltered($userId),  
          "data" => $data  
      );  
      echo json_encode($output);
    }//end of get role
  }

  public function all()
  {
    if($this->getRole()==3)
      echo $this->blade->view()->make("student.submission.all");
  }

  public function procAllList()
  {
    if($this->getRole()==3){
      $userId=$this->decrypt($_SESSION["userId"]);
      $fetch_data=$this->submission_m->allListDB($userId);
      $data = array();
      foreach($fetch_data as $row) { 
        $setup=$this->submission_m->getSetup($row->assignment_id);
          $sub_array = array();  
          $sub_array[] = "";
          $sub_array[] = $row->title;
          $sub_array[] =$this->get_time_ago(strtotime($row->submitted_at));
          $sub_array[] = "<textarea readonly class='form-control' style='background-color: black; color:#28A745'>".$row->source_code."</textarea>";
          $sub_array[] = "<textarea readonly class='form-control' style='background-color: black; color:#fff;'>".$row->output."</textarea>";
          $sub_array[] = $row->status; 
          $data[] = $sub_array;  
      }//end of foreach
      $output = array(  
          "draw" => intval($_POST["draw"]),  
          "recordsTotal" => $this->submission_m->allListCount($userId),  
          "recordsFiltered" => $this->submission_m->allListFiltered($userId),  
          "data" => $data  
      );  
      echo json_encode($output);
    }//end of get role
  }

  public function viewCode($submitId)
  {
    $data["submit"]=$this->submission_m->viewCode(base64_decode($submitId));
    echo json_encode($output);
  }
}