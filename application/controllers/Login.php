<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends My_Controller {

    /*The constructor of the class. All loading (libaries etc) performs here. 
    */
	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Login_model','login_m');
		if($this->validAuth()){
			redirect("dashboard");
		}
	}

	/*-This method is to load login view.-*/
	public function index()
	{
		echo $this->blade->view()->make("guest.login");
	}

	/*-This method is to processed login.-*/
	public function process() {
		if(empty($this->input->post()))
			redirect("login");

		//settings form validation stuff
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		//if form validation failed!
		if($this->form_validation->run() == FALSE){
			echo $this->blade->view()->make("login");
		}
		
		else {
			$email=$this->input->post('email');
			$password=$this->input->post('password');

			$rec = $this->login_m->getLogin($email);//fetch login credentials
			if(isset($rec->email) && password_verify($password,$rec->password)) { 
				//create token
				$token = $this->generateToken($rec->email);

				//encrypt the session data
				$cryptedEmail=$this->encrypt($rec->email);
				$cryptedRole=$this->encrypt($rec->role_id);
				$cryptedUserId=$this->encrypt($rec->id);
				$cryptedToken= $this->encrypt($token);
				
				$sessdata = array(
					'email'=>$cryptedEmail,
					'display' =>$rec->name,
					'role' => $cryptedRole,
					'userId' => $cryptedUserId,
					'token'=>$cryptedToken
				);

				$userLogData = array(
					"user_id" => $rec->id,
					"start_login" => date('Y-m-d H:i:s')
				);

				//set session
				$this->session->set_userdata($sessdata);
				$this->login_m->insertLogged($userLogData);
				redirect('dashboard');
			}

			else {//invalid email or password
				$data['status']="invalid email";
				$data['msg']="Login Failed! Invalid email or password";
				echo $this->blade->view()->make("guest.login",$data);
			}
		}
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */