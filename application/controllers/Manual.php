<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manual extends My_Controller {
    public function manual1()
    {
        $pdffilename = base_url().'assets/manuals/instructor/manual1.pdf';
	    header('Content-type: application/pdf');
	    header('Content-Description: inline; filename="'.$pdffilename.'"');
	    header('Content-Transfer-Encoding: binary');
	    header('Accept-Ranges: bytes');
	    @readfile($pdffilename);
    }
	public function manual2()
    {
        $pdffilename = base_url().'assets/manuals/instructor/manual2.pdf';
	    header('Content-type: application/pdf');
	    header('Content-Description: inline; filename="'.$pdffilename.'"');
	    header('Content-Transfer-Encoding: binary');
	    header('Accept-Ranges: bytes');
	    @readfile($pdffilename);
    }
}