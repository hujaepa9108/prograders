<?php
class Error404 extends My_Controller {

	public function __construct() {
	    parent::__construct();
	}

	public function index() {
	    $this->output->set_status_header('404');
	    $data['link'] = $this->validAuth()==true ?  base_url()."dashboard" : NULL; 	    	
	    $this->load->view('err404',$data);    
	}

}