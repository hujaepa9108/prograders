<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->validAuth()) {
			redirect("landing");
		}
		$this->load->model('Dashboard_model','dashboard_m');
	}

	/*
		Description: to load dashboard info stuff.
		Roles: stud, inst
	*/
	public function index() {

		if($this->getRole()==2) { //get instructor dashboard
			$data["course"]=$this->dashboard_m->getCourseInfo($this->decrypt($_SESSION["userId"]));
			$data["assignment"]=$this->dashboard_m->getAssgmt($this->decrypt($_SESSION["userId"]));
			$fetch_sections=$this->dashboard_m->getSectionList($this->decrypt($_SESSION["userId"]));
			$data["fetch_sections"]=$fetch_sections;
			for ($i=0; $i < count($fetch_sections); $i++) {
				$data["studentList"][$i]=$this->dashboard_m->getStudents($fetch_sections[$i]->section_id);
				if(count($data["studentList"][$i])>0) {
					for($j=0; $j < count($data["studentList"][$i]); $j++){
						$temp=$this->dashboard_m->getHighestMark($data["studentList"][$i][$j]->stud_id);
						// print_r($temp->highest_mark);
						if(isset($temp->highest_mark) && $temp->highest_mark > 0) {
							$data["studentList"][$i][$j]->finalize_mark=$temp->highest_mark."/".$temp->total_mark;
							$data["studentList"][$i][$j]->percentage=round(($temp->highest_mark/$temp->total_mark)*100,2);
							$data["studentList"][$i][$j]->title=$temp->title;
						}

						else{
							$data["studentList"][$i][$j]->finalize_mark="-";
							$data["studentList"][$i][$j]->title="-";
							$data["studentList"][$i][$j]->percentage="-";
						}
					}
				}
			}
			echo $this->blade->view()->make("instructor.dashboard",$data);
		}

		else if($this->getRole()==3) { //get student dashboard
			$data["course"]=$this->dashboard_m->getCourseInfo($this->decrypt($_SESSION["userId"]));
			$data["section"]=$this->dashboard_m->getSectionInfo($this->decrypt($_SESSION["userId"]));
			$data["assignment"]=$this->dashboard_m->getAssignment($this->decrypt($_SESSION["userId"]),$data["section"]->id);//get list of assignment
			$data["total_assgmt"]=$this->dashboard_m->getTotalAssgmt($this->decrypt($_SESSION["userId"]),$data["section"]->id);
			for($i=0; $i<count($data["assignment"]); $i++){
				$temp=$this->dashboard_m->getGradeSetup($data["assignment"][$i]->id);
				$temp2=$this->dashboard_m->getAssessment($data["assignment"][$i]->id,$this->decrypt($_SESSION["userId"]));
				if(!empty($temp2->finalize_mark)){
					$data['assignment'][$i]->grade_mark=$temp->grade_mark;
					$data['assignment'][$i]->finalize_mark=$temp2->finalize_mark;
					$data['assignment'][$i]->percentage=round(($temp2->finalize_mark/$temp->grade_mark)*100,2);
				}
				else{
					$data['assignment'][$i]->grade_mark=$temp->grade_mark;
					$data['assignment'][$i]->finalize_mark=0;
					$data['assignment'][$i]->percentage=round((0/$temp->grade_mark)*100,2);
				}
			}
			// $data["assessment"]=$this->dashboard_m->studAssignment();//get list of sassignment with marking sort by highest to lowest
			echo $this->blade->view()->make("student.dashboard",$data);
		}
	}
	public function specific($studId)
	{
		if($this->getRole()==2) { 
			echo "test";
		}
	}
}