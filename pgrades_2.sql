-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 27, 2022 at 12:59 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pgrades_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_tab`
--

DROP TABLE IF EXISTS `admin_tab`;
CREATE TABLE IF NOT EXISTS `admin_tab` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tab_username_idx` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_tab`
--

INSERT INTO `admin_tab` (`id`, `username`, `password`, `created_at`, `created_by`, `role_id`) VALUES
(1, '$uPER@DMIn9108', '$2y$12$Vt4VU/FAsrnCUDXEkaKMrel66opMMWtWHbDLT6XO6XzLFPLlo6pIW', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `assignment_grade_setup_tab`
--

DROP TABLE IF EXISTS `assignment_grade_setup_tab`;
CREATE TABLE IF NOT EXISTS `assignment_grade_setup_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `item_1` int(11) DEFAULT NULL,
  `item_2` int(11) DEFAULT NULL,
  `item_3` int(11) DEFAULT NULL,
  `total_mark` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignment_id` (`assignment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assignment_grade_setup_tab`
--

INSERT INTO `assignment_grade_setup_tab` (`id`, `assignment_id`, `item_1`, `item_2`, `item_3`, `total_mark`) VALUES
(1, 1, 2, 0, 0, 11),
(2, 2, 2, 0, 0, 11);

-- --------------------------------------------------------

--
-- Table structure for table `assignment_tab`
--

DROP TABLE IF EXISTS `assignment_tab`;
CREATE TABLE IF NOT EXISTS `assignment_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `source_code` text,
  `input_test_case` text,
  `output_test_case` text,
  `status` varchar(50) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assignment_tab`
--

INSERT INTO `assignment_tab` (`id`, `title`, `description`, `source_code`, `input_test_case`, `output_test_case`, `status`, `start_date`, `end_date`, `created_at`, `created_by`, `modified_at`) VALUES
(1, 'HELLO WORLD', 'write a c++ program that display output hello world', '#include <iostream>\nusing namespace std;\n\nint main(){\n    cout<<\"hello world\";\n    return 0;\n}', '', 'hello world', 'ACTIVE', '2021-02-01 14:00:00', '2021-04-27 23:59:00', '2021-02-01 14:53:34', NULL, NULL),
(2, 'HELLO WORLD', 'Write a C++ program that display hello world', '#include <iostream>\r\nusing namespace std;\r\n\r\nint main(){\r\n    cout<<\"Hello World\";\r\n    return 0;\r\n}\r\n', '', 'Hello World', 'ACTIVE', '2021-02-06 07:00:00', '2021-03-31 23:59:00', '2021-02-06 07:43:22', 79, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course_tab`
--

DROP TABLE IF EXISTS `course_tab`;
CREATE TABLE IF NOT EXISTS `course_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `university_id` int(11) DEFAULT NULL,
  `uni_level` varchar(100) DEFAULT NULL,
  `pl_id` int(11) DEFAULT NULL,
  `pl_name` varchar(200) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `university_id` (`university_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_tab`
--

INSERT INTO `course_tab` (`id`, `name`, `code`, `university_id`, `uni_level`, `pl_id`, `pl_name`, `description`, `created_at`, `created_by`) VALUES
(4, 'INTRODUCTORY OF PROGRAMMING', 'MTS0001', 12, 'POSTGRADUATE', 0, 'Java (OpenJDK 13.0.1)', 'fdfdfd', '2020-05-28 21:15:00', 1),
(5, 'PRINCIPLES OF PROGRAMMING', 'MTS0003', 13, 'POSTGRADUATE', 0, 'C# (Mono 6.6.0.161)', 'for testing purposes', '2020-05-29 10:59:00', 1),
(9, 'TEST NAME', 'MTS0002', 9, NULL, 54, 'C++ (GCC 9.2.0)', 'sdsdsdsd', '2020-05-29 12:00:00', 1),
(10, 'TEST', 'MTS0010', 8, 'foundation', 54, 'C++ (GCC 9.2.0)', 'dsdsdsds', '2020-05-29 14:55:00', 1),
(11, 'PRINCIPLES OF PROGRAMMING II', 'MTS0101', 13, 'DIPLOMA', 54, 'C++ (GCC 9.2.0)', 'Using C++', '2020-05-30 19:07:00', 1),
(12, 'FUNDAMENTAL PROGRAMMING', 'MKS1103', 6, 'POSTGRADUATE', 54, 'C++ (GCC 9.2.0)', 'This course introduces students to the basic concept and characteristics of structure programming. This course\r\nconsists of problem solving techniques, algorithm design and coding in programming language. To code a program,\r\nstudent will learned the syntax and semantics of programming language. Other than that, the course emphasize on\r\nhow to do an efficient and correct way of programming with error free. Besides the main concepts of game\r\ndevelopment to apply the concept of structured programming is also emphasized.', '2020-06-30 08:53:09', 1),
(13, 'OBJECT-ORIENTED PROGRAMMING', 'MKS1023', 6, 'POSTGRADUATE', 62, 'Java (OpenJDK 13.0.1)', 'The goal of this course is to introduce students to the basics, the features and the concepts of object-based\r\nprogramming. This course stresses upon the basic programming applications such as structure sequential control,\r\nselection, loop, method, array and file. Emphasis is also given in generating error-free and efficient program.', '2020-06-30 08:57:26', 1),
(15, 'TESTING C++', 'TESTINGC++01', 14, 'diploma', 54, 'C++ (GCC 9.2.0)', 'TESTING C++ COURSE', '2021-02-04 11:10:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `course_user_tab`
--

DROP TABLE IF EXISTS `course_user_tab`;
CREATE TABLE IF NOT EXISTS `course_user_tab` (
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  KEY `course_id` (`course_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_user_tab`
--

INSERT INTO `course_user_tab` (`course_id`, `user_id`) VALUES
(12, NULL),
(12, NULL),
(12, NULL),
(12, NULL),
(12, NULL),
(12, NULL),
(13, NULL),
(13, NULL),
(4, NULL),
(12, NULL),
(12, 66),
(12, 67),
(12, 68),
(12, 69),
(12, NULL),
(12, NULL),
(4, NULL),
(4, NULL),
(NULL, NULL),
(5, NULL),
(NULL, 76),
(12, NULL),
(12, NULL),
(15, 79),
(15, 80),
(12, 81),
(15, 82),
(15, 83);

-- --------------------------------------------------------

--
-- Table structure for table `first_time_login_tab`
--

DROP TABLE IF EXISTS `first_time_login_tab`;
CREATE TABLE IF NOT EXISTS `first_time_login_tab` (
  `user_id` int(11) DEFAULT NULL,
  `password_change_status` varchar(100) DEFAULT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `first_time_login_tab`
--

INSERT INTO `first_time_login_tab` (`user_id`, `password_change_status`) VALUES
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(66, NULL),
(67, NULL),
(68, NULL),
(69, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(76, NULL),
(NULL, NULL),
(NULL, NULL),
(79, NULL),
(80, NULL),
(81, NULL),
(82, NULL),
(83, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logged_user_tab`
--

DROP TABLE IF EXISTS `logged_user_tab`;
CREATE TABLE IF NOT EXISTS `logged_user_tab` (
  `user_id` int(11) DEFAULT NULL,
  `start_login` datetime DEFAULT NULL,
  `end_login` datetime DEFAULT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logged_user_tab`
--

INSERT INTO `logged_user_tab` (`user_id`, `start_login`, `end_login`) VALUES
(NULL, '2020-07-07 18:54:33', NULL),
(NULL, '2020-07-07 18:55:48', NULL),
(NULL, '2020-07-07 19:03:11', NULL),
(NULL, '2020-07-08 14:42:42', NULL),
(NULL, '2020-07-08 15:09:12', NULL),
(NULL, '2020-07-08 15:11:44', NULL),
(NULL, '2020-07-08 15:12:43', NULL),
(NULL, '2020-07-08 15:21:48', NULL),
(NULL, '2020-07-08 15:38:34', NULL),
(NULL, '2020-07-08 16:57:08', NULL),
(NULL, '2020-07-08 17:03:31', NULL),
(NULL, '2020-07-09 07:25:06', NULL),
(NULL, '2020-07-09 13:20:57', NULL),
(NULL, '2020-07-09 13:40:33', NULL),
(NULL, '2020-07-09 13:45:45', NULL),
(NULL, '2020-07-09 17:00:11', NULL),
(NULL, '2020-07-10 09:00:25', NULL),
(NULL, '2020-07-11 10:20:24', NULL),
(NULL, '2020-07-15 12:20:10', NULL),
(NULL, '2020-07-15 12:40:44', NULL),
(NULL, '2020-07-15 17:33:02', NULL),
(NULL, '2020-07-15 20:14:00', NULL),
(NULL, '2020-07-16 07:24:33', NULL),
(NULL, '2020-07-16 09:09:46', NULL),
(NULL, '2020-07-16 11:20:58', NULL),
(NULL, '2020-07-19 09:16:30', NULL),
(NULL, '2020-07-19 15:33:50', NULL),
(NULL, '2020-07-19 20:31:40', NULL),
(NULL, '2020-07-21 07:50:12', NULL),
(NULL, '2020-07-21 19:48:48', NULL),
(NULL, '2020-07-22 07:28:21', NULL),
(NULL, '2020-07-22 08:03:18', NULL),
(NULL, '2020-07-23 15:22:49', NULL),
(NULL, '2020-07-25 16:21:28', NULL),
(NULL, '2020-07-25 21:04:47', NULL),
(NULL, '2020-07-25 21:22:11', NULL),
(NULL, '2020-07-25 21:36:17', NULL),
(NULL, '2020-07-25 21:43:46', NULL),
(NULL, '2020-07-29 22:03:57', NULL),
(NULL, '2020-07-29 22:08:29', NULL),
(NULL, '2020-07-30 07:21:22', NULL),
(NULL, '2020-07-30 07:24:41', NULL),
(NULL, '2020-07-30 07:28:23', NULL),
(NULL, '2020-07-30 07:56:58', NULL),
(NULL, '2020-07-30 20:38:32', NULL),
(NULL, '2020-08-01 08:47:31', NULL),
(NULL, '2020-08-02 07:51:48', NULL),
(NULL, '2020-08-02 08:04:49', NULL),
(NULL, '2020-08-03 10:52:58', NULL),
(NULL, '2020-08-04 08:50:13', NULL),
(NULL, '2020-08-05 12:15:22', NULL),
(NULL, '2020-08-07 07:40:33', NULL),
(NULL, '2020-08-07 10:23:26', NULL),
(NULL, '2020-08-07 20:01:02', NULL),
(NULL, '2020-08-08 09:12:37', NULL),
(NULL, '2020-08-08 20:59:20', NULL),
(NULL, '2020-08-08 21:04:26', NULL),
(NULL, '2020-08-08 21:25:31', NULL),
(NULL, '2020-08-08 21:26:20', NULL),
(NULL, '2020-08-08 21:28:38', NULL),
(NULL, '2020-08-10 07:44:37', NULL),
(NULL, '2020-08-10 08:01:41', NULL),
(NULL, '2020-08-10 08:07:49', NULL),
(NULL, '2020-08-10 08:15:33', NULL),
(NULL, '2020-08-10 08:29:33', NULL),
(NULL, '2020-08-10 08:30:26', NULL),
(NULL, '2020-08-10 15:47:22', NULL),
(NULL, '2020-08-10 16:13:26', NULL),
(NULL, '2020-08-10 18:24:32', NULL),
(NULL, '2020-08-10 20:44:35', NULL),
(NULL, '2020-08-11 20:26:14', NULL),
(NULL, '2020-08-14 09:43:22', NULL),
(NULL, '2020-08-14 10:57:59', NULL),
(NULL, '2020-08-15 10:04:47', NULL),
(NULL, '2020-08-15 20:28:57', NULL),
(NULL, '2020-08-15 20:49:15', NULL),
(NULL, '2020-08-15 20:58:01', NULL),
(NULL, '2020-08-16 07:44:17', NULL),
(NULL, '2020-08-16 15:08:59', NULL),
(NULL, '2020-08-16 19:31:34', NULL),
(NULL, '2020-08-17 08:23:11', NULL),
(NULL, '2020-08-17 20:59:26', NULL),
(NULL, '2020-08-18 08:47:02', NULL),
(NULL, '2020-08-20 08:02:20', NULL),
(NULL, '2020-08-20 20:13:35', NULL),
(NULL, '2020-08-21 09:49:04', NULL),
(NULL, '2020-08-21 14:48:10', NULL),
(NULL, '2020-08-23 09:54:01', NULL),
(NULL, '2020-08-23 14:59:06', NULL),
(NULL, '2020-08-25 08:29:23', NULL),
(NULL, '2020-08-26 14:53:19', NULL),
(NULL, '2020-08-26 14:57:47', NULL),
(NULL, '2020-08-26 15:04:08', NULL),
(NULL, '2020-08-26 15:15:56', NULL),
(NULL, '2020-08-26 15:16:26', NULL),
(NULL, '2020-08-27 07:48:27', NULL),
(NULL, '2020-08-27 07:53:02', NULL),
(NULL, '2020-08-27 09:45:04', NULL),
(NULL, '2020-08-27 09:50:44', NULL),
(NULL, '2020-08-27 12:42:24', NULL),
(NULL, '2020-08-28 09:28:12', NULL),
(NULL, '2020-08-28 09:55:58', NULL),
(NULL, '2020-08-28 10:12:45', NULL),
(NULL, '2020-08-28 10:15:08', NULL),
(NULL, '2020-08-29 07:18:01', NULL),
(NULL, '2020-08-29 08:07:25', NULL),
(NULL, '2020-08-29 08:11:37', NULL),
(NULL, '2020-08-30 20:47:07', NULL),
(NULL, '2020-08-30 21:19:31', NULL),
(NULL, '2020-08-30 21:20:10', NULL),
(NULL, '2020-08-31 10:35:46', NULL),
(NULL, '2020-08-31 10:38:12', NULL),
(NULL, '2020-09-01 10:39:31', NULL),
(NULL, '2020-09-01 10:40:25', NULL),
(NULL, '2020-09-01 11:54:36', NULL),
(NULL, '2020-09-03 10:57:42', NULL),
(NULL, '2020-09-03 11:25:56', NULL),
(NULL, '2020-09-03 19:52:37', NULL),
(NULL, '2020-09-03 19:54:55', NULL),
(NULL, '2020-09-03 20:14:28', NULL),
(NULL, '2020-09-03 20:37:22', NULL),
(NULL, '2020-09-03 20:37:43', NULL),
(NULL, '2020-09-04 09:50:15', NULL),
(NULL, '2020-09-04 10:28:06', NULL),
(NULL, '2020-09-05 06:53:34', NULL),
(NULL, '2020-09-06 14:44:54', NULL),
(NULL, '2020-09-06 14:46:19', NULL),
(NULL, '2020-09-06 15:38:32', NULL),
(NULL, '2020-09-07 09:00:13', NULL),
(NULL, '2020-09-07 09:20:32', NULL),
(NULL, '2020-09-07 10:56:48', NULL),
(NULL, '2020-09-07 10:57:08', NULL),
(NULL, '2020-09-07 14:39:15', NULL),
(NULL, '2020-09-07 19:58:39', NULL),
(NULL, '2020-09-07 20:11:59', NULL),
(NULL, '2020-09-08 06:59:15', NULL),
(NULL, '2020-09-08 07:14:21', NULL),
(NULL, '2020-09-08 10:56:30', NULL),
(NULL, '2020-09-08 11:00:14', NULL),
(NULL, '2020-09-08 12:04:29', NULL),
(NULL, '2020-09-10 08:07:10', NULL),
(NULL, '2020-09-10 09:03:21', NULL),
(NULL, '2020-09-10 14:44:10', NULL),
(NULL, '2020-09-10 16:26:08', NULL),
(NULL, '2020-09-14 09:58:10', NULL),
(NULL, '2020-09-14 11:02:32', NULL),
(NULL, '2020-09-14 19:48:05', NULL),
(NULL, '2020-09-16 09:57:38', NULL),
(NULL, '2020-09-16 09:58:41', NULL),
(NULL, '2020-09-16 15:31:59', NULL),
(NULL, '2020-09-16 15:40:03', NULL),
(NULL, '2020-09-16 15:45:06', NULL),
(NULL, '2020-09-17 07:44:19', NULL),
(NULL, '2020-09-17 07:45:41', NULL),
(NULL, '2020-09-17 20:10:41', NULL),
(NULL, '2020-09-17 20:14:53', NULL),
(NULL, '2020-09-18 08:08:05', NULL),
(NULL, '2020-09-18 09:57:44', NULL),
(NULL, '2020-09-18 12:05:16', NULL),
(NULL, '2020-09-18 12:06:58', NULL),
(NULL, '2020-09-18 20:53:27', NULL),
(NULL, '2020-09-18 22:22:10', NULL),
(NULL, '2020-09-18 22:26:58', NULL),
(NULL, '2020-09-19 22:23:16', NULL),
(NULL, '2020-09-19 22:28:39', NULL),
(NULL, '2020-09-20 07:15:06', NULL),
(NULL, '2020-09-20 07:17:55', NULL),
(NULL, '2020-09-20 08:45:55', NULL),
(NULL, '2020-09-20 08:57:56', NULL),
(NULL, '2020-09-20 15:35:15', NULL),
(NULL, '2020-09-21 10:16:41', NULL),
(NULL, '2020-09-21 10:39:11', NULL),
(NULL, '2020-09-23 14:35:50', NULL),
(NULL, '2020-09-23 15:23:56', NULL),
(NULL, '2020-09-25 10:30:43', NULL),
(NULL, '2020-09-25 16:23:07', NULL),
(NULL, '2020-09-25 16:25:34', NULL),
(NULL, '2020-09-25 16:31:30', NULL),
(NULL, '2020-09-25 16:35:10', NULL),
(NULL, '2020-09-25 16:53:10', NULL),
(NULL, '2020-09-25 16:58:22', NULL),
(NULL, '2020-09-25 17:00:05', NULL),
(NULL, '2020-09-25 17:01:51', NULL),
(NULL, '2020-09-25 20:28:14', NULL),
(NULL, '2020-09-26 10:34:07', NULL),
(NULL, '2020-09-27 10:05:06', NULL),
(NULL, '2020-09-27 17:09:14', NULL),
(NULL, '2020-09-27 17:09:41', NULL),
(NULL, '2020-09-27 18:47:07', NULL),
(NULL, '2020-09-30 07:30:18', NULL),
(NULL, '2020-09-30 07:35:39', NULL),
(NULL, '2020-09-30 07:46:53', NULL),
(NULL, '2020-10-01 10:11:43', NULL),
(NULL, '2020-10-01 10:23:00', NULL),
(NULL, '2020-10-01 10:43:45', NULL),
(NULL, '2020-10-01 13:23:25', NULL),
(NULL, '2020-10-01 13:24:01', NULL),
(NULL, '2020-10-01 13:24:57', NULL),
(NULL, '2020-10-01 19:56:13', NULL),
(NULL, '2020-10-01 20:56:32', NULL),
(NULL, '2020-10-01 21:31:41', NULL),
(NULL, '2020-10-01 22:01:51', NULL),
(NULL, '2020-10-02 00:03:17', NULL),
(NULL, '2020-10-02 00:10:10', NULL),
(NULL, '2020-10-02 10:44:24', NULL),
(NULL, '2020-10-02 10:47:01', NULL),
(NULL, '2020-10-02 10:51:51', NULL),
(NULL, '2020-10-04 19:29:36', NULL),
(NULL, '2020-10-05 07:39:18', NULL),
(NULL, '2020-10-05 07:41:36', NULL),
(NULL, '2020-10-06 06:29:48', NULL),
(NULL, '2020-10-06 07:00:31', NULL),
(NULL, '2020-10-06 07:02:08', NULL),
(NULL, '2020-10-06 08:47:42', NULL),
(NULL, '2020-10-06 12:24:09', NULL),
(NULL, '2020-10-06 12:33:21', NULL),
(NULL, '2020-10-06 21:38:19', NULL),
(NULL, '2020-10-06 21:38:31', NULL),
(NULL, '2020-10-07 10:16:19', NULL),
(NULL, '2020-10-08 09:11:47', NULL),
(NULL, '2020-10-08 22:42:25', NULL),
(NULL, '2020-10-08 22:45:09', NULL),
(NULL, '2020-10-09 21:13:23', NULL),
(NULL, '2020-10-09 21:16:41', NULL),
(69, '2020-10-09 22:19:55', NULL),
(69, '2020-10-09 22:21:30', NULL),
(NULL, '2020-10-09 22:22:05', NULL),
(69, '2020-10-09 22:23:21', NULL),
(NULL, '2020-10-09 22:26:02', NULL),
(NULL, '2020-10-09 23:04:54', NULL),
(NULL, '2020-10-10 06:53:48', NULL),
(NULL, '2020-10-10 19:18:29', NULL),
(NULL, '2020-10-11 08:26:06', NULL),
(NULL, '2020-10-11 20:57:09', NULL),
(NULL, '2020-10-12 09:26:54', NULL),
(NULL, '2020-10-12 16:38:23', NULL),
(NULL, '2020-10-12 16:43:00', NULL),
(NULL, '2020-10-12 22:27:53', NULL),
(NULL, '2020-10-13 07:17:41', NULL),
(NULL, '2020-10-14 10:21:25', NULL),
(NULL, '2020-10-14 10:32:45', NULL),
(NULL, '2020-10-14 14:48:36', NULL),
(NULL, '2020-10-14 15:50:23', NULL),
(NULL, '2020-10-14 20:18:04', NULL),
(NULL, '2020-10-14 20:18:19', NULL),
(66, '2020-10-14 20:29:11', NULL),
(NULL, '2020-10-14 20:31:53', NULL),
(NULL, '2020-10-14 20:33:50', NULL),
(NULL, '2020-10-14 20:36:15', NULL),
(NULL, '2020-10-14 20:36:40', NULL),
(NULL, '2020-10-14 20:40:54', NULL),
(NULL, '2020-10-14 20:41:57', NULL),
(NULL, '2020-10-15 15:23:06', NULL),
(NULL, '2020-10-16 16:05:13', NULL),
(NULL, '2020-10-16 20:05:29', NULL),
(NULL, '2020-10-16 20:41:29', NULL),
(NULL, '2020-10-16 21:12:15', NULL),
(NULL, '2020-10-16 21:17:16', NULL),
(NULL, '2020-10-19 08:49:16', NULL),
(NULL, '2020-10-19 08:53:47', NULL),
(NULL, '2020-10-19 15:04:43', NULL),
(NULL, '2020-10-20 06:55:01', NULL),
(66, '2020-10-20 07:33:59', NULL),
(66, '2020-10-20 07:46:54', NULL),
(66, '2020-10-20 07:56:18', NULL),
(NULL, '2020-10-20 10:04:31', NULL),
(66, '2020-10-20 15:27:28', NULL),
(NULL, '2020-10-20 15:30:15', NULL),
(NULL, '2020-10-20 19:12:22', NULL),
(NULL, '2020-10-21 08:43:52', NULL),
(NULL, '2020-10-21 10:02:28', NULL),
(NULL, '2020-10-21 12:41:11', NULL),
(66, '2020-10-21 13:47:10', NULL),
(NULL, '2020-10-22 07:20:19', NULL),
(NULL, '2020-10-22 10:44:32', NULL),
(NULL, '2020-10-24 14:58:15', NULL),
(NULL, '2020-10-25 09:49:14', NULL),
(NULL, '2020-10-27 07:50:03', NULL),
(NULL, '2020-10-27 07:55:32', NULL),
(NULL, '2020-10-27 18:12:30', NULL),
(NULL, '2020-10-27 19:17:07', NULL),
(NULL, '2020-10-27 22:29:10', NULL),
(NULL, '2020-10-28 09:23:44', NULL),
(66, '2020-10-28 09:56:03', NULL),
(66, '2020-10-28 10:04:05', NULL),
(NULL, '2020-10-28 10:04:22', NULL),
(NULL, '2020-10-28 10:16:16', NULL),
(NULL, '2020-10-28 10:35:17', NULL),
(NULL, '2020-11-01 07:17:56', NULL),
(NULL, '2020-11-01 07:20:00', NULL),
(NULL, '2020-11-01 10:04:59', NULL),
(NULL, '2020-11-01 10:05:20', NULL),
(NULL, '2020-11-01 11:38:08', NULL),
(NULL, '2020-11-01 11:54:15', NULL),
(NULL, '2020-11-01 11:54:49', NULL),
(66, '2020-11-01 22:39:46', NULL),
(NULL, '2020-11-01 22:41:53', NULL),
(NULL, '2020-11-02 07:49:13', NULL),
(NULL, '2020-11-02 08:07:34', NULL),
(NULL, '2020-11-02 09:46:33', NULL),
(NULL, '2020-11-02 10:14:28', NULL),
(68, '2020-11-02 10:15:55', NULL),
(NULL, '2020-11-02 19:10:16', NULL),
(NULL, '2020-11-02 19:37:14', NULL),
(NULL, '2020-11-03 09:28:16', NULL),
(NULL, '2020-11-10 08:07:53', NULL),
(NULL, '2020-11-10 19:45:44', NULL),
(NULL, '2020-11-11 08:47:33', NULL),
(NULL, '2020-11-11 12:52:41', NULL),
(NULL, '2020-11-11 12:53:45', NULL),
(NULL, '2020-11-13 09:54:40', NULL),
(NULL, '2020-11-13 09:55:08', NULL),
(66, '2020-11-13 09:57:25', NULL),
(NULL, '2020-11-13 10:09:34', NULL),
(NULL, '2020-11-13 10:10:37', NULL),
(NULL, '2020-11-13 10:22:07', NULL),
(NULL, '2020-11-16 15:01:37', NULL),
(NULL, '2020-11-19 08:31:32', NULL),
(NULL, '2020-12-04 11:08:36', NULL),
(NULL, '2020-12-09 11:34:43', NULL),
(NULL, '2020-12-15 08:54:20', NULL),
(NULL, '2020-12-15 08:57:50', NULL),
(NULL, '2020-12-15 09:02:16', NULL),
(NULL, '2020-12-15 09:05:40', NULL),
(NULL, '2020-12-15 09:10:48', NULL),
(NULL, '2021-01-05 11:41:29', NULL),
(NULL, '2021-01-05 14:31:10', NULL),
(NULL, '2021-01-16 19:42:27', NULL),
(NULL, '2021-01-17 10:40:07', NULL),
(NULL, '2021-01-18 06:57:16', NULL),
(NULL, '2021-01-18 19:28:25', NULL),
(NULL, '2021-01-19 07:09:26', NULL),
(NULL, '2021-01-19 12:22:48', NULL),
(NULL, '2021-01-23 11:53:20', NULL),
(NULL, '2021-01-26 20:34:19', NULL),
(NULL, '2021-01-27 20:26:00', NULL),
(NULL, '2021-01-28 09:22:20', NULL),
(NULL, '2021-01-28 10:16:26', NULL),
(NULL, '2021-01-28 10:16:59', NULL),
(NULL, '2021-01-28 10:17:27', NULL),
(NULL, '2021-01-28 21:58:34', NULL),
(NULL, '2021-01-28 21:59:13', NULL),
(NULL, '2021-01-28 22:01:39', NULL),
(NULL, '2021-01-28 22:23:05', NULL),
(NULL, '2021-01-29 11:14:18', NULL),
(NULL, '2021-01-30 10:22:32', NULL),
(NULL, '2021-01-30 10:26:36', NULL),
(NULL, '2021-01-30 10:28:01', NULL),
(NULL, '2021-01-30 10:31:49', NULL),
(NULL, '2021-01-30 10:32:28', NULL),
(NULL, '2021-01-30 10:33:13', NULL),
(NULL, '2021-01-30 11:26:05', NULL),
(NULL, '2021-01-30 11:31:48', NULL),
(NULL, '2021-01-30 11:35:41', NULL),
(NULL, '2021-01-30 13:47:09', NULL),
(NULL, '2021-01-30 14:35:25', NULL),
(NULL, '2021-01-30 22:16:08', NULL),
(NULL, '2021-02-01 14:36:07', NULL),
(NULL, '2021-02-01 14:51:02', NULL),
(79, '2021-02-05 07:29:04', NULL),
(82, '2021-02-06 07:36:20', NULL),
(82, '2021-02-06 07:39:54', NULL),
(80, '2021-02-06 07:41:17', NULL),
(79, '2021-02-06 07:42:08', NULL),
(82, '2021-02-06 07:46:09', NULL),
(79, '2021-02-12 22:03:31', NULL),
(79, '2021-02-13 09:25:03', NULL),
(79, '2021-02-17 12:04:59', NULL),
(79, '2021-05-16 00:47:03', NULL),
(82, '2021-05-16 01:05:07', NULL),
(79, '2021-05-21 05:02:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_tab`
--

DROP TABLE IF EXISTS `role_tab`;
CREATE TABLE IF NOT EXISTS `role_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_tab`
--

INSERT INTO `role_tab` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'instructor'),
(3, 'student');

-- --------------------------------------------------------

--
-- Table structure for table `section_assignment_tab`
--

DROP TABLE IF EXISTS `section_assignment_tab`;
CREATE TABLE IF NOT EXISTS `section_assignment_tab` (
  `assignment_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  KEY `section_id` (`section_id`),
  KEY `section_assignment_tab_ibfk_3` (`assignment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section_assignment_tab`
--

INSERT INTO `section_assignment_tab` (`assignment_id`, `section_id`) VALUES
(1, 9),
(2, 14);

-- --------------------------------------------------------

--
-- Table structure for table `section_tab`
--

DROP TABLE IF EXISTS `section_tab`;
CREATE TABLE IF NOT EXISTS `section_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section_tab`
--

INSERT INTO `section_tab` (`id`, `name`, `course_id`, `created_at`) VALUES
(1, 'SECTION 1A', 5, '2020-06-23 19:59:01'),
(2, 'SECTION 1B', 5, '2020-06-23 20:01:42'),
(3, 'SECTION 1A', 11, '2020-06-24 09:38:29'),
(4, 'SECTION 1A', 4, '2020-06-28 09:49:36'),
(5, 'SECTION 1B', 4, '2020-06-28 09:51:11'),
(6, 'SECTION 1C', 4, '2020-06-28 09:51:27'),
(7, 'SECTION 1C', 11, '2020-06-28 10:59:53'),
(8, 'SECTION 1A', 9, '2020-06-28 20:41:03'),
(9, 'SECTION 1A', 12, '2020-06-30 09:36:49'),
(10, 'SECTION 1B', 12, '2020-06-30 09:36:57'),
(11, 'SECTION 1A', 13, '2020-06-30 09:37:28'),
(12, 'SECTION 1B', 13, '2020-06-30 09:37:39'),
(13, 'BEGINNER', NULL, '2020-11-23 18:37:46'),
(14, 'SECTION 1', 15, '2021-02-04 17:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `section_user_tab`
--

DROP TABLE IF EXISTS `section_user_tab`;
CREATE TABLE IF NOT EXISTS `section_user_tab` (
  `user_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  KEY `section_user_tab_ibfk_1` (`section_id`),
  KEY `section_user_tab_ibfk_2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section_user_tab`
--

INSERT INTO `section_user_tab` (`user_id`, `section_id`) VALUES
(NULL, 9),
(NULL, 9),
(NULL, 9),
(66, 9),
(67, 9),
(68, 9),
(NULL, 10),
(NULL, 10),
(NULL, 10),
(82, 14),
(83, 14),
(79, 14);

-- --------------------------------------------------------

--
-- Table structure for table `student_tab`
--

DROP TABLE IF EXISTS `student_tab`;
CREATE TABLE IF NOT EXISTS `student_tab` (
  `student_metric_no` varchar(100) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  UNIQUE KEY `student_metric_no` (`student_metric_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_tab`
--

INSERT INTO `student_tab` (`student_metric_no`, `student_id`) VALUES
('stud0001', 61),
('stud0002', 63),
('stud0003', 65),
('stud0004', 66),
('stud0005', 67),
('stud0006', 68),
('stud0007', 69),
('stud0010', 70),
('stud0011', 71),
('stud0001uniten', 76),
('stud1c++', 82),
('stud2c++', 83);

-- --------------------------------------------------------

--
-- Table structure for table `submission_tab`
--

DROP TABLE IF EXISTS `submission_tab`;
CREATE TABLE IF NOT EXISTS `submission_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `c1` int(11) DEFAULT '0',
  `c2` int(11) DEFAULT '0',
  `c3` int(11) DEFAULT '0',
  `c4` int(11) DEFAULT '0',
  `c5` int(11) DEFAULT '0',
  `total_mark` int(11) DEFAULT '0',
  `finalize_mark` int(11) DEFAULT '0',
  `source_code` text,
  `output_produce` text,
  `compile_status` varchar(300) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `submitted_by` int(11) DEFAULT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `compile_message` text,
  `line_error` int(11) DEFAULT NULL,
  `feedback` text,
  PRIMARY KEY (`id`),
  KEY `submission_tab_ibfk_1` (`assignment_id`),
  KEY `submitted_by` (`submitted_by`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_tab`
--

INSERT INTO `submission_tab` (`id`, `assignment_id`, `c1`, `c2`, `c3`, `c4`, `c5`, `total_mark`, `finalize_mark`, `source_code`, `output_produce`, `compile_status`, `status`, `submitted_by`, `submitted_at`, `compile_message`, `line_error`, `feedback`) VALUES
(113, NULL, 2, 0, 0, 4, 0, 6, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Welcome To UPSI\";\r\n    return 0;\r\n}\r\n', 'Welcome To UPSI', 'Accepted', 'in-progress', NULL, '2020-10-27 18:10:23', 'ée', NULL, NULL),
(114, NULL, 2, 0, 0, 4, 0, 6, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Welcome To UPSI\";\r\n    return 0;\r\n}\r\n', 'Welcome To UPSI', 'Accepted', 'in-progress', NULL, '2020-10-27 18:14:06', 'ée', NULL, NULL),
(115, NULL, 1, 0, 0, 3, 0, 4, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello World\";\r\n    return 0;\r\n}\r\n', 'Hello World', 'Accepted', 'in-progress', NULL, '2020-11-01 07:20:39', 'ée', NULL, NULL),
(116, NULL, 2, 0, 0, 4, 0, 6, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello World!\";\r\n    return 0;\r\n}\r\n', 'Hello World!', 'Accepted', 'in-progress', NULL, '2020-11-01 11:10:30', 'ée', NULL, NULL),
(117, NULL, 1, 0, 0, 3, 0, 4, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello world\";\r\n    return 0;\r\n}\r\n', 'Hello world', 'Accepted', 'in-progress', NULL, '2020-11-01 11:26:58', 'ée', NULL, NULL),
(118, NULL, 1, 0, 0, 3, 0, 4, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello Wold\";\r\n    return 0;\r\n}\r\n', 'Hello Wold', 'Accepted', 'in-progress', NULL, '2020-11-01 11:27:11', 'ée', NULL, NULL),
(119, NULL, 2, 0, 0, 4, 0, 6, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello World!\";\r\n    return 0;\r\n}\r\n', 'Hello World!', 'Accepted', 'in-progress', NULL, '2020-11-01 11:27:41', 'ée', NULL, NULL),
(120, NULL, 1, 0, 0, 1, 0, 2, 0, 'offOverlay();', 'Compilation Error : \nCheck at line number 1\n==============\nCompiler Status:\nmain.cpp:1:13: error: expected constructor, destructor, or type conversion before â;â token\n    1 | offOverlay();\n      |             ^\n', 'Compilation Error', 'in-progress', NULL, '2020-11-01 11:29:32', 'main.cpp:1:13: error: expected constructor, destructor, or type conversion before â;â token\n    1 | offOverlay();\n      |             ^\n', 1, NULL),
(121, NULL, 1, 0, 0, 1, 0, 2, 0, '\r\n          <div id=\"overlay\">\r\n            <div id=\"overlay-text\">Processing....</div>\r\n          </div>', 'Compilation Error : \nCheck at line number 2\n==============\nCompiler Status:\nmain.cpp:2:11: error: expected unqualified-id before â<â token\n    2 |           <div id=\"overlay\">\n      |           ^\n', 'Compilation Error', 'in-progress', NULL, '2020-11-01 11:31:41', 'main.cpp:2:11: error: expected unqualified-id before â<â token\n    2 |           <div id=\"overlay\">\n      |           ^\n', 2, NULL),
(122, NULL, 2, 0, 0, 4, 0, 6, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello World!\";\r\n    return 0;\r\n}\r\n', 'Hello World!', 'Accepted', 'in-progress', NULL, '2020-11-01 11:32:28', 'ée', NULL, NULL),
(123, NULL, 1, NULL, NULL, 3, 3, 7, 7, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Welcome To UPSI\";\r\n    return 0;\r\n}\r\n', 'Welcome To UPSI', 'Accepted', 'finalize', NULL, '2020-11-01 11:33:07', 'ée', NULL, '<div><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAAAaCAYAAADljINzAAAGHklEQVR4Ae1bP28TSxDns6TIuUuP8gXi9wVQqFFIiyK6SDwJ8igo+ABpUiAr4k9HEYmKSDENBQ2WMK8kTdLElVMNmpmd29l/57VN7p39trDuvLc7Ozvz++3M7Nn3qt4GdP5z+g2m0wlMRydQ9U7gO95Pv8EgpfvzT3BFfSbw/XQF1pdaR2lvDZv3Ok+CAobWwPB/xkIhQiFaIVpvAwoRChEKEdaZCMdnB3D5tQ+HvS34+PUALs+278zhdq5Sj7SdXlnbL+fntY0Ih2/2HCL8eLMF1X4ffowPYPjKBSz1He/Bx3233RrZbfedndvPH9fu920Yjg/gsulDG0fzWkOdWS7Z14uu1gcoMza/b/OGPq92Xd2NrnYOJkJMj1DncI2tE2EwmsD06hO88IyWVnYHrqu83XxQPYJzI5cMZKIAApUNFHOa2UkCguQbdjWI4DmfgOUD0euT5aOYTVmOBSl+5356E6Lntd3jcrCPHRP2ift5/nV0mAg7cL35BG43n8BohkMGFfe7rXbgSPqioxURxJghaNG4ezA826v7EykT0SNG2FDm/I6Iyb3Ttg4QoVJpK5NiF47Ff9FrSIQq4ed5bZckwovzX3x2b87j+QwfHfwMPl/hOb589Hk+n/G7Z/fcdnX+DAKZ09g5/zaMMglwVD0iojgEiBrQAjMwOBoSw6xcZTwBRTmGvqvUQtUcIRHYYTYNUTuvJ0eH8lo33UeRWeQJqdnZ/lxKZ1lL7EpzKL10Hz3/WKKptaELsgg4jSxaT51ucT9X9w0Q273DVHY8S/f0XK5OqOsRDCcAMLmAl3ptifsoERiwv+Dzc1k8gh8Bb15m0YstfsZ9hQzNREBl06mRJcB1tdVY2NYE2HyQfqmWWDDXCRYA5CwEG0UA2y4OOkQ5BAztJLcwc/pKLqyIguMR8Ax0O4ekC0IGfq4LewtyARD3EV1CcLnPxX+Ra4IIs3QMAZcGJ8lqJIKyo4nASHZZ6zxzhX2XJoLdwQPh9IZXQC/G1f0XJYJJgzJqAU6DbC0Q6JgiQN3u5v4IYjY8t+t7BqhulzVvGFAzIDUR0kB05xW9dX99z88jYxRhqb8mHK4xN6WLEiEyX89dq+htr4sTgderNgZFBol+kt7yfOm5rD7WR/O0hRGBfp6go4EVTLt/UOiaVEn9/CGVGqFi/3lE6JlwTABCw1pHEKDrdiEIG792jHPqEhLByrB2004MdjsFyHmJQHM5+tjULZin3giMXmpeCxheazA22lfWlwYnrceLCK4dJbKJLPcq65OI6UdQq7c7bpH2DhFBFmOL5FkpUl0kz5sikWN34Zh2IOUMbJd6oc5XE+BQwCKHGYe3TgQ/Iii9GgERBXdirdG+4q/5iBCQbIa+2rbtEsHUAVjcBoa8s9RIjKqvlhALnRo1GdikF/GTInOCVO9k8XRB20Y7K9zVZU1xObq/vmf5kTFGd3zn4e64Mk/mNQruyHwzUyMe46YwqIPfniBZk5+c6I0y06TT/ljkPowIkr5M3fRoMMoplnWahIrbEyYhlltc5zjtAdxm1A5o+HP1HqHZGMZJwYlIop1AI6mS6LwNQx0FhDiS5+qder8Pw+xiWUUoAyabGkgNIOkcA8MH4fGZPBddI9coEaQe0OMzwBezT9DWTATcTJx1IkESMoJ+CTK9/HIDADcw/Ceyfm9MlAgIIgasHJHKT6BRoAU3H6F6xbP6CfSUyKSL6XC8W0/MVrgZ4PnjaRdX9YHI5XYf9AJAm4Pr0w0dEViOAWidvytgGedKrqydOm9E4LkseUWmTwxZm3MlPZReGhgNOjoyGsZcBrZtJoIU+fUayHZKP08n/vlMk7+XPjVqEl6eJYGgQVHuw7S6dZucwk8A+Pk+D7PJiFAcnmfAYqeO2un9GADG8DaTgIUImYYqgO8o4BP+e/svwM2Xo+zIVIiQMGQB/moBf1l/FSIUImTvmsuCrcvjCxEKEQoR1vkfal3efYpu3Uu71ici3L+Ak78BPpjPycP8QqkAs3vAbNsnf4wI9Lsf/ceYNlMOQ4LXfxmH+t/b1KXMtZKp1loQof/wBj48vYC+AuHjfQja2t5lynyrE2l+A1xf8sOdbTjNAAAAAElFTkSuQmCC\" alt=\"\" /><br />look at line 5: error</div>'),
(124, NULL, 2, NULL, NULL, 4, 3, 9, 9, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello World!\";\r\n    return 0;\r\n}\r\n', 'Hello World!', 'Accepted', 'finalize', 66, '2020-11-01 22:40:22', 'ée', NULL, '<img src=\"http://localhost/prograders/assets/img/tinymce/1604241815.png\" alt=\"\" width=\"317\" height=\"80\" />'),
(125, NULL, 2, 5, NULL, 4, 4, 15, 15, '#include <iostream>\n#include <string>\nusing namespace std;\nint main() {\n    // Write C++ code here\n    string myword;\n    getline (cin, myword);\n    cout <<\"Hello \"<< myword;\n\n    return 0;\n}', 'Hello Ali', 'Accepted', 'finalize', NULL, '2020-11-02 08:08:48', 'ée', NULL, 'Please insert some comments please'),
(126, NULL, 2, NULL, NULL, 4, 3, 9, 9, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main(){\r\n    cout<<\"Hello World\";\r\n    return 0;\r\n}\r\n', 'Hello World', 'Accepted', 'finalize', 68, '2020-11-02 10:16:25', 'ée', NULL, ''),
(133, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\n\npublic class Main{\n    public static void main(String args[]){\n        System.out.println(\"Hello\");\n    }\n}', 'Hello\n', 'Accepted', 'in-progress', NULL, '2021-01-28 10:11:14', 'ée', NULL, NULL),
(134, NULL, 2, 0, 0, 4, 0, 6, 0, 'import java.util.Scanner;\n\npublic class Main{\n    public static void main(String args[]){\n        System.out.println(\"19.625\");\n    }\n}', '19.625\n', 'Accepted', 'in-progress', NULL, '2021-01-28 10:19:09', 'ée', NULL, NULL),
(135, NULL, 2, 0, 0, 4, 0, 6, 0, 'import java.util.Scanner;\n\npublic class Main{\n    public static void main(String args[]){\n        System.out.println(\"19.625\");\n    }\n}', '19.625\n', 'Accepted', 'in-progress', NULL, '2021-01-28 10:19:42', 'ée', NULL, NULL),
(136, NULL, 2, 0, 0, 4, 0, 6, 0, 'import java.util.Scanner;\n\npublic class Main{\n    public static void main(String args[]){\n        System.out.println(\"19.625\");\n    }\n}', '19.625\n', 'Accepted', 'in-progress', NULL, '2021-01-28 10:21:09', 'ée', NULL, NULL),
(137, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\n\npublic class Main{\n    public static void main(String args[]){\n        System.out.println(\"19\");\n    }\n}', '19\n', 'Accepted', 'in-progress', NULL, '2021-01-28 10:21:24', 'ée', NULL, NULL),
(138, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\n\nclass Main{\n    public static void main(String args[]){\n        System.out.println(\"123\");\n    }\n}', '123\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:02:50', 'ée', NULL, NULL),
(139, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\n\nclass Main{\n    public static void main(String args[]){\n        System.out.println(\"123\");\n    }\n}', '123\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:03:13', 'ée', NULL, NULL),
(140, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\n\nclass Main{\n    public static void main(String args[]){\n        System.out.println(\"123\");\n    }\n}', '123\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:04:21', 'ée', NULL, NULL),
(141, NULL, 1, 0, 0, 1, 0, 2, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.142;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        area=c.calArea(r);\n        System.out.println(\"Area of cicle is \"+area);\n    }\n}', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:15: error: cannot find symbol\n        area=c.calArea(r);\n                       ^\n  symbol:   variable r\n  location: class Main\n1 error\n', 'Compilation Error', 'in-progress', NULL, '2021-01-28 22:11:00', 'Main.java:15: error: cannot find symbol\n        area=c.calArea(r);\n                       ^\n  symbol:   variable r\n  location: class Main\n1 error\n', NULL, NULL),
(142, NULL, 1, 0, 0, 1, 0, 2, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.142;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        area=c.calArea(radius);\n        System.out.println(\"Area of cicle is \"+area);\n    }\n}', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:15: error: variable radius might not have been initialized\n        area=c.calArea(radius);\n                       ^\n1 error\n', 'Compilation Error', 'in-progress', NULL, '2021-01-28 22:12:06', 'Main.java:15: error: variable radius might not have been initialized\n        area=c.calArea(radius);\n                       ^\n1 error\n', NULL, NULL),
(143, NULL, 1, 0, 0, 1, 0, 2, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.142;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        area=c.calArea(r);\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        System.out.println(\"Area of cicle is \"+area);\n    }\n}', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:15: error: cannot find symbol\n        area=c.calArea(r);\n                       ^\n  symbol:   variable r\n  location: class Main\n1 error\n', 'Compilation Error', 'in-progress', NULL, '2021-01-28 22:13:38', 'Main.java:15: error: cannot find symbol\n        area=c.calArea(r);\n                       ^\n  symbol:   variable r\n  location: class Main\n1 error\n', NULL, NULL),
(144, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.142;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        area=c.calArea(radius);\n        System.out.println(\"Area of cicle is \"+area);\n    }\n}', 'Area of cicle is 19.6375\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:14:56', 'ée', NULL, NULL),
(145, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.142;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        area=c.calArea(radius);\n        System.out.println(area);\n    }\n}', '19.6375\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:15:37', 'ée', NULL, NULL),
(146, NULL, 1, 0, 0, 3, 0, 4, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.14;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        area=c.calArea(radius);\n        System.out.println(\"Area of cicle is \"+area);\n    }\n}', 'Area of cicle is 19.625\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:16:06', 'ée', NULL, NULL),
(147, NULL, 1, 0, 0, 1, 0, 2, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.14;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        area=c.calArea(radius);\n        System.out.println(\"area);\n    }\n}', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:18: error: unclosed string literal\n        System.out.println(\"area);\n                           ^\n1 error\n', 'Compilation Error', 'in-progress', NULL, '2021-01-28 22:16:38', 'Main.java:18: error: unclosed string literal\n        System.out.println(\"area);\n                           ^\n1 error\n', NULL, NULL),
(148, NULL, 1, 0, 0, 1, 0, 2, 0, 'rt java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.14;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        area=c.calArea(radius);\n        System.out.println(area);\n    }\n}', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:1: error: class, interface, or enum expected\nrt java.util.Scanner;\n^\n1 error\n', 'Compilation Error', 'in-progress', NULL, '2021-01-28 22:17:15', 'Main.java:1: error: class, interface, or enum expected\nrt java.util.Scanner;\n^\n1 error\n', NULL, NULL),
(149, NULL, 2, 0, 0, 4, 0, 6, 0, 'import java.util.Scanner;\nimport java.lang.Math;\n\nclass Circle{\n    public double calArea(double r){\n        final double pi=3.14;\n        return pi*Math.pow(r,2);\n    }\n}\n\nclass Main{\n    public static void main(String args[]){\n        double radius, area;\n        Circle c= new Circle();\n        Scanner scan=new Scanner(System.in);\n        radius=scan.nextDouble();\n        area=c.calArea(radius);\n        System.out.println(area);\n    }\n}', '19.625\n', 'Accepted', 'in-progress', NULL, '2021-01-28 22:17:56', 'ée', NULL, NULL),
(150, NULL, 1, 0, 0, 1, 0, 2, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main() {\r\n       cout<<\"hello world\";\r\n      return 0;\r\n}\r\n', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:1: error: illegal character: \'#\'\n#include <iostream>\n^\nMain.java:1: error: class, interface, or enum expected\n#include <iostream>\n         ^\nMain.java:4: error: class, interface, or enum expected\nint main() {\n^\nMain.java:6: error: class, interface, or enum expected\n      return 0;\n      ^\nMain.java:7: error: class, interface, or enum expected\n}\n^\n5 errors\n', 'Compilation Error', 'in-progress', NULL, '2021-01-30 10:27:04', 'Main.java:1: error: illegal character: \'#\'\n#include <iostream>\n^\nMain.java:1: error: class, interface, or enum expected\n#include <iostream>\n         ^\nMain.java:4: error: class, interface, or enum expected\nint main() {\n^\nMain.java:6: error: class, interface, or enum expected\n      return 0;\n      ^\nMain.java:7: error: class, interface, or enum expected\n}\n^\n5 errors\n', NULL, NULL),
(151, NULL, 1, 0, 0, 1, 0, 2, 0, '#include <iostream>\r\n\r\nusing namespace std;\r\nint main() {\r\n       cout<<\"testing\";\r\n      return 0;\r\n}\r\n', 'Compilation Error : \nCheck at line number \n==============\nCompiler Status:\nMain.java:1: error: illegal character: \'#\'\n#include <iostream>\n^\nMain.java:1: error: class, interface, or enum expected\n#include <iostream>\n         ^\nMain.java:4: error: class, interface, or enum expected\nint main() {\n^\nMain.java:6: error: class, interface, or enum expected\n      return 0;\n      ^\nMain.java:7: error: class, interface, or enum expected\n}\n^\n5 errors\n', 'Compilation Error', 'in-progress', NULL, '2021-01-30 11:59:34', 'Main.java:1: error: illegal character: \'#\'\n#include <iostream>\n^\nMain.java:1: error: class, interface, or enum expected\n#include <iostream>\n         ^\nMain.java:4: error: class, interface, or enum expected\nint main() {\n^\nMain.java:6: error: class, interface, or enum expected\n      return 0;\n      ^\nMain.java:7: error: class, interface, or enum expected\n}\n^\n5 errors\n', NULL, NULL),
(152, 2, 2, 0, 0, 4, 0, 6, 0, '#include <iostream>\r\nusing namespace std;\r\n\r\nint main(){\r\n    cout<<\"Hello World\";\r\n    return 0;\r\n}\r\n', 'Hello World', 'Accepted', 'in-progress', 82, '2021-02-06 07:44:02', 'ée', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `university_tab`
--

DROP TABLE IF EXISTS `university_tab`;
CREATE TABLE IF NOT EXISTS `university_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `university_tab`
--

INSERT INTO `university_tab` (`id`, `name`, `created_at`, `created_by`) VALUES
(5, 'UNIVERSITI PUTRA MALAYSIA (UPM)', '2020-05-26 14:53:00', 1),
(6, 'UNIVERSITI PENDIDIKAN SULTAN IDRIS (UPSI)', '2020-05-26 14:55:00', 1),
(8, 'UNIVERSITI KEBANGSAAN MALAYSIA (UKM)', '2020-05-26 14:56:00', 1),
(9, 'UNIVERSITI MALAYA (UM)', '2020-05-26 14:57:00', 1),
(12, 'UNIVERSITI SAINS MALAYSIA (USM)', '2020-05-26 18:55:00', 1),
(13, 'UNIVERSITI TENAGA NASIONAL (UNITEN)', '2020-05-26 19:00:00', 1),
(14, 'TESTING UNI', '2020-12-15 10:18:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_tab`
--

DROP TABLE IF EXISTS `user_tab`;
CREATE TABLE IF NOT EXISTS `user_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `uni_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uni_id` (`uni_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_tab`
--

INSERT INTO `user_tab` (`id`, `email`, `password`, `name`, `uni_id`, `created_at`, `role_id`) VALUES
(66, 'stud4@upsi.org', '$2y$10$qAc4/5dKL4BvwEv57g.6dOQpagzfixvv3Iy341pZujhhcvTUKu3rW', 'STUD0004', 6, '2020-10-09 22:16:07', 3),
(67, 'stud5@upsi.org', '$2y$10$yABVNh8hctfJYjjoQinrZO8WO7TeEx2yhhTpSoq9EZRnZtut1/Rgu', 'STUD0005', 6, '2020-10-09 22:17:49', 3),
(68, 'stud6@upsi.org', '$2y$10$/Mi8BMyHkCSEMwW0we8WD.ef3i6PuDmke5sFv7F.LbXJwTfldQGzi', 'STUD0006', 6, '2020-10-09 22:18:32', 3),
(69, 'stud7@upsi.org', '$2y$10$ppEW4RSxsVe26RRcA80lk.F98EfntJ/n7wAomsRhl7PaYi7xfDXsO', 'STUD0007', 6, '2020-10-09 22:19:09', 3),
(76, 'stud1@uniten.ll', '$2y$10$7uzEBcaw62.t7Uolt1JofeMHr7xCC3EJDNc1y3s8lejP53WZFszDC', 'STUD ONE', 13, '2020-12-23 13:24:10', 3),
(79, 'instc++@prograders.org', '$2y$10$FxLTT4SFTHojknyfzMX83.5pivQPNGDTFF77Bn4vMVX0ef87orbxe', 'INST C++ 01', 14, '2021-02-04 11:11:44', 2),
(80, 'instc++02@prograders.org', '$2y$10$ZSmiOWjKj5T2EZsX94KqJe4293FKB0V1ZTeZWY/38xEzd6R9ujlpm', 'INST C++ 02', 14, '2021-02-04 11:18:04', 2),
(81, 'hujaepa@gmail.com', '$2y$10$zuMYJejy2GzQhza3u5TyCO92g3oJB1AvW41F3TRjGgiKwlunkn6fW', 'HUJAEPA', 6, '2021-02-04 17:48:56', 2),
(82, 'stud1c++@prograders.org', '$2y$10$TXe5aWjeL926x4u1WKbRXegtdPRIcZZFKecaQBICCnJ/iIb/1lm3G', 'STUD1C++', 14, '2021-02-05 07:48:14', 3),
(83, 'stud2c++@prograders.org', '$2y$10$9/7LMMEeUbCZ6F08EAdmB.N835m0cuLHYwwEMy.tkGZzMq/KtMUQe', 'STUD2 C++', 14, '2021-02-05 07:48:43', 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignment_grade_setup_tab`
--
ALTER TABLE `assignment_grade_setup_tab`
  ADD CONSTRAINT `assignment_grade_setup_tab_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `assignment_tab`
--
ALTER TABLE `assignment_tab`
  ADD CONSTRAINT `assignment_tab_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `course_tab`
--
ALTER TABLE `course_tab`
  ADD CONSTRAINT `course_tab_ibfk_1` FOREIGN KEY (`university_id`) REFERENCES `university_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `course_user_tab`
--
ALTER TABLE `course_user_tab`
  ADD CONSTRAINT `course_user_tab_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `course_user_tab_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `first_time_login_tab`
--
ALTER TABLE `first_time_login_tab`
  ADD CONSTRAINT `first_time_login_tab_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `logged_user_tab`
--
ALTER TABLE `logged_user_tab`
  ADD CONSTRAINT `logged_user_tab_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `section_assignment_tab`
--
ALTER TABLE `section_assignment_tab`
  ADD CONSTRAINT `section_assignment_tab_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `section_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `section_assignment_tab_ibfk_3` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `section_tab`
--
ALTER TABLE `section_tab`
  ADD CONSTRAINT `section_tab_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `section_user_tab`
--
ALTER TABLE `section_user_tab`
  ADD CONSTRAINT `section_user_tab_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `section_user_tab_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `submission_tab`
--
ALTER TABLE `submission_tab`
  ADD CONSTRAINT `submission_tab_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `submission_tab_ibfk_2` FOREIGN KEY (`submitted_by`) REFERENCES `user_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `user_tab`
--
ALTER TABLE `user_tab`
  ADD CONSTRAINT `user_tab_ibfk_1` FOREIGN KEY (`uni_id`) REFERENCES `university_tab` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
