//fetch data to populate section list
$(document).ready(function() {
  // var url=window.location.href.split('/');
  var datatable=$('#section_list').DataTable({  
       processing:true,  
       serverSide:true,
       order:[],  
       ajax:{  
            url:$("#url").val()+"assignment/processSectionList",
            type:"post"  
       },
       language: {
        searchPlaceholder: "Enter section name"
       },
       ordering: false,
       columns: [
          {render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
          }},
          null,
          null,
          null,
          null
      ]
  });//end of data table
});//end of document ready



