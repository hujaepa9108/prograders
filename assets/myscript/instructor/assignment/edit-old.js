//OVERLAY STUFF
function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },3000);
}
var url=window.location.href.split("/");
var assignmentIdOri=url[url.length-1];
var sectionIdOri=url[url.length-2];
//FORM STUFF(eg ckeditor instance, ace editor instance etc)
// CKEDITOR.replace("content");
tinymce.init({
    selector: '#content',
    plugins: ["table","image","lists", "charmap","superscript", "subscript"],
    toolbar: "undo redo | styleselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | numlist bullist | outdent indent | link image | code | superscript subscript | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol",
    height:"400",
    add_form_submit_trigger : true,
    forced_root_block : false,
    lists_indent_on_tab: false,
    automatic_uploads: true,
    file_picker_types: 'image', 
	paste_data_images:true,
	relative_urls: false,
    remove_script_host: false,
    images_upload_url: $("#url").val()+"upload/tinymce_upload",
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', $("#url").val()+'upload/tinymce_upload');
        xhr.onload = function() {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            
            json = JSON.parse(xhr.responseText);
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
    }
});
$('input[name="duedate"]').daterangepicker({
	timePicker: true,
	startDate: moment().startOf('hour'),
	// endDate: moment().startOf('hour').add(32, 'hour'),
	locale: {
	  format: 'DD/M/Y hh:mm A'
	},
	drops:"up",
	autoUpdateInput : false
});

$('input[name="duedate"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/M/Y hh:mm A') + ' - ' + picker.endDate.format('DD/M/Y hh:mm A'));
});

//SOURCE CODE EDITOR
var codeEditor = ace.edit("codeEditor");
codeEditor.setTheme("ace/theme/monokai");
codeEditor.session.setMode("ace/mode/c_cpp");

//INPUT TEST CASE
var inputTestCase = ace.edit("inputTestCase");
inputTestCase.setTheme("ace/theme/monokai");

//COMPILER
var compiler = ace.edit("compiler");
compiler.setTheme("ace/theme/terminal");
compiler.setReadOnly(true);
compiler.renderer.$cursorLayer.element.style.opacity=0;
var session =compiler.session;

//OUTPUT TEST CASE
var outputTestCase = ace.edit("outputTestCase");
outputTestCase.setTheme("ace/theme/monokai");
outputTestCase.setReadOnly(true);
outputTestCase.renderer.$cursorLayer.element.style.opacity=0;

//API JUDGE0 STUFF
$("#compile").click(function(e){
    e.preventDefault();
    $(".empty-code-error").html("");
    $(".codePanel").removeClass("border border-danger");
    $(".empty-output-error").html("");
    $(".outputTerminal").removeClass("border border-danger");
    compiler.setValue("Processing\n");
    outputTestCase.setValue("");

    if(codeEditor.getValue()===""||codeEditor.getValue()==="undefined"){
        $(".empty-code-error").html("<p class='badge badge-danger'>Please add your code first before proceeds to compiling!</p>");
        $(".codePanel").addClass("border border-danger");
        return;
    }
    else{
        //GET AUTH
        /*$.ajax({
            url: 'http://www.prograders.org:8080/authenticate',
            type: 'post',
            headers:{"X-Auth-Token": "f6583e60-b13b-4228-b554-2eb332ca64e7"},
            success:function(res,status){
                if(status==="success"){*/
                    $.ajax({
                        url:$("#url").val()+"assignment/getCourseInfo",
                        type:"post",
                        success:function(res){
                            //GET LANGUAGE ID
                            parseData=JSON.parse(res);
                            var lang_id=parseData.course.pl_id;
                            // console.log(inputTestCase.getValue());
                            var stdin=(inputTestCase.getValue()!=="")?inputTestCase.getValue():null;
                            //CREATE SUBMISSION
                            var dataSubmit={
                                source_code: codeEditor.getValue(),
                                language_id: lang_id,
                                stdin: stdin
                            };
                            $.ajax({
                                url: 'http://www.prograders.org:8080/submissions/?base64_encoded=false&wait=false',
                                type: 'post',
                                data: JSON.stringify(dataSubmit),
                                headers: {"Content-Type": "application/json"},
                                success: function(res){
                                    
                                    setTimeout(fetchSubmission.bind(null, res.token), 1500);
                                }
                            });
                            //CREATE SUBMISSION
                        }
                    });
                /*}//END IF
            }
        });*/
        //END AUTH
    }
});

function fetchSubmission(token) {
    $.ajax({
        url: "http://www.prograders.org:8080/submissions/"+token+"?base64_encoded=true&fields=stdout,stderr,status_id,language_id,compile_output,status",
        type: 'get',
        headers: {"Content-Type": "application/json"},
        success: function(res){
            if ((res.status === undefined || res.status.id <= 2)) {
                setTimeout(fetchSubmission.bind(null, token), 1500);
            }
            else{
                if(res.status.id==3){
                    session.insert({
                    row:session.getLength(),
                    column:0},res.status.description);
                    outputTestCase.setValue(atob(res.stdout));
                }
                else{
                    session.insert({
                    row:session.getLength(),
                    column:0},res.status.description+": \n"+atob(res.compile_output));
                    outputTestCase.setValue("ERROR CODE:"+res.status.id+"\nFail to generate output because of "+res.status.description);
                }
            }
        }
    });
}

//CANCEL BUTTON ONCLICK
$("#cancel").click(function(event) {
    bootbox.confirm({
        message: "Are you sure you want to exit without saving? ",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if(result){
                window.location.href=$("#url").val()+"assignment/list/"+sectionIdOri;
            }
        }
    });
});

//CLEARING DATE RANGE
$("#cleardates").click(function(e) {
    e.preventDefault();
    $("input[name='duedate']").val("");
});

//FORM VALIDATE
$("#add_assignment").validate({
    //ERROR HANDLING//
    ignore: [], //enable validation on hidden field
    highlight: function(element) {
        $(element).closest('.form-control').addClass('is-invalid');
    },

    unhighlight: function(element) {
        $(element).closest('.form-control').removeClass('is-invalid');
    },

    errorElement: 'span',

    errorClass: 'badge badge-danger',
    
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) 
            error.insertAfter(element.parent());
        else 
            error.insertAfter(element);
    },
    //END OF ERROR HANDLING//
    rules:{
        title:{
            required:true
            // remote:  $("#url").val()+"assignment/checkExistTitle"
        },
        "sectionId[]":{
            required:true
        },
        duedate:{
            required:true
        },
        item_2:{
            required:true
        },
        item_3:{
            required:true
        }
    },
    messages:{
        "sectionId[]":{
            required: "Please assigns at least one of the sections"
        },
        title:{
            remote: "Title exist!"
        }
    },
    
    submitHandler: function(form,event) {
        event.preventDefault();
        tinyMCE.triggerSave();
        // setOverlay();
        // 
        if(codeEditor.getValue()=="" && $("[name='type']").val()=="active"){
            console.log ("test");
            return;
        }
        if(outputVal){
            return;
        }
        var getOutput=outputTestCase.getValue();
        if(outputTestCase.getValue()=="" || getOutput.includes("ERROR CODE")){
            if($("[name='type']").val()=="active")
                return;
        }
        var url = $("#url").val() + "assignment/processEdit/"+sectionIdOri+"/"+assignmentIdOri;//url setup
        var codeEditorVal = codeEditor.getValue();
        var inputVal = inputTestCase.getValue();
        var outputVal = outputTestCase.getValue();
        var data = $(form).serialize()+"&source_code="+codeEditorVal+"&input="+inputVal+"&output="+outputVal;//form serialize for jquery validation
        $.ajax({
            url: url,
            type: 'post',
            beforeSend: function() {
                setOverlay();
            }, 
            dataType: 'json',
            data: data,
            success: function(res){
                offOverlay();
                if(res.val){
                    var sectionId=$("input[name='sectionId[]']:checkbox:checked").val();
                    bootbox.alert(res.msg,function(){ 
                        window.location.href=$("#url").val()+"assignment/list/"+btoa(sectionId);
                    });
                }
            }
        }).fail(function(jqXHR,status,errorThrown) {
            console.log("Error : "+errorThrown);
        });
    }
});//end of form validate

//FORM SUBMISSION <DRAFT>
$("#draft").click(function(){
    $("[name='type']").val("draft");//set save type to draft
    $(".empty-code-error").html("");
    $(".codePanel").removeClass("border border-danger");
    $(".empty-output-error").html("");
    $(".empty-content-error").html("");
    $(".outputTerminal").removeClass("border border-danger");
    $('[name="duedate"],[name="item_2"],[name="item_3"]').each(function (idx,elm) {
        $(this).rules('remove');
        $(this).removeClass("is-invalid");//remove all the invalid classes
    });
    $("#add_assignment").submit(); 
});//end of draft

//FORM SUBMISSION <ACTIVE>
$("#active").click(function(){
    $(".empty-code-error").html("");
    $(".codePanel").removeClass("border border-danger");
    $(".empty-output-error").html("");
    $(".outputTerminal").removeClass("border border-danger");
    // var errorCount=0;
    $("[name='type']").val("active");//set save type as active

    if(tinymce.get("content").getContent()==""){
        // $(".empty-content-error").css("border-color","red");
        $(".empty-content-error").html("Please add the assignment description.");
    }
    else if(tinymce.get("content").getContent()!=""){
        $(".empty-content-error").html("");
    }

    $('[name="duedate"],[name="item_2"],[name="item_3"]').each(function (idx,elm) {
        $(this).rules('add',"required");
    });

    if(codeEditor.getValue()==="undefined" || codeEditor.getValue()===""){
        $(".empty-code-error").html("<p class='badge badge-danger'>There is no source code added!</p>");
        $(".codePanel").addClass("border border-danger"); 
    }

    if(outputTestCase.getValue()==="undefined" || outputTestCase.getValue()===""){
        $(".empty-output-error").html("<p class='badge badge-danger'>There is no output generated!</p>");
        $(".outputTerminal").addClass("border border-danger"); 
    }

    var compile_error=outputTestCase.getValue();//check if there is compilation error
    if(compile_error.includes("ERROR CODE")){
        $(".empty-output-error").html("<p class='badge badge-danger'>Save as active requires free from compilation error.</p>");
        $(".outputTerminal").addClass("border border-danger"); 
    }
    // if(errorCount==0)
    $("#add_assignment").submit(); 
});//end of draft

//BACK BUTTON