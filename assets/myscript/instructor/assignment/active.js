//fetch data to populate course list
$(document).ready(function() {
    var url=window.location.href.split("/");
    var sectionId=url[url.length-1];
    $('#assignment_list').DataTable({  
         processing:true,  
         serverSide:true,
         order:[],  
         ajax:{  
              url:$("#url").val()+"assignment/processActiveList/"+sectionId,  
              type:"post"  
         },
         language: {
          searchPlaceholder: "by 'assignment title' "
         },
         ordering: false,
         columns: [
          {
            render: function (data, type, row, meta) {
                   return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
            null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ] 
    });//end of data table
  });//end of document ready
  
    //OVERLAY STUFF
    function setOverlay(){
        $("#overlay").css({
            display : "block"
        });
    }
  
    function offOverlay() {
        setTimeout(function() {
            $("#overlay").css({
                display : "none"
            });
        },500);
    }
  
    function removeAssgmt(assignmentId,title) {
      bootbox.confirm({ 
        size: "medium",
        message: "<span class='bootbox-custom-format'>Are you sure you want to <b><i>permanently</i></b> <span class='bootbox-red-text'>delete</span> this assignment (\""
        +title+"\") ?</span>",
        callback: function(result){
          if(result)
            removeAjax(assignmentId);
        }
      });
    }
  
    function removeAjax(assignmentId){
      var formData =  {
        assignmentId: assignmentId
      };
      var url = $("#url").val() + "assignment/procDelete";//url setup
      $.ajax({
          url: url,
          type: 'post',
          beforeSend: function() {
              setOverlay();
          }, 
          dataType: 'json',
          data: formData,
          success: function(res){
            console.log(res);
              offOverlay();
              if(res.val)
                location.reload();
              else
                alert(res.msg);
          }
      }).fail(function(jqXHR,status,errorThrown) {
          console.log("Error : "+errorThrown);
      });
    }