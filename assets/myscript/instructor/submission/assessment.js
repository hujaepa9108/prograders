/*
NOTES: tinggal nak betulkan url ajax... cek kat form
*/
$(document).ready(function(){
    //OVERLAY STUFF
    function setOverlay(){
        $("#overlay").css({
            display : "block"
        });
    }

    function offOverlay() {
        setTimeout(function() {
            $("#overlay").css({
                display : "none"
            });
        },3000);
    }

    var Range = ace.require('ace/range').Range;
    var codeEditor = ace.edit("codeEditor");
    codeEditor.setTheme("ace/theme/monokai");
    codeEditor.setReadOnly(true);
    codeEditor.renderer.$cursorLayer.element.style.opacity=0;
    codeEditor.session.setMode("ace/mode/c_cpp");
    codeEditor.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });

    var Range = ace.require('ace/range').Range;
    var answerCode = ace.edit("answerCode");
    answerCode.setTheme("ace/theme/monokai");
    answerCode.setReadOnly(true);
    answerCode.renderer.$cursorLayer.element.style.opacity=0;
    answerCode.session.setMode("ace/mode/c_cpp");
    answerCode.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });

    var output = ace.edit("output");
    output.setTheme("ace/theme/monokai");
    output.setReadOnly(true);
    output.renderer.$cursorLayer.element.style.opacity=0;
    output.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });
    
    var stdout = ace.edit("stdout");
    stdout.setTheme("ace/theme/monokai");
    stdout.setReadOnly(true);
    stdout.renderer.$cursorLayer.element.style.opacity=0;
    stdout.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });

    //highlighting test case
    var totalLine=$("#totalLine").val();
    for(var i=0;i<totalLine;i++){
        var from=$("#line_"+i).val()-1;
        var to=$("#line_"+i).val()-1;
        output.session.addMarker(new Range(from, 0, to, 1), "myMarker", "fullLine");
        stdout.session.addMarker(new Range(from, 0, to, 1), "myMarker", "fullLine"); 
    }
    
    //feedback tinymce stuff
    tinymce.init({
        selector: '#feedback',
        plugins: ["table","image","lists"],
        toolbar: "undo redo | styleselect | forecolor | bold italic | alignleft aligncenter alignright alignjustify | numlist bullist | outdent indent | link image | code",
        height:"400",
        add_form_submit_trigger : true,
        forced_root_block : false,
        lists_indent_on_tab: false,
        automatic_uploads: true,
        file_picker_types: 'image', 
        paste_data_images:true,
        relative_urls: false,
        remove_script_host: false,
        images_upload_url: $("#base_url").val()+"upload/tinymce_upload",
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', $("#url").val()+'upload/tinymce_upload');
            xhr.onload = function() {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                
                json = JSON.parse(xhr.responseText);
                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        }
    });
    var total=0;

    //onclick add mark (C1)
    $("[name='c1']:radio").click(function(){
       $("#c1_mark").html($(this).val());
        // total+=$(this).val();
    });

    //onclick add mark (C2)
    $("[name='c2']:radio").click(function(){
        $("#c2_mark").html($(this).val());
        $(".c2-error").html("");
        // total+=$(this).val();
    });

    //onclick add mark (C3)
    $("[name='c3']:radio").click(function(){
        $("#c3_mark").html($(this).val());
        $(".c3-error").html("");
        // total+=$(this).val();
    });

    //onclick add mark (C4)
    $("[name='c4']:radio").click(function(){
        $("#c4_mark").html($(this).val());
        // total+=$(this).val();
    });

    //onclick add mark (C5)
    $("[name='c5']:radio").click(function(){
        $("#c5_mark").html($(this).val());
        $(".c5-error").html("");
        // total+=$(this).val();
    });
    
    // $("#total_mark").html(total);
    $("input[type=radio]").click(function() {
        var total = 0;
        $("input[type=radio]:checked").each(function() {
            total += parseFloat($(this).val());
        });
        $("#total_mark").html(total);
    });

    $("#assessment").validate({
        ignore: [],  // ignore NOTHING
        rules: {
            "c2": {
                required: true
            },
            "c3": {
                required: true
            },
            "c5": {
                required: true
            },
        },
        messages:{
            "c2":{
                required:"&nbsp;<span class='badge badge-danger'>!</span>"
            },
            "c3":{
                required:"&nbsp;<span class='badge badge-danger'>!</span>"
            },
            "c5":{
                required:"&nbsp;<span class='badge badge-danger'>!</span>"
            }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "c2" ){
                error.appendTo("#c2-tab");
                $(".c2-error").html("*This section is required to fill in.<br/>");
            }
            if(element.attr("name") == "c3" ){
                $(".c3-error").html("*This section is required to fill in.<br/>");
                error.appendTo("#c3-tab");
            }
            if(element.attr("name") == "c5" ){
                $(".c5-error").html("*This section is required to fill in.<br/>");
                error.appendTo("#c5-tab");
            }
        },
        submitHandler: function(form,event) {
            var url=window.location.href.split("/");
            var submitId=url[url.length-1];
            var assignmentId=url[url.length-2];
            var sectionId=url[url.length-3];
            var totalMark=$("#total_mark").text();
            event.preventDefault();
            tinyMCE.triggerSave();
            var data = $(form).serialize()+"&total_mark="+totalMark+"&submit_id="+submitId;
            $.ajax({
                url: $("#url").val()+"submission/procFinalize",
                type: 'post',
                beforeSend: function() {
                    setOverlay();
                }, 
                dataType: 'json',
                data: data,
                success: function(res){
                    offOverlay();
                    if(res.val){
                        bootbox.alert(res.msg,function(){ 
                            window.location.href=$("#url").val()+"assignment/detail/"+sectionId+"/"+assignmentId;
                        });
                    }
                }
            }).fail(function(jqXHR,status,errorThrown) {
                console.log("Error : "+errorThrown);
            });
        }
    });
    
    $('#finalize').on('click', function (e) {
        // e.preventDefault();
        $("#assessment").valid();
        var ele = $("#assessment :input.error:first");
        if (ele.is(':hidden')) {
            var tabToShow = ele.closest('.tab-pane');
            $('#assessment .nav-tabs a[href="#' + tabToShow.attr('id') + '"]').tab('show');
        }
    });
});
