//fetch data to populate section list
$(document).ready(function() {
  var url=location.pathname.split('/');
  $('#section_list').DataTable({  
       processing:true,  
       serverSide:true,
       order:[],  
       ajax:{  
            url:$("#url").val()+"section/processList/"+url[4],  
            type:"post"  
       },
       language: {
        searchPlaceholder: "Enter section name"
       }, 
       columnDefs:[  
	        {  
             targets:[2],  
             orderable:false,  
	        },  
       ],  
  });//end of data table
});//end of document ready

//OVERLAY STUFF
function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },500);
}

//To change section label
function edit(sectionId,sectionLabel) {
   $("#section-edit-modal").modal({
            backdrop: 'static',
            keyboard: false
        });
  $("#section-edit-modal").on("shown.bs.modal",function(event){
    $("#sectionLabel").val(sectionLabel);
    $("#sectionId").val(btoa(sectionId));
  });
}



// function remove(userid,username){
//   bootbox.confirm({ 
//     size: "medium",
//     message: "<span class='bootbox-custom-format'>Are you sure you want to delete \""
//     +username+"\" course ? </span>",
//     callback: function(result){
//       if(result)
//         removeAjax(userid);
//     }
//   });
// }

// function removeAjax(userid){
//   var formData =  {
//     userid: userid
//   };
//   console.log(formData);
//   var url = $("#url").val() + "instructor/processdelete/"+userid;//url setup
//   $.ajax({
//       url: url,
//       type: 'post',
//       beforeSend: function() {
//           setOverlay();
//       }, 
//       dataType: 'json',
//       data: formData,
//       success: function(res){
//         console.log(res);
//           offOverlay();
//           if(res.msg=="success")
//             location.reload();
//             // console.log(res);
//           else
//             alert(res.msg);
//       }
//   }).fail(function(jqXHR,status,errorThrown) {
//       console.log("Error : "+errorThrown);
//   });
// }