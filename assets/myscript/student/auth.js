$("#change").click(function(e){
    e.preventDefault();
    var passwd1=$("#password1").val();
    var passwd2=$("#password2").val();

    if(passwd1==="" || passwd1==="undefined"){
        $("#error_msg").text("Please filled in the fields!");
    }
    else if(passwd2==="" || passwd2==="undefined"){
        $("#error_msg").text("Please filled in the fields!");
    }
    else if(passwd1!==passwd2){
        $("#error_msg").text("Password does not matched!");
    }
    else{
        $("#changePasswd").submit();
    }
});