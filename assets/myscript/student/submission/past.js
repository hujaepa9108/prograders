$(document).ready(function() {
    var url=window.location.href.split("/");
    var assignmentId=url[url.length-1];
    $('#past_list').DataTable({  
         processing:true,  
         serverSide:true,
         order:[],
         ajax:{  
              url:$("#url").val()+"assignment/procPastSub/"+assignmentId,  
              type:"post"  
         },
         searching:false,
         ordering: false,
         columns: [
          {
            render: function (data, type, row, meta) {
                   return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
            null,
            null,
            null,
            null,
            null
        ] 
    });//end of data table
});//end of document ready
  