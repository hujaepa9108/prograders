$(document).ready(function(){
    var Range = ace.require('ace/range').Range;
    var codeEditor = ace.edit("codeEditor");
    codeEditor.setTheme("ace/theme/monokai");
    codeEditor.setReadOnly(true);
    codeEditor.renderer.$cursorLayer.element.style.opacity=0;
    codeEditor.session.setMode("ace/mode/c_cpp");
    codeEditor.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });

    if($("#line_error").val()>0){
        var codeLineFrom=$("#line_error").val()-1;
        var codeLinTo=$("#line_error").val()-1;
        codeEditor.session.addMarker(new Range(codeLineFrom, 0, codeLinTo, 1), "myMarker2", "fullLine");
    }

    var output = ace.edit("output");
    output.setTheme("ace/theme/monokai");
    output.setReadOnly(true);
    output.renderer.$cursorLayer.element.style.opacity=0;
    output.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });
    
    var stdout = ace.edit("stdout");
    stdout.setTheme("ace/theme/monokai");
    stdout.setReadOnly(true);
    stdout.renderer.$cursorLayer.element.style.opacity=0;
    stdout.setOptions({
        fontFamily: "tahoma",
        fontSize: "11pt"
    });

    //highlighting test case
    var totalLine=$("#totalLine").val();
    for(var i=0;i<totalLine;i++){
        var from=$("#line_"+i).val()-1;
        var to=$("#line_"+i).val()-1;
        output.session.addMarker(new Range(from, 0, to, 1), "myMarker", "fullLine");
        stdout.session.addMarker(new Range(from, 0, to, 1), "myMarker", "fullLine"); 
    }

    $("#showMsg").hide();
    // $("#copy").click(function showAlert() {
    //     $("#showMsg").fadeIn(500,function(){
    //         codeEditor.selectAll();
    //         document.execCommand('copy');
    //         $(this).fadeOut(500);
    //     });
    // });
});
