$(document).ready(function() {
    $('#assignment_list').DataTable({  
         processing:true,  
         serverSide:true,
         order:[],
         ajax:{  
              url:$("#url").val()+"submission/procInProgressList",  
              type:"post"  
         },
         language: {
          searchPlaceholder: "by 'assignment title' "
         },
        //  bLengthChange: false,
        //  searching: false,
         ordering: false,
         columns: [
          {
            render: function (data, type, row, meta) {
                   return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
            null,
            null,
            null,
            null
        ] 
    });//end of data table
});//end of document ready