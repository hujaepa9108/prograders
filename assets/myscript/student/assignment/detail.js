//OVERLAY STUFF
function setOverlay(){
    $("#overlay").css({
        display : "block"
    });
}

function offOverlay() {
    setTimeout(function() {
        $("#overlay").css({
            display : "none"
        });
    },3000);
}

//COUNTDOWN SETUP
var start_date=$("#start_date").val();
var end_date=$("#end_date").val();

// Set the date we're counting down to
var countDownDate = new Date(end_date).getTime();
var allowDate = new Date(start_date).getTime();
// Update the count down every 1 second
var x = setInterval(function() {
  // Get today's date and time
  var now = new Date().getTime();
  if(now >= allowDate) {
    // Find the distance between now and the count down date
    var distance = countDownDate - now;
    $("#submit").attr("disabled", false);
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "Has Passed Due Date. You Cannot Submit Your Answer's Anymore.";
        // $("#submit").attr("disabled", true);
        $("#submit").remove();
    }
  }
  else{
    document.getElementById("countdown").innerHTML = "You cannot submit your answer yet.";
    $("#submit").attr("disabled", true);
  }
}, 1000);

//SOURCE CODE EDITOR
var codeEditor = ace.edit("codeEditor");
codeEditor.setTheme("ace/theme/monokai");
codeEditor.session.setMode("ace/mode/c_cpp");

//API JUDGE0 STUFF
$("#submit").click(function(e){
    e.preventDefault();
    $(".empty-code-error").html("");
    $(".codePanel").removeClass("border border-danger");

    if(codeEditor.getValue()===""||codeEditor.getValue()==="undefined"){
        $(".empty-code-error").html("<p class='badge badge-danger'>Please add your code first before proceeds to compiling!</p>");
        $(".codePanel").addClass("border border-danger");
        return;
    }
    else{
        
    setOverlay();
        //GET AUTH
        $.ajax({
            url: 'http://www.prograders.org:8080/authenticate',
            type: 'post',
            headers:{"X-Auth-Token": "f6583e60-b13b-4228-b554-2eb332ca64e7"},
            success:function(res,status){
                if(status==="success"){
                    //CREATE SUBMISSION
                    var dataSubmit={
                        source_code: codeEditor.getValue(),
                        language_id: $("#lang_id").val(),
                        stdin: $("#input").val(),
                    };
                    $.ajax({
                        url: 'http://www.prograders.org:8080/submissions/?base64_encoded=false&wait=false',
                        type: 'post',
                        data: JSON.stringify(dataSubmit),
                        headers: {"Content-Type": "application/json"},
                        success: function(res){
                            setTimeout(fetchSubmission.bind(null, res.token), 1500);
                        }
                    });
                }
            }//END success
        });
        //END AUTH
    }
});

function fetchSubmission(token) {
    var url=window.location.href.split("/");
    var assignmentId=url[url.length-1];
    $.ajax({
        url: "http://www.prograders.org:8080/submissions/"+token+"?base64_encoded=true&fields=stdout,stderr,status_id,language_id,compile_output,status,time,stderr,compile_output",
        type: 'get',
        headers: {"Content-Type": "application/json"},
        success: function(res){
            if ((res.status === undefined || res.status.id <= 2)) {
                setTimeout(fetchSubmission.bind(null, token), 1500);
            }
            else{
                var data={
                    statusId : res.status.id,
                    statusDesc: res.status.description,
                    compile_message:atob(res.compile_output),
                    source_code: codeEditor.getValue(),
                    stdout: $("#output").val(),//orioutput
                    output: atob(res.stdout),//youroutput
                    assignmentId: assignmentId
                };
                // if(res.status.id==3)
                // //     compareOutput(data);
                // else if(res.status.id>=5 && res.status.id<=12)
                // console.log(atob(res.stderr));
                performAssess(data);
            }
        }
    });
}

// function compareOutput(data){
//     var string1=data.stdout.trim().replace(/\s/g, "").toLowerCase();
//     var string2=data.output.trim().replace(/\s/g, "").toLowerCase();
//     $.ajax({
//         url: "https://text-similarity-calculator.p.rapidapi.com/stringcalculator.php?ftext="+string1+"&stext="+string2,
//         type: 'GET',
//         async: true,
//         crossDomain: true,
//         headers: {
//             "x-rapidapi-host": "text-similarity-calculator.p.rapidapi.com",
//             "x-rapidapi-key": "35abad9fa6msh9ac3a4e4c74c8edp15f100jsn11dbf8f7bb9b"
//         },
//         success: function(res) { 
//             data.percent=res.percentage;
//             console.log(data.percent);
//             saveDB(data);
//         }
//     });
// }

function performAssess(data) {
    $.ajax({
        url: $("#url").val()+"submission/assess1",
        type: 'POST',
        beforeSend: function() {
            setOverlay();
        }, 
        data: data,
        success: function(res){
            offOverlay();
            var parseData=JSON.parse(res);
            if(parseData.val)
                location.reload();
                // alert(parseData.msg);
        }
    });

}