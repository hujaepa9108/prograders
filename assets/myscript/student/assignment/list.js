$(document).ready(function() {
    var sectionId=$("#sectionId").val();
    $('#assignment_list').DataTable({  
         processing:true,  
         serverSide:true,
         order:[],
         ajax:{  
              url:$("#url").val()+"assignment/processStudList/"+sectionId,  
              type:"post"  
         },
         language: {
          searchPlaceholder: "by 'assignment title' "
         },
         ordering: false,
         columns: [
          {
            render: function (data, type, row, meta) {
                   return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
            null,
            null,
            null,
            null,
            null,
            // null,
            null
        ] 
    });//end of data table
});//end of document ready
  